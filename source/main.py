import PySimpleGUI as sg
import serial.tools.list_ports as stlp
import minimalmodbus as mm
import threading
import time
import numpy
from struct import *
import os

import wsd1
import wsd2
import ua1
import measurement_log as log
import interface


# Be sure to change the title when a new version is created.
mainTitle = 'Wind Sensor Testing Software [BETA]'
sensorList = ['WSD1', 'WSD2 (SDI-12)', 'UA1']
# logging path if the user doesn't choose one
loggingPath = os.path.dirname(os.path.realpath(__file__))
# Sets the visuals of the GUI
sg.theme('Dyacon')



def getPorts():
    '''Makes a current list of the computer ports that are open or in use.'''
    ports = []
    descriptionPorts = stlp.comports()
    for port in descriptionPorts:
        ports.append(port.name)

    return ports

def startMeasurementThread():
    sensorObject.connected = not sensorObject.connected

    if sensorObject.connected == True:
        # Before starting/resuming the measurement loop, re-read the SN and other info in case the SN was changed.
        sensorObject.getSN()
        pauseMeasurementThread.set()
        try:
            measurementRunning.set()
            if measurementThread.ident == None:
                measurementThread.start()
        finally:
            pass

    if sensorObject.connected == False:
        sensorObject.event.wait(timeout=3)
        pauseMeasurementThread.clear()
        sensorObject.event.clear()
        for keys in sensorObject.measurementKeys:
            window1[keys].update('')
        window1['-MW_LOG-'].update(disabled=True)
        sensorObject.logging = False
        pauseLoggingThread.clear()
        window1['-MW_LOG-'].update(text='Start Logging', button_color='#FFCC00')

def measurementLoop():
    while measurementRunning.is_set():
        pauseMeasurementThread.wait()
        try:
            sensorObject.getMeasurements(window1, pauseLoggingThread)
        except mm.InvalidResponseError:
            continue
        except IndexError:
            for keys in ['-MW_STAT-','-MW_S-', '-MW_D-']:
                window1[keys].update(numpy.nan)
        except Exception as e:
            #print(e)
            if sensorObject.sn == None:
                startMeasurementThread()
            else:
                continue
        # if sensorObject.connected == False:
        #     # If the sensor is disconnected before the loop pauses, empty the visible fields.
        #     pauseMeasurementThread.clear()
        #     for keys in sensorObject.measurementKeys:
        #         window1[keys].update('')

def startLoggingThread():
    '''Function to set up everything for logging'''
    sensorObject.logging = not sensorObject.logging

    if sensorObject.logging == True:
        pauseLoggingThread.set()
        try:
            loggingRunning.set()
            window1['-MW_LOG-'].update(text='Stop Logging', button_color='#ff4d00')
            if loggingThread.ident == None:
                loggingThread.start()
        finally:
            pass
    if sensorObject.logging == False:
        pauseLoggingThread.clear()
        window1['-MW_LOG-'].update(text='Start Logging', button_color='#FFCC00')
        #print("paused")

def loggingLoop():
    '''threading loop for logging'''
    while loggingRunning.is_set():
        pauseLoggingThread.wait()
        try:
            log.storeMeasurements(loggingPath, sensorObject, pauseLoggingThread)
        except ValueError:
            pass
        except Exception as e:
            #print(e)
            pass

# Needed to create the window. 
window1 = interface.mainWindow().generateWindow(mainTitle)
window2 = None
window3 = None
window4 = None
# sensorObject is the class object. Gets re-assigned when a type is chosen in the sensor configuration window
sensorObject = wsd1.WSD1(sensorList)
# default for WSD1 baudrate
sensorBaudrate = 19200
# Threads are necessary for the measureing and logging to run in the background of the GUI.
measurementThread = threading.Thread(target=measurementLoop)
loggingThread = threading.Thread(target=loggingLoop)
# The "running" is a flag that can pause the measurements.Honestly a little foggy on how it works
measurementRunning = threading.Event()
pauseMeasurementThread = threading.Event()
pauseMeasurementThread.set()
loggingRunning = threading.Event()
pauseLoggingThread = threading.Event()
pauseLoggingThread.set()

while True:
    '''This while loop keeps the GUI running and reading inputs from the user.'''
    window, event, values= sg.read_all_windows(timeout=100)

    if sensorObject != None:
        window1['-MW_CONNECT-'].update(text='Disconnect' if sensorObject.connected else 'Connect', 
                                       button_color='#ff4d00' if sensorObject.connected else '#FFCC00')

    if event == sg.WIN_CLOSED:
        '''Insures the program ends when the window is closed'''
        window.close()
        if window == window2:
            window2 = None
        elif window == window1:
            pauseLoggingThread.set()
            pauseMeasurementThread.set()
            measurementRunning.clear()
            loggingRunning.clear()
            if measurementThread.ident!=None:
                measurementThread.join()
            if loggingThread.ident != None:
                loggingThread.join()
            break

    
    #---------------Measurement Window Events------------------
    elif event == '-MW_CONNECT-':
        if sensorObject.sn != None:
            try:
                startMeasurementThread()
                if sensorObject.connected:
                    window1['-MW_SN-'].update(sensorObject.sn)
                    window1['-MW_A-'].update(sensorObject.address)
                    window1['-MW_BD-'].update(sensorObject.baudrate)
                    window1['-MW_P-'].update(sensorObject.port)
                    window1['-MW_STAT-'].update(sensorObject.status)
                    window1['-MW_LOG-'].update(disabled=False)
                if sensorObject.connected == False:
                    for keys in sensorObject.measurementKeys:
                        window1[keys].update('')
                    
            except Exception as e:
                # I hate the PySimpleGUI popup's bc the buttons are always all over the place, so this fixes it, but keeps it one line.
                choice, _ = sg.Window('!', [[sg.Text(f'Something went wrong. Error message: {e}')], [sg.Push(), sg.OK(s=5), sg.Push()]]).read(close=True)
                if sensorObject.connected:
                    startMeasurementThread()
        else:
            choice, _ = sg.Window('', [[sg.Text('Must configure sensor first.')], [sg.Push(), sg.OK(s=5), sg.Push()]]).read(close=True)

    elif event == "-MW_LOG-":
        if sensorObject.connected:
            startLoggingThread()
        elif sensorObject.sn != None:
             choice, _ = sg.Window('', [[sg.Text('Must connect sensor first.')], [sg.Push(), sg.OK(s=5), sg.Push()]]).read(close=True)
        else:
            choice, _ = sg.Window('', [[sg.Text('Must configure sensor first.')], [sg.Push(), sg.OK(s=5), sg.Push()]]).read(close=True)


    #---------------Command Window Events------------------
    elif event == '-CW_NORTH-':
        choice, _ = sg.Window('Continue?', [[sg.Text('Press OK to set current direction to 0°.')], 
                                            [sg.Cancel(s=5), sg.Push(), sg.OK(s=5)]]).read(close=True)
        #answer = sg.popup_ok_cancel('Press OK to set current direction to 0°.', line_width=20)
        if choice == 'OK':
            if sensorObject.sn != None:
                try:
                    if sensorObject.connected:
                        startMeasurementThread()
                        time.sleep(0.5)
                    success = sensorObject.setNorth()
                    if success == True:
                        choice, _ = sg.Window('', [[sg.Text('Unplug and plug sensor back in now before continuing.')], 
                                    [sg.Push(), sg.OK(s=5), sg.Push()]]).read(close=True)
                    else:
                        choice, _ = sg.Window('', [[sg.Text('Setting failed.')], [sg.Push(), sg.OK(s=5), sg.Push()]]).read(close=True)
                except Exception as e:
                    choice, _ = sg.Window('!', [[sg.Text(f'Something went wrong. Error message: {e}')], [sg.Push(), sg.OK(s=5), sg.Push()]]).read(close=True)
            elif sensorObject.sn == None:
                choice, _ = sg.Window('', [[sg.Text('Must configure sensor first.')], [sg.Push(), sg.OK(s=5), sg.Push()]]).read(close=True)

    
    elif event == '-CW_SN-':
        window3 = interface.inputSensorSNWindow()

    elif event == '-CW_MC-':
        window4 = interface.moreCommandsWindow()

    elif event == '-ISN_SN-':
        val = values[event]
        window3['-ISN_SET-'].update(disabled=True)
        if (len(val) and val[-1] not in ('0123456789')) or (val != '' and int(val)>65535):
            '''If inputed value is not an integer, delete the inputed value'''
            window3[event].update(val[:-1])
            val = val[:-1]
        window3['-ISN_SET-'].update(disabled=True if val=='' else False)
    
    elif event == '-ISN_C-':
        window3.close()

    elif event == '-ISN_SET-':
        new_sn = values['-ISN_SN-']
        if sensorObject.sn != None:
            try:
                if sensorObject.connected:
                    startMeasurementThread()
                    time.sleep(0.5)
                success = sensorObject.setSN(int(new_sn))
                if success == True:
                    choice, _ = sg.Window('', [[sg.Text('Unplug and plug sensor back in now before continuing.')], 
                                [sg.Push(), sg.OK(s=5), sg.Push()]]).read(close=True)
                    sensorObject.sn = new_sn
                    try:
                        sensorObject.connection()
                        window1['-MW_SN-'].update(sensorObject.sn)
                        window1['-MW_A-'].update(sensorObject.address)
                        window1['-MW_BD-'].update(sensorObject.baudrate)
                        window1['-MW_P-'].update(sensorObject.port)
                    except:
                        window1['-MW_SN-'].update('')
                        window1['-MW_A-'].update('')
                        window1['-MW_BD-'].update('')
                        window1['-MW_P-'].update('')
                        window1['-MW_STAT-'].update('')
                        raise
                else:
                    choice, _ = sg.Window('', [[sg.Text('Setting failed.')], [sg.Push(), sg.OK(s=5), sg.Push()]]).read(close=True)
                    #sg.popup('Setting failed.')
                window3.close()
            except Exception as e:
                choice, _ = sg.Window('!', [[sg.Text(f'Something went wrong. Error message: {e}')], [sg.Push(), sg.OK(s=5), sg.Push()]]).read(close=True)
                #sg.popup(f'Something went wrong. Error message: {e}')
        else:
            choice, _ = sg.Window('', [[sg.Text('Must configure sensor first.')], [sg.Push(), sg.OK(s=5), sg.Push()]]).read(close=True)
            window3.close()
    
    elif event == 'Clear Output':
        window1["-CW_OUTPUT-"].update(value='')

    # MODBUS Tab Events
    elif event == 'Read':
        if sensorObject.sn == None:
            choice, _ = sg.Window('', [[sg.Text('Must configure sensor first.')], [sg.Push(), sg.OK(s=5), sg.Push()]]).read(close=True, timeout=3000)
        elif values['-CW_MOD_IN-'] == '':
            choice, _ = sg.Window('', [[sg.Text('Address field is empty.')], [sg.Push(), sg.OK(s=5), sg.Push()]]).read(close=True, timeout=3000)
        elif sensorObject.connected:
            choice, _ = sg.Window('', [[sg.Text('Polling is active. Pausing polling...')], [sg.Push(), sg.OK(s=5), sg.Push()]]).read(close=True, timeout=2000)
            startMeasurementThread()
            time.sleep(0.5)
        else:
            try:
                userAddress = int(values['-CW_MOD_IN-'])
                answer = sensorObject.userRead(userAddress)
                sg.cprint(answer)
            except Exception as e:
                sg.cprint(e)
            finally:
                window1['-CW_MOD_IN-'].update("")

    elif event == 'Write':
        if sensorObject.sn == None:
            choice, _ = sg.Window('', [[sg.Text('Must configure sensor first.')], [sg.Push(), sg.OK(s=5), sg.Push()]]).read(close=True, timeout=3000)
        elif values['-CW_MOD_IN-'] == '':
            choice, _ = sg.Window('', [[sg.Text('Address field is empty.')], [sg.Push(), sg.OK(s=5), sg.Push()]]).read(close=True, timeout=3000)
        elif values['-CW_MOD_VAL-'] == '':
             choice, _ = sg.Window('', [[sg.Text('Cannot write an empty value to the sensor.')], [sg.Push(), sg.OK(s=5), sg.Push()]]).read(close=True, timeout=3000)
        elif sensorObject.connected:
            choice, _ = sg.Window('', [[sg.Text('Polling is active. Pausing polling...')], [sg.Push(), sg.OK(s=5), sg.Push()]]).read(close=True, timeout=2000)
            startMeasurementThread()
            time.sleep(0.5)
        else:
            try:
                userAddress = int(values['-CW_MOD_IN-'])
                userValue = int(values["-CW_MOD_VAL-"])
                answer = sensorObject.userWrite(userAddress, userValue)
                sg.cprint(answer)
                if userAddress == 103:
                    # if the user changed the address
                    choice, _ = sg.Window('', [[sg.Text('Unplug and plug sensor back in now before continuing.')], 
                                [sg.Push(), sg.OK(s=5), sg.Push()]]).read(close=True)
                    sensorObject.address = int(userValue)
                    try: 
                        sensorObject.connection()
                        sg.cprint("Address changed.\n")
                    except:
                        raise
                if userAddress == 104:
                    # if user changed the baudrate
                    choice, _ = sg.Window('', [[sg.Text('Unplug and plug sensor back in now before continuing.')], 
                                [sg.Push(), sg.OK(s=5), sg.Push()]]).read(close=True)
                    try: 
                        if userValue == 0:
                            sensorObject.baudrate = 1200
                        elif userValue == 1:
                            sensorObject.baudrate = 2400
                        elif userValue == 2:
                            sensorObject.baudrate = 4800
                        elif userValue == 3:
                            sensorObject.baudrate = 9600
                        elif userValue == 4:
                            sensorObject.baudrate = 19200
                        elif userValue == 5:
                            sensorObject.baudrate = 38400 
                        sensorObject.connection()
                        sg.cprint("Baudrate changed.\n")
                    except:
                        raise
            except Exception as e:
                sg.cprint(e)
            finally:
                window1['-CW_MOD_IN-'].update("")
                window1['-CW_MOD_VAL-'].update("")

    # SDI-12 Tab Events
    elif event == 'Send ASCII':
        if sensorObject.sn == None:
            choice, _ = sg.Window('', [[sg.Text('Must configure sensor first.')], [sg.Push(), sg.OK(s=5), sg.Push()]]).read(close=True, timeout=3000)
        elif values['-CW_SDI_IN-'] == '':
            choice, _ = sg.Window('', [[sg.Text('Field is empty.')], [sg.Push(), sg.OK(s=5), sg.Push()]]).read(close=True, timeout=3000)
        elif sensorObject.connected:
            choice, _ = sg.Window('', [[sg.Text('Polling is active. Pausing polling...')], [sg.Push(), sg.OK(s=5), sg.Push()]]).read(close=True, timeout=2000)
            startMeasurementThread()
            time.sleep(0.5)
        else:
            try:
                userInput = str(values["-CW_SDI_IN-"])
                answer = sensorObject.userInput(userInput)
                sg.cprint(answer)
                if f"{sensorObject.address}A" in userInput:
                    # if the user changed the address
                    choice, _ = sg.Window('', [[sg.Text('Unplug and plug sensor back in now before continuing.')], 
                                [sg.Push(), sg.OK(s=5), sg.Push()]]).read(close=True)
                    sensorObject.address = int(answer)
                    try: 
                        sensorObject.connection()
                        sg.cprint("Address changed.\n")
                    except:
                        raise
            except Exception as e:
                sg.cprint(e)
            finally:
                window1['-CW_SDI_IN-'].update("")

    # Common Commands Autofill
    elif event in ['?!','aA<new address>!', 'aI!', 'aM!', 'aD0!']:
        window1['-CW_SDI_TAB-'].select()
        if sensorObject.sn != None:
            if event == '?!':
                window1["-CW_SDI_IN-"].update(event)  
            elif event == 'aA<new address>!':
                '''Refer to elif event == -IA_A- below for logic. A new window opens and 
                has new keys.'''
                window3 = interface.inputNewAddressWindow()
            elif event == 'aI!':
                window1["-CW_SDI_IN-"].update(f"{sensorObject.address}I!")
            elif event == 'aM!':
                window1["-CW_SDI_IN-"].update(f"{sensorObject.address}M!")
            elif event == 'aD0!':
                window1["-CW_SDI_IN-"].update(f"{sensorObject.address}D0!")
        else:
            choice, _ = sg.Window('', [[sg.Text('Must configure sensor first.')], [sg.Push(), sg.OK(s=5), sg.Push()]]).read(close=True)
    elif event == '-IA_A-':
        # event from input new address window that appears after clicking aA<new address>! 
        # in the common commands frame
        val = values[event]
        window3['-IA_SET-'].update(disabled=True)
        if (len(val) and val[-1] not in ('0123456789')) or (val != '' and int(val)>256):
            '''If inputed value is not an integer, delete the inputed value'''
            window3[event].update(val[:-1])
            val = val[:-1]
        window3['-IA_SET-'].update(disabled=True if val=='' else False)
    elif event == '-IA_SET-':
        val = values["-IA_A-"]
        window1['-CW_SDI_IN-'].update(f"{sensorObject.address}A{val}!")
        window3.close()
    elif event == '-IA_C-':
        window3.close()

    elif event in ['101 (Read)','102 (Read)', '103 (Read/Write)', '104 (Read/Write)']:
        window1['-CW_MOD_TAB-'].select()
        if sensorObject.sn != None:
            if event == '101 (Read)':
                window1['-CW_MOD_IN-'].update('101')
            elif event == '102 (Read)':
                window1['-CW_MOD_IN-'].update('102')
            elif event == '103 (Read/Write)':
                window1['-CW_MOD_IN-'].update('103')
            elif event == '104 (Read/Write)':
                window1['-CW_MOD_IN-'].update('104')
        else:
            choice, _ = sg.Window('', [[sg.Text('Must configure sensor first.')], [sg.Push(), sg.OK(s=5), sg.Push()]]).read(close=True)


    #---------------Sensor Settings Window Events------------------
    elif event == 'Sensor Configuration':
        window2 = interface.sensorSettingsWindow().generateWindow(sensorList, getPorts(), sensorObject)
        window2['-SSW_LOG-'].print('Change baudrate from 19200 for Modbus sensors by right-clicking anywhere on this window.\n')

    elif event == 'Logging Settings':
        task, choices = sg.Window('Logging Folder').layout([[sg.Text('Choose save location of logging files.')],
                            [sg.VPush()],
                            [sg.Input(loggingPath, key='_FOLDER_', readonly=True), sg.FolderBrowse()], 
                            [sg.VPush()],
                            [sg.Cancel(), sg.Push(), sg.OK()]]).read(close=True)
        if task == 'OK':
            if choices['_FOLDER_']:
                loggingPath:str = choices['_FOLDER_']

        #print(loggingPath)

    elif event =='-SSW_ST_IN-':
        # if sensor type is changed in sensor config window, disable new address field.
        if sensorObject:
            # make sure object has been created
            if sensorObject != values['-SSW_ST_IN-']:
                window2['-SSW_NA_IN-'].update(disabled=True)
                window2['-SSW_NA_IN-'].update('')
                window2['-SSW_NA-'].update(disabled=True)
            '''disable certain features for certain sensors: UA1 sensors cannot be written to. Send ascii is disabled for modbus sensors.
            read and write disabled for sdi-12'''
            if values['-SSW_ST_IN-'] == 'UA1':
                window1['Write'].update(disabled=True)
                window1['Read'].update(disabled=False)
                window1['-CW_SN-'].update(disabled=True)
                window1['-CW_NORTH-'].update(disabled=True)
                window1['Send ASCII'].update(disabled=True)
            if values['-SSW_ST_IN-'] == 'WSD1':
                window1['Write'].update(disabled=False)
                window1['Read'].update(disabled=False)
                window1['-CW_SN-'].update(disabled=False)
                window1['-CW_NORTH-'].update(disabled=False)
                window1['Send ASCII'].update(disabled=True)
            if values['-SSW_ST_IN-'] == 'WSD2 (SDI-12)':
                window1['Write'].update(disabled=True)
                window1['Read'].update(disabled=True)
                window1['-CW_SN-'].update(disabled=False)
                window1['-CW_NORTH-'].update(disabled=False)
                window1['Send ASCII'].update(disabled=False)
            
    elif event == '-SSW_P_IN-':
        # If port is changed in sensor configuration window, disable new address field.
        if sensorObject:
            if sensorObject.port:
                if sensorObject.port != values[event]:
                    window2['-SSW_NA_IN-'].update(disabled=True)
                    window2['-SSW_NA_IN-'].update('')
                    window2['-SSW_NA-'].update(disabled=True)

    elif event == '-SSW_A_IN-':
        # If this is edited after the sensor has been calibrated, it diables the new address field.
        window2['-SSW_NA_IN-'].update(disabled=True)
        window2['-SSW_NA_IN-'].update('')
        window2['-SSW_NA-'].update(disabled=True)

        val= values[event]
        if (len(val) and val[-1] not in ('0123456789')) or (val != '' and int(val)>255):
            '''If inputed value is not an integer, delete the inputed value'''
            window2[event].update(val[:-1])
        elif val != '':
            #sensorObject.address = int(val)
            pass

    elif event == 'Change Baudrate':
        if values['-SSW_ST_IN-'] == '' or values['-SSW_ST_IN-'] == 'WSD2 (SDI-12)':
            choice, _ = sg.Window('', [[sg.Text('Please choose a Modbus sensor type.')], [sg.Push(), sg.OK(s=5), sg.Push()]]).read(close=True)
        else:
            window3 = interface.inputNewBaudrateWindow(sensorBaudrate)
    elif event == '-IBD_SET-':
        sensorBaudrate = values["-IBD_BD-"]
        window3.close()
        window2['-SSW_LOG-'].print(f'WSD1 baudrate is set to {sensorBaudrate}.\n')
    elif event == '-IA_C-':
        window3.close()

    elif event == '-SSW_NA_IN-':
        val= values[event]
        if (len(val) and val[-1] not in ('0123456789')) or (val != '' and int(val)>255):
            '''If inputed value is not an integer, delete the inputed value'''
            window2[event].update(val[:-1])
        elif val != '':
            window2['-SSW_NA-'].update(disabled=False)
        elif val == '':
            window2['-SSW_NA-'].update(disabled=True)

    elif event == '-SSW_NA-':
        # Apply new address button was clicked.
        try:
            if sensorObject.connected:
                # if sensor is polling, stop it.
                startMeasurementThread()
            success = sensorObject.changeAddress(int(values['-SSW_NA_IN-']), 0)
            if success == True:
                choice, _ = sg.Window('Notice', [[sg.Text('Unplug and plug sensor back in now before continuing.')], 
                            [sg.Push(), sg.OK(s=5), sg.Push()]]).read(close=True)
                sensorObject.address = int(values['-SSW_NA_IN-'])
                try:
                    window2['-SSW_LOG-'].print('Testing Connection...')
                    sensorObject.connection()
                    window2['-SSW_LOG-'].print('Connection Successful!\n')
                    window2['-SSW_LOG-'].print(f"New address is {sensorObject.address}.\n")
                    window2['-SSW_NA_IN-'].update(disabled=False)
                    window2['-SSW_NA_IN-'].update('')
                    window2['-SSW_A_IN-'].update(sensorObject.address)
                    window2['-SSW_SE-'].update(disabled=False)
                    window1['-MW_SN-'].update(sensorObject.sn)
                    window1['-MW_A-'].update(sensorObject.address)
                    window1['-MW_BD-'].update(sensorObject.baudrate)
                    window1['-MW_P-'].update(sensorObject.port)
                except Exception as e:
                    window2['-SSW_LOG-'].print('Could not connect to sensor.\n')
                    window2['-SSW_LOG-'].print(f'Exception Message: {e}\n')
                    window2['-SSW_NA_IN-'].update(disabled=True)
                    window2['-SSW_SE-'].update(disabled=True)
                    window1['-MW_SN-'].update('')
                    window1['-MW_A-'].update('')
                    window1['-MW_BD-'].update('')
                    window1['-MW_P-'].update('')
                    window1['-MW_STAT-'].update('')
            else:
                choice, _ = sg.Window('Notice', [[sg.Text('Setting failed.')], [sg.Push(), sg.OK(s=5), sg.Push()]]).read(close=True)
                #sg.popup('Setting failed.')
        except Exception as e:
            choice, _ = sg.Window('ERROR', [[sg.Text(f'Something went wrong. Error message: {e}')], [sg.Push(), sg.OK(s=5), sg.Push()]]).read(close=True)


    elif event == '-SSW_TC-':
        if '' in (values['-SSW_ST_IN-'], values['-SSW_P_IN-'], values['-SSW_A_IN-']):
            window2['-SSW_LOG-'].print('Must fill the first 3 fields first.\n')
        else:
            if values['-SSW_ST_IN-'] == sensorList[0]:
                sensorObject = wsd1.WSD1(sensorList)
                sensorObject.baudrate = sensorBaudrate
            elif values['-SSW_ST_IN-'] == sensorList[1]:
                sensorObject = wsd2.WSD2(sensorList)
            elif values['-SSW_ST_IN-'] == sensorList[2]:
                sensorObject = ua1.UA1(sensorList)
                sensorObject.baudrate = sensorBaudrate
            sensorObject.port = values['-SSW_P_IN-']
            sensorObject.address = int(values['-SSW_A_IN-'])
            
            try:
                window2['-SSW_LOG-'].print('Testing Connection...')
                sensorObject.connection()
                window2['-SSW_LOG-'].print('Connection Successful!')
                if sensorObject.type != "UA1":
                    print(sensorObject)
                    window2['-SSW_NA_IN-'].update(disabled=False)
                window2['-SSW_SE-'].update(disabled=False)
                window1['-MW_SN-'].update(sensorObject.sn)
                window1['-MW_A-'].update(sensorObject.address)
                window1['-MW_BD-'].update(sensorObject.baudrate)
                window1['-MW_P-'].update(sensorObject.port)
            except Exception as e:
                window2['-SSW_LOG-'].print('Could not connect to sensor.\n')
                window2['-SSW_LOG-'].print(f'Exception Message: {e}\n')
                window2['-SSW_NA_IN-'].update(disabled=True)
                window2['-SSW_SE-'].update(disabled=True)
                window1['-MW_SN-'].update('')
                window1['-MW_A-'].update('')
                window1['-MW_BD-'].update('')
                window1['-MW_P-'].update('')
                window1['-MW_STAT-'].update('')
    
    elif event == '-SSW_SE-':
        window2.close()

window.close()

if __name__ == '__main__':
    interface.mainWindow()