import datetime as dt
import csv
log_exists = False

def storeMeasurements(path:str, sensorObject:object, pauseLog):
    '''This is a function called from the loggingLoop thread. It sends all measurements and sensor 
    information to a csv. If the file is empty, it needs headers. If it already has content, it 
    gets appended. All sensors will have the same layout for storing data'''
    absolutepath = path+"/sensor"+str(sensorObject.sn)+'log.csv'
    global log_exists
    currentTime = dt.datetime.now()
    csvTime = dt.datetime.strftime(currentTime, '%H:%M:%S')
    csvWrite = [csvTime]
    for value in sensorObject.valuesList:
        csvWrite.append(value)

    if bool(log_exists) == False:
        try:
            with open(absolutepath, mode='x', newline='') as f:
                fileout=csv.writer(f, delimiter=',', quoting=csv.QUOTE_ALL)
                f.write('Sensor Type: {},SN: {},Date: {}\n'.format(sensorObject.type, sensorObject.sn, dt.datetime.strftime(currentTime, '%Y/%m/%d')))
                f.write('Time,Status,Speed,Direction,2 min Speed,2 min Direction,'
                        '10 min Speed,10 min Direction,Gust Speed,Gust Direction\n')
                fileout.writerow(csvWrite)
            log_exists = True
        except FileExistsError:
            log_exists = True
        except Exception as e:
            raise
    else:
        with open(absolutepath, mode='a', newline='') as f:
            fileout=csv.writer(f, delimiter=',', quoting=csv.QUOTE_ALL)
            fileout.writerow(csvWrite)    
    pauseLog.clear()