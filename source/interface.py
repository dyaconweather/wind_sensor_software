import PySimpleGUI as sg


class mainWindow():
    '''Creates the main window of the GUI.
    I recognize that the window classes make more sense to be nested functions instead of classes, but I like the
    organization of classes better, even if I'm not creating class objects.'''

    def generateWindow(self, title):
        '''Creates the window. Is called on initialization.'''
        layout = self.mainLayout()

        return sg.Window(title=title, layout=layout, finalize=True, grab_anywhere=False)

    def mainLayout(self):
        '''Creates the skelton for the window. Includes the toolbar and the tab groups.'''
        toolbar = [['&Settings', ['&Sensor Configuration', '&Logging Settings']], ]

        mainLayout = [[sg.Menu(toolbar, tearoff=False)], 
                      [sg.TabGroup([[sg.Tab('Measurements', self.measurementsTab()), sg.Tab('Command Panel', self.commandTab())]])]]

        return mainLayout

    def measurementsTab(self):
        '''Creates everything inside the Measurements tab.'''
        inputPad = ((2,0), (16,3))
        endTextPad = ((2,5), (8,3))
        startTextPad = ((0,3), (8,3))

        col1 = [[sg.Text('S/N', pad=startTextPad), sg.Input('', size=(10,1), pad=inputPad, readonly=True, key='-MW_SN-')], 
                [sg.Text('Address', pad=startTextPad), sg.Input('', size=(10,1), pad=inputPad, readonly=True, key='-MW_A-')], 
                [sg.Text('Baud Rate', pad=startTextPad), sg.Input('', size=(10,1), pad=inputPad, readonly=True, key='-MW_BD-')], 
                [sg.Text('Port', pad=startTextPad), sg.Input('', size=(10,1), pad=inputPad, readonly=True, key='-MW_P-')],
                [sg.Text('Status', pad=startTextPad), sg.Input('', size=(10,1), pad=inputPad, readonly=True, key='-MW_STAT-')]]

        col2 = [[sg.Text('Speed', pad=startTextPad), sg.Input('', size=(7,1), pad=inputPad, readonly=True, key='-MW_S-'), sg.Text('m/s', pad=endTextPad)],
                [sg.Text('Speed 2 min', pad=startTextPad), sg.Input('', size=(7,1), pad=inputPad, readonly=True, key='-MW_S2-'), sg.Text('m/s', pad=endTextPad)],
                [sg.Text('Speed 10 min', pad=startTextPad), sg.Input('', size=(7,1), pad=inputPad, readonly=True, key='-MW_S10-'), sg.Text('m/s', pad=endTextPad)],
                [sg.Text('Gust Speed', pad=startTextPad), sg.Input('', size=(7,1), pad=inputPad, readonly=True, key='-MW_GS-'), sg.Text('m/s', pad=endTextPad)],]

        col3 = [[sg.Text('Direction', pad=startTextPad), sg.Input('', size=(7,1), pad=inputPad, readonly=True, key='-MW_D-'), sg.Text('°', pad=endTextPad)],
                [sg.Text('Direction 2 min', pad=startTextPad), sg.Input('', size=(7,1), pad=inputPad, readonly=True, key='-MW_D2-'), sg.Text('°', pad=endTextPad)],
                [sg.Text('Direction 10 min', pad=startTextPad), sg.Input('', size=(7,1), pad=inputPad, readonly=True, key='-MW_D10-'), sg.Text('°', pad=endTextPad)],
                [sg.Text('Gust Direction', pad=startTextPad), sg.Input('', size=(7,1), pad=inputPad, readonly=True, key='-MW_GD-'), sg.Text('°', pad=endTextPad)]]

        bottomcol1 = [[sg.Text('Starting speed'), sg.Input('5', size=(6,1), pad=((0,5),(3,3))), sg.Text('')],
                      [sg.Text('End Speed'), sg.Input('0.5', size=(6,1), pad=((0,5),(3,3))), sg.Text('')]]

        tab = [[sg.Column([[sg.Frame('Info', [[sg.Column(col1, pad=((5,5),(3,3)), element_justification='right')]], expand_y=True)]], expand_y=True, p=(0,1)), 
                sg.Column([[sg.Frame('Measurements', [[sg.Column(col2, pad=((10,3),(3,3)), element_justification='right'), sg.Push(),
                                                       sg.Column(col3, pad=((5,5),(3,3)), element_justification='right', vertical_alignment='t')]],
                                     expand_x=True, expand_y=True)],
                           [sg.Button('Connect', bind_return_key=True, key='-MW_CONNECT-', size=(10,2), p=((4,3),(2,6))), sg.Push(), 
                            sg.Button('Start Logging', key='-MW_LOG-', size=(12,2), p=((0,6),(2,6)), disabled=True)]], p=(0,0), expand_x=True, expand_y=True)]]
        
        #recycled Test frame
        '''[sg.Frame('Test', [[sg.VPush()],[sg.Button('Start\nTest', size=(8,3), pad=((10,5),(3,3)), font='Helvetica 12'), 
                                   sg.Column(bottomcol1, element_justification='right', pad=((3,20),(3,3))),
                                   sg.Push(),
                                   sg.Button('00:00', size=(10,3), pad=((3,10),(3,3)), font='Helvetica 12')], [sg.VPush()]], 
                         expand_x=True, expand_y=True)]'''

        return tab

    def commandTab(self):
        '''Creates everything inside the Command Panel tab. Essentiallly a mini RealTerm.'''
        baudratesList = [115200, 57600, 38400, 19200, 9600, 1200, 300, 921600, 460800, 230400, 4800, 2400, 150, 110]
        textFont = 'Helvetica 8'

        modbusTab = [[sg.VPush()],[sg.Text('Address', pad=((5,0),(3,3))), 
                                   sg.Input('',s=7,key="-CW_MOD_IN-",pad=((1,3),(3,3)), tooltip="Not to confuse with slave address. This is the register-1."), 
                                   sg.Text('Value', pad=((5,0),(3,3))),
                                   sg.Input('', s=11, k="-CW_MOD_VAL-", pad=((1,3),(3,3)), tooltip="Value to write to the sensor. Only used with 'Write' registers."), 
                                   sg.Push(), sg.Button('Read', pad=((3,0),(3,3)), tooltip="Read value from sensor."),
                                   sg.Button('Write', pad=((1,5),(3,3)), tooltip="Write value to sensor.")], [sg.VPush()]]

        sdi12Tab = [[sg.VPush()], [sg.Input('', size=(34,1), key='-CW_SDI_IN-', focus=True, pad=((5,0),(3,3))),
                                   sg.Button('Send ASCII'),],
                    [sg.VPush()]]

        col1 = [[sg.Multiline(size=(53,8), auto_refresh=True, autoscroll=True, write_only=True, expand_y=True, disabled=True, reroute_cprint=True,
                              right_click_menu=[['&X'],["&Clear Output"]], k='-CW_OUTPUT-')],
                [sg.TabGroup([[sg.Tab('SDI-12', sdi12Tab, k='-CW_SDI_TAB-',), 
                               sg.Tab('Modbus', modbusTab, k='-CW_MOD_TAB-')]], tab_location='leftbottom', pad=((3,3), (5,5)), expand_x=True)],
                [sg.Button('Set North', key='-CW_NORTH-', tooltip='Sets current direction to 0°.'), sg.Button('Set SN', key='-CW_SN-'), 
                 sg.Push(), sg.Button('More Commands', pad=((3,3),3), key='-CW_MC-', tooltip="A table listing SDI-12 and MODBUS commands.")]]
        
        col2 = [[sg.Frame('Common Commands', [[sg.Text('?!', tooltip='Address Query', enable_events=True, pad=(3,0))],
                          [sg.Text('aA<new address>!', tooltip='Change Address e.g. 0A1!', enable_events=True, p=(3,0))], 
                          [sg.Text('aI!', tooltip='Send Identification e.g. 0I!', enable_events=True, p=(3,0))],
                          [sg.Text('aM!', tooltip='Start Measurement e.g. 0M!', enable_events=True, pad=(3,0))], 
                          [sg.Text('aD0!', tooltip='Send Data e.g. 0D0!', enable_events=True, p=(3,0))],
                          [sg.Text('-------------------------------')], 
                          [sg.Text('101 (Read)', tooltip='Serial Number', enable_events=True, p=(3,0))],
                          [sg.Text('102 (Read)', tooltip='Firmware Version', enable_events=True, p=(3,0))], 
                          [sg.Text('103 (Read/Write)', tooltip='Modbus Slave Address', enable_events=True, p=(3,0))],
                          [sg.Text('104 (Read/Write)', tooltip='Baud Rate', enable_events=True, p=(3,0))]], 
                          size=(145,230), expand_y=True, expand_x=True, title_location='n', tooltip='Click on a command to auto-fill input bar.')]]

        tab = [[sg.Column(col1, pad=((5,0),(3,3)), expand_y=True, expand_x=True), sg.Column(col2, pad=((0,5), (3,2)), vertical_alignment='top', expand_y=True, expand_x=True)]]

        return tab


class sensorSettingsWindow():
    '''This window is for establishing connection with the sensor. Only the basic settings are here.'''

    def generateWindow(self, sensorTypeList, computerPorts, sensorObject:object):
        self.sensorlist = sensorTypeList
        layout = self.mainLayout(computerPorts, sensorObject)

        return sg.Window(title='Sensor Configuration', layout=layout, finalize=True, grab_anywhere=True, right_click_menu=[['&Click'], ['&Change Baudrate']])

    def mainLayout(self, computerPorts:list, sensorObject:object):

        col1 = [[sg.Text('Sensor Type'), sg.Combo(self.sensorlist, default_value=sensorObject.type if sensorObject.sn else None, 
                                                  size=13, readonly=True, enable_events=True, key='-SSW_ST_IN-')], 
                [sg.Text('Port'), sg.Combo(computerPorts, default_value=sensorObject.port if sensorObject.sn else None, size=13, key='-SSW_P_IN-', enable_events=True)],
                [sg.Text('Address'), sg.Input(default_text=sensorObject.address if sensorObject.sn else '', size=15, enable_events=True, 
                                              tooltip='Address must be less than 256', key='-SSW_A_IN-')],
                [sg.Text('New Address'), sg.Input('', size=15, disabled=False if sensorObject.sn else True, key='-SSW_NA_IN-', enable_events=True)]]

        col2 = [[sg.Multiline(size=(25,7), write_only=True, autoscroll=True, key='-SSW_LOG-')]]

        mainLayout = [[sg.Column(col1, element_justification='right'), sg.Column(col2)],
                      [sg.Button('Test Connection', key='-SSW_TC-'), sg.Button('Apply New Address', enable_events=True, disabled=True, key='-SSW_NA-'), 
                       sg.Push(), 
                       sg.Button('Save & Exit', key='-SSW_SE-', disabled=False if sensorObject.sn else True)]]

        return mainLayout

def inputSensorSNWindow():
    layout = [[sg.Text('Please input serial number.')], 
              [sg.Input('', enable_events=True, size=(30,1), tooltip='Value must be an integer under 65535.', key='-ISN_SN-')],
              [sg.Button('Cancel', key='-ISN_C-'), sg.Push(), sg.Button('Set', key='-ISN_SET-', disabled=True)]]
    return sg.Window(title='Sensor Serial Number', layout=layout, finalize=True, grab_anywhere=True)

def inputNewAddressWindow():
    layout = [[sg.Text('Please input new address.')], 
              [sg.Input('', enable_events=True, size=(30,1), tooltip='Value must be an integer 256 and lower.', key='-IA_A-')],
              [sg.Button('Cancel', key='-IA_C-'), sg.Push(), sg.Button('OK', key='-IA_SET-', disabled=True)]]
    return sg.Window(title='Sensor Address', layout=layout, finalize=True, grab_anywhere=True)

def inputNewBaudrateWindow(currentBaudrate:int):
    baudrates = [1200, 2400, 4800, 9600, 19200, 38400]
    layout = [[sg.Text("Please select sensor's current baudrate.")], 
              [sg.Combo(baudrates, default_value=currentBaudrate, enable_events=True, size=(32,1), key='-IBD_BD-')],
              [sg.Button('Cancel', key='-IBD_C-'), sg.Push(), sg.Button('OK', key='-IBD_SET-')]]
    return sg.Window(title='Sensor Baudrate', layout=layout, finalize=True, grab_anywhere=True)

def moreCommandsWindow():
    modbusHeaders = ['Description', 'Address', 'Register', 'Access Type', 'Range', 'Data Type']
    modbusValues = [['Product ID/Rev', 100, 101, 'Read', '', '16-bit Signed Int',],
                    ['Serial Number', 101, 102, 'Read', '', '16-bit Signed Int'],
                    ['Firmware Version', 102, 103, 'Read', '', '16-bit Signed Int'],
                    ['Modbus Slave Address', 103, 104, 'Read/Write', '1-247', '16-bit Signed Int'],
                    ['', '', '', '', 'Default: 1', ''], 
                    ['Baud Rate', 104, 105, 'Read/Write', '0=1200 bps', '16-bit Signed Int'],
                    ['', '', '', '', '1=2400 bps', ''], ['', '', '', '', '2=4800 bps', ''], ['', '', '', '', '3=9600 bps', ''],
                    ['', '', '', '', '4=19200 bps', ''], ['', '', '', '', '5=38400 bps', '',], ['', '', '', '', 'Default: 4', ''],
                    ['Parity', 105, 106, 'Read/Write', '0=None', '16-bit Signed Int'], ['', '', '', '', '1=Odd', ''],
                    ['', '', '', '', '2=Even', ''], ['', '', '', '', 'Default: 0', ''], 
                    ['NIST Traceable', 107, 108, 'Read/Write', '0-1', '16-bit Signed Int'],
                    ['Calibration Date', 108, '109+110', 'Read/Write', '', '32-bit Signed Long'], 
                    ['Speed Slope', 110, '111+112', 'Read/Write', 'Default: 1', '32-bit Float'],
                    ['Speed Offset', 112, '113+114', 'Read/Write', 'Default: 0', '32-bit Float'],
                    ['Direction Offset', 114, '115+116', 'Read/Write', 'Default: 0', '32-bit Float'],
                    ['System Status', 200, 201, 'Read', '0 to 15', '16-bit Signed Int'], 
                    ['Wind Speed (m/s)', 201, 202, 'Read', '0 to 500', '16-bit Signed Int'],
                    ['Wind Direction (°)', 202, 203, 'Read', '0 to 3599', '16-bit Signed Int'],
                    ['2 min Avg Wind Speed', 203, 204, 'Read', '0 to 500', '16-bit Signed Int'],
                    ['2 min Avg Wind Speed', 204, 205, 'Read', '0 to 3599', '16-bit Signed Int'],
                    ['10 min Avg Wind Speed', 205, 206, 'Read', '0 to 500', '16-bit Signed Int'],
                    ['10 min Avg Wind Direction', 206, 207, 'Read', '0 to 3599', '16-bit Signed Int'],
                    ['Wind Gust Speed', 207, 208, 'Read', '0 to 500', '16-bit Signed Int'],
                    ['Wind Gust Direction', 208, 209, 'Read', '0 to 3599', '16-bit Signed Int'],
                    ['Raw Speed', 300, '301+302', 'Read', '', '32-bit Unsigned Long'],
                    ['Factory Cal Speed Slope', 400, '401+402', 'Read/Write', 'Default: 1', '32-bit Float'],
                    ['Factory Cal Speed Offset', 402, '402+403', 'Read/Write', 'Defualt: 0', '32-bit Float']]
    SDI12Headers = ['Description', 'Command Format', 'Example', 'Response', 'Response Format']
    SDI12Values = [['Acknowledge', 'a!', '0!', 0, 'a'], ['Address Query', '?!', '?!', 0, 'a'], 
                   ['Change Address', 'aA<new address>!', '0A1!', 1, '<new address>'],
                   ['Send Indentification', 'aI!', '0I!', "013DYACON", 'a<SDI-12 Version (1.3)>'], 
                   ['', '', '', "00051400265535", '<Manufacturer><Model (000514)>'], ['','', '', '', '<Firmware Version (002)>'],
                   ['', '', '', '', '<Serial Number>'], ['Start Measurement', 'aM!', '0M!', '00002', 'a<seconds to wait for data (000)>'],
                   ['', '', '', '', '<data count (2 parameters)>'], ['Start Concurrent', 'aC!', '0C!', '000020', 'a<seconds to wait>'],
                   ['Measurement', '', '', '', '<data count>'], ['Send Data*', 'aD0!', '0D0!', '0+3.2+285.7', 'a<speed (m/s)>'],
                   ['', '', '', '', '<direction (degrees)>'], ['Start Verification**', 'aV!', '0V!', '00001', 'a<seconds to wait><data count>'],
                   ['Verification', 'aD0!', '0D0!', '0+0', '1+x'], ['Response***', '', '', '', 'x = zero - No Error'], 
                   ['', '', '', '', 'x = 1 to 16 - Wind direction error']]
    layout = [[sg.Column([[sg.Push(), sg.Text('Modbus Commands', font='12'), sg.Push()],
                          [sg.Table(values=modbusValues, headings=modbusHeaders, col_widths=[18, 7, 8, 10, 11, 15], 
                                    auto_size_columns=False, justification='left', header_background_color='#FFCC00', 
                                    selected_row_colors=('black', 'white'), num_rows=33, hide_vertical_scroll=True, row_height=20)],
                          [sg.Push(), sg.Text('SDI-12 Commands', font='12'), sg.Push()],
                          [sg.Table(values=SDI12Values, headings=SDI12Headers, col_widths=[14, 13, 7, 12, 23],
                                    auto_size_columns=False, justification='left', header_background_color='#FFCC00', 
                                    selected_row_colors=('black', 'white'), num_rows=17, hide_vertical_scroll=True, row_height=20)]],
                         scrollable=True, vertical_scroll_only=True)]]
        
    return sg.Window(title='Commands', layout=layout, finalize=True,)
