import PySimpleGUI as sg
import minimalmodbus as mm
import threading
import time
import datetime as dt
from struct import *


class WSD1():
    def __init__(self, sensorList:list):
        self.type = sensorList[0]
        self.sn = None
        self.port = None
        self.address = None
        self.baudrate = 19200
        self.comm = None
        self.connected = False
        self.event = threading.Event()
        self.logging = False
        self.status = 0
    
    def connection(self):
        self.comm = mm.Instrument(self.port, self.address)
        self.comm.serial.timeout=0.2
        self.comm.serial.baudrate = self.baudrate
        self.getSN()
    
    def getSN(self):
        i=0
        while i<5:
            try:
                self.sn = self.comm.read_register(101)
                time.sleep(0.5)
                self.getBaudRate()
                self.comm.serial.baudrate=self.baudrate
                break
            except mm.NoResponseError as e:
                i+=1
                if i<5:
                    continue
                else:
                    self.sn = None
                    raise
            except mm.InvalidResponseError as e:
                '''It connected, but the sensor responded atypically.'''
                continue
            except Exception as e:
                self.sn = None
                raise

    def getBaudRate(self):
        i=0
        while i<5:
            try:
                rawBaudrate = self.comm.read_register(104)
                if rawBaudrate == 0:
                    self.baudrate = 1200
                elif rawBaudrate == 1:
                    self.baudrate = 2400
                elif rawBaudrate == 2:
                    self.baudrate = 4800
                elif rawBaudrate == 3:
                    self.baudrate = 9600
                elif rawBaudrate == 4:
                    self.baudrate = 19200
                elif rawBaudrate == 5:
                    self.baudrate = 38400
                break
            except mm.NoResponseError:
                i+=1
                if i <5:
                    continue 
                else:
                    raise
            except Exception as e:
                raise

    def getMeasurements(self, window, pauseLog):
        # print(self.comm.roundtrip_time)
        try:
            rawValuesList = self.comm.read_registers(200,9,4)
            #print(rawValuesList)
            self.interpretMeasurements(rawValuesList, window, pauseLog)
        except Exception as e:
            '''Will raise to def measurementLoop, which will take care of it.'''
            #print(e)
            raise

    def interpretMeasurements(self, rawValues:list, window, pauseLog):
        # print("here")
        currentTime = dt.datetime.now()
        valuesList = []
        columnsNames = [f'Status_{self.sn}', f'Wind Speed_{self.sn}', f'Wind Direction_{self.sn}', f'2 min Wind Speed_{self.sn}',
                        f'2 min Wind Direction_{self.sn}', f'10 min Wind Speed_{self.sn}', f'2 min Wind Direction_{self.sn}', 
                        f'Gust Speed_{self.sn}', f'Gust Direction_{self.sn}']
        for measurement in rawValues:
            if measurement != rawValues[0]:
                valuesList.append(measurement/10)
            else:
                valuesList.append(measurement)
        
        self.showMeasurements(valuesList, window, pauseLog)

    def showMeasurements(self, valuesList:list, window:sg.Window, pauseLog):
        self.valuesList = valuesList
        if self.logging:
            pauseLog.set()
        self.measurementKeys = ['-MW_STAT-','-MW_S-', '-MW_D-', '-MW_S2-', '-MW_D2-', '-MW_S10-', '-MW_D10-', '-MW_GS-', '-MW_GD-']
        if self.connected:
            for (key, measurement) in zip(self.measurementKeys, valuesList):
                window[key].update(measurement)
        else:
            self.event.set()        
    
    def changeAddress(self, newAddress:int, error_count:int):
        '''Attempt to change sensor address. This function can't be called until the sensor is configured, so any error should
        be a checksum/timing/fluke, UNLESS the user has unplugged the sensor after configuring it. Due to that possibility and
        to avoid an infinite loop, this function will call itself only 5 times, then give an error.'''
        success = False
        if self.sn:
            try:
                self.comm.write_register(103, newAddress)
                self.address = self.comm.read_register(103)
                self.comm = mm.Instrument(self.port, newAddress)
                success = True
            except Exception as e:
                #print(e)
                error_count += 1
                if error_count == 5:
                    raise
                else:
                    self.changeAddress(newAddress, error_count)
        return success

    def setSN(self, newSN:int):
        ''' Changes the serial number. To change the SN for the WSD-1, the system looks for a specific byte
        message'''
        success = False
        code = pack('>HBBB', 0, 117, 191, 0)
        case = pack('B', 1)
        snBytes = newSN.to_bytes(2, 'big')
        payload = (str(code, "latin1")+str(case, "latin1")
                    +str(snBytes[0].to_bytes(1,'big'), "latin1")
                    +str(snBytes[1].to_bytes(1,'big'), "latin1"))
        while True:
            try:
                self.comm._perform_command(6, payload)
                success = True
                break
            except mm.NoResponseError:
                continue
            except mm.InvalidResponseError:
                # Response may be too short, but that doesn't mean it didn't work.
                success = True
                break
            except Exception as e:
                raise
        return success

    def setNorth(self):
        success = False
        code = pack('>HBBBB', 0, 117, 191, 0, 2)
        code = str(code, encoding = "latin1")
        while True:
            try:
                self.comm._perform_command(6, code)
                success = True
                break
            except mm.NoResponseError:
                continue
            except mm.InvalidResponseError:
                # Response may be too short, but that doesn't mean it didn't work.
                success = True
                break
            except:
                raise
        return success

    def userRead(self, address):
        '''This function sends user-inputted commands to the sensor.'''
        try:
            if address == 108:
                # 32 bit signed long
                answer = self.comm.read_long(address, signed=True)
            elif address == 300:
                # 32 bit unsigned long
                answer = self.comm.read_long(address, signed=False)
            elif address in [110, 112, 114, 400, 402]:
                # 32 bit float
                answer = self.comm.read_float(address, functioncode=3)
            else:
                # 16 bit signed int
                answer = self.comm.read_register(address, functioncode=4, signed=True)
        except Exception as e:
            raise
        return answer
    
    def userWrite(self, address, writeVar):
        '''This function overwites variables in the sensor with user-given inputs'''
        try:
            if address == 108:
                # 32 bit signed long
                self.comm.write_long(address, value=writeVar, signed=True)
                answer = self.comm.read_long(address, signed=True)
            elif address == 300:
                # 32 bit unsigned long
                self.comm.write_long(address, value=writeVar, signed=False)
                answer = self.comm.read_long(address, signed=False)
            elif address in [110, 112, 114, 400, 402]:
                # 32 bit float
                self.comm.write_float(address, value=writeVar, functioncode=3)
                answer = self.comm.read_float(address, functioncode=3)
            else:
                # 16 bit signed int
                self.comm.write_register(address, value=writeVar)
                answer = self.comm.read_register(address, functioncode=4, signed=True)
        except Exception as e:
            raise
        return answer