Copy this into the terminal. File names may have changed based on version and will need to be edited.

Pyinstaller doesn't work with Python version 3.10, so I had to install an earlier version (3.9) and setup the virtual env with version 3.9.
Setuptools needs to be version 63.1.0.
pyinstaller needs to be version 5.3.


pyInstaller --clean -D -w "C:\Users\natal\OneDrive\Documents\OneDrive\Documents\Dyacon\wind_sensor_software\source\main.py" -p "C:\Users\natal\OneDrive\Documents\OneDrive\Documents\Dyacon\wind_sensor_software\testenv\Lib\site-packages" -p "C:\Users\natal\OneDrive\Documents\OneDrive\Documents\Dyacon\wind_sensor_software\source" -i "C:\Users\natal\OneDrive\Documents\OneDrive\Documents\Dyacon\wind_sensor_software\executable\LogoLinks.ico" --distpath "C:\Users\natal\OneDrive\Documents\OneDrive\Documents\Dyacon\wind_sensor_software\executable\dist" --specpath "C:\Users\natal\OneDrive\Documents\OneDrive\Documents\Dyacon\wind_sensor_software\executable\build" --workpath "C:\Users\natal\OneDrive\Documents\OneDrive\Documents\Dyacon\wind_sensor_software\executable\build" -n "Wind Sensor Software (Beta)" --noconfirm