import PySimpleGUI as sg
import serial
import threading
from serial.serialutil import PARITY_EVEN, SEVENBITS, SerialTimeoutException
import time
import datetime as dt
import numpy
from struct import *


class WSD2():
    def __init__(self, sensorList:list):
        self.type = sensorList[1]
        self.sn = None
        self.port = None
        self.address = None
        self.baudrate = 1200
        self.comm = None
        self.connected = False
        self.event = threading.Event()
        self.logging = False
    
    def connection(self):
        self.comm = serial.Serial(self.port, bytesize=SEVENBITS, parity=PARITY_EVEN)
        self.comm.baudrate = 1200
        self.comm.timeout = 1
        self.comm.write_timeout = 0.5
        self.comm.break_condition = ' ' # SDI-12 protocols say the break statement is continuous spaces.
        self.duration = 0.012   # Duration of break statement sent to sensor.
        if self.comm.isOpen() == False:
            self.comm.open()
        self.getSN()
    
    def getSN(self):
        command = f"{self.address}I!"
        if self.comm.isOpen() == False:
            self.comm.open()
        try:
            for attempt in range(3):
                # Sometimes the poll attempt doesn't work.
                self.comm.reset_input_buffer()
                self.comm.send_break(duration=self.duration)
                self.comm.write(command.encode('ascii'))
                raw = self.comm.readline().decode('utf-8')
                # print(raw)
                if "DYACON" in raw:
                    break
            raw = raw.strip('\r\n')
            self.sn = raw[20:]
            self.sn = int(self.sn)
            time.sleep(0.5)
            self.getStatus()
        except ValueError as e:
            self.sn = None
            raise SerialTimeoutException("Address or port is incorrect.") from None
        except Exception as e:
            self.sn = None
            raise
        finally:
            ##self.data = pd.DataFrame() #The column names change if the sn changes.
            self.comm.close()

    def getStatus(self):
        try:
            command1 = f"{self.address}M!"
            command2 = f"{self.address}V!"
            command3 = f"{self.address}D0!"
            self.comm.reset_input_buffer()
            self.comm.send_break(duration=self.duration)
            self.comm.write(command1.encode('ascii'))
            raw = self.comm.readline().decode('utf-8')
            self.comm.write(command2.encode('ascii'))
            raw = self.comm.readline().decode('utf-8')
            self.comm.write(command3.encode('ascii'))
            self.status = self.comm.readline().decode('utf-8')
            self.status = self.status.strip('\r\n')
        except Exception as e:
            raise

    def getMeasurements(self, window, pauseLog):
        command1 = f"{self.address}M!"
        command2 = f"{self.address}D0!"
        if self.comm.isOpen() == False:
            self.comm.open()
        try:
            self.comm.reset_input_buffer()
            self.comm.send_break(duration = self.duration)
            self.comm.write(command1.encode('ascii'))
            raw = self.comm.readline().decode('utf-8')
            self.comm.write(command2.encode('ascii'))
            raw = self.comm.readline().decode('utf-8')
            raw = raw.strip('\r\n')
            rawValuesList = raw.split('+')
            del rawValuesList[0]
            self.interpretMeasurements(rawValuesList, window, pauseLog)
        except Exception as e:
            raise
        finally:
            self.comm.close()

    def interpretMeasurements(self, rawValues:list, window, pauseLog):
        try:
            currentTime = dt.datetime.now()
            valuesList = [self.status, float(rawValues[0]), float(rawValues[1]), numpy.nan, numpy.nan, numpy.nan, numpy.nan, numpy.nan, numpy.nan]
            columnsNames = [f'Status_{self.sn}', f'Wind Speed_{self.sn}', f'Wind Direction_{self.sn}', f'2 min Wind Speed_{self.sn}',
                            f'2 min Wind Direction_{self.sn}', f'10 min Wind Speed_{self.sn}', f'10 min Wind Direction_{self.sn}', 
                            f'Gust Speed_{self.sn}', f'Gust Direction_{self.sn}']
            # if self.data.empty == True:
            #     self.data = pd.DataFrame([valuesList], columns=columnsNames, index=[currentTime])
            # else:
            #     self.data = self.data.append(pd.DataFrame([valuesList], columns=columnsNames, index=[currentTime]))
            self.showMeasurements(valuesList, window, pauseLog)
        except Exception as e:
            raise

    def showMeasurements(self, valuesList:list, window:sg.Window, pauseLog):
        self.valuesList=valuesList
        if self.logging:
            pauseLog.set()
        self.measurementKeys = ['-MW_STAT-','-MW_S-', '-MW_D-', '-MW_S2-', '-MW_D2-', '-MW_S10-', '-MW_D10-', '-MW_GS-', '-MW_GD-']
        if self.connected:
            for (key, measurement) in zip(self.measurementKeys, valuesList):
                window[key].update(measurement)
            else:
                self.event.set()

    def changeAddress(self, new_address, error_count=None):
        success = False
        command = f"{self.address}A{new_address}!"
        if self.comm.isOpen() == False:
            self.comm.open()
        try:
            for attempt in range(3):
                # Sometimes the poll attempt doesn't work.
                self.comm.reset_input_buffer()
                self.comm.send_break(duration=self.duration)
                self.comm.write(command.encode('ascii'))
                raw = self.comm.readline().decode('utf-8')
                # print(raw)
                if str(new_address) in raw:
                    success=True
                    break 
        except ValueError as e:
            raise SerialTimeoutException("Address or port is incorrect.") from None
        except Exception as e:
            raise
        finally:
            self.comm.close()
        return success
    
    def setSN(self, new_sn):
        ''' Changes the serial number. To change the SN for the SDI-12 sensor, the sensor code looks for a 
        specific byte message and a certain manipulation of the serial number, which I don't quite understand.'''
        if self.comm.isOpen() == False:
            self.comm.open()

        digitKey = pack('>BBBB', 13, 1, 37, 90)
        divided_sn = [(new_sn>>12)&63, (new_sn>>6)&63, new_sn&63]

        # The string formatting must be latin1 for the bytes to be properly interpreted.
        payload = (str(digitKey, "latin1")
                    +str(divided_sn[0].to_bytes(1, 'big'), "latin1")
                    +str(divided_sn[1].to_bytes(1, 'big'), "latin1")
                    +str(divided_sn[2].to_bytes(1, 'big'), "latin1")) 
        try:
            for attempt in range(3):
                # Sometimes the connection fails, so it's wise to attempt it a few times.
                success = False
                self.comm.reset_input_buffer()
                self.comm.send_break(duration=self.duration)
                self.comm.write(f"{self.address}B{payload}!".encode('ascii'))
                raw = self.comm.read().decode('utf-8')
                if raw == '0':
                    success = True
                    break
        except Exception as e:
            raise
        finally:
            self.comm.close()
        return success

    def setNorth(self):
        '''Sets the current direction of the wind sensor to zero. This requires a specific byte message
        that the sesnor is looking for.'''
        if self.comm.isOpen() == False:
            self.comm.open()

        digitKey = pack('>BBBBB', 66, 14, 1, 37, 90)
        # The string formatting must be latin1 for the bytes to be properly interpreted.
        payload = str(digitKey, "latin1")
        try:
            for attempt in range(3):
                success = False
                self.comm.reset_input_buffer()
                self.comm.send_break(duration=self.duration)
                self.comm.write(f"{self.address}{payload}!".encode('ascii'))
                raw = self.comm.read().decode('utf-8')
                if raw == '0':
                    success = True
                    break
        except:
            raise
        finally:
            self.comm.close()
        return success
    
    def userInput(self, input):
        '''This function sends user-inputted commands to the sensor'''
        if self.comm.isOpen() == False:
            self.comm.open()
        try:
            self.comm.reset_input_buffer()
            self.comm.send_break(duration = self.duration)
            self.comm.write(input.encode('ascii'))
            answer = self.comm.readline().decode('utf-8')
        except:
            raise
        finally:
            self.comm.close()
        return answer