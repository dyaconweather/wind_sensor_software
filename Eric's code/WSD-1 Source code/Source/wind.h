#ifndef _WIND_H_
#define _WIND_H_

void Wind_Init(void);
void Wind_Process(void);
int Wind_Get(unsigned long *raw);
int Wind_ApplyCal(int wind);

#endif
