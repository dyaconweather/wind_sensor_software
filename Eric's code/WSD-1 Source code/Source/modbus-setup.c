#include <p24Fxxxx.h>
#include "modbus-setup.h"
#include "modbus.h"
#include "config.h"
#include "eeprom.h"
#include "wind.h"
#include "direction.h"

#define DEFAULT_SLAVE_ADDRESS	1

extern volatile int mbr1[MBR1_LEN];
extern volatile int mbr2[MBR2_LEN];
extern volatile int mbr3[MBR3_LEN];

//Register 1 is for settings (read/write)
//writeenable, min value, max value
const mbr1_config_t mbr1_config[MBR1_LEN] = {
{0, 0, 0 },		//100, Product ID
{0, 0, 0 },		//101, Serial Number
{0, 0, 0 },		//102, Firmware Version
{1,	1, 247 },	//103, Slave Address, 1 to 247 is valid
{1,	0, 7 },		//104, Baud Rate - 19200 default, 0 to 7 is valid
{1, 0, 2 },		//105, Parity - None default, 0 to 2 valid
{0, 0, 0 },		//106, Not Used
{0, 0, 0 },		//107, NIST Cal Flag
{0, 0, 0 },		//108, Cal Date
{0, 0, 0 },		//109, Cal Date
{1, 0, 0 },		//110, Wind Speed Slope
{1, 0, 0 },		//111, Wind Speed Slope
{1, 0, 0 },		//112, Wind Speed Offset
{1, 0, 0 },		//113, Wind Speed Offset
{1, 0, 0 },		//114, Wind Direction Offset
{1, 0, 0 },		//115, Wind Direction Offset
};

//Register 2
//200, Status
//201, Wind Speed
//202, Wind Direction
//203, Wind Speed, 2 min
//204, Wind Direction, 2 min
//205, Wind Speed, 10 min
//206, Wind Direction, 10 min
//207, Wind Speed, Gust
//208, Wind Direction, Gust

//Register 3
//300, Raw Wind Speed

//Register 4
//400, Factory Cal Wind Speed Slope
//402, Factory Cal Wind Speed Offset

void ModBUS_EEDefault(void)
{
	int data;
	double val;
	int ddata[2];

	//Write eeprom version
	data = EEPROM_VERSION;
	EE_Write(EEPROM_VERSION_LOCATION, 1, &data);

	//Write defalts
	data = PRODUCTID;		//100, Product ID
	EE_Write(EEPROM_SETTING_LOCATION, 1, &data);

	//We don't ever write defaults into the serial number

	//Don't write firmware version either

	data = DEFAULT_SLAVE_ADDRESS;	//103, Slave Address
	EE_Write(EEPROM_SETTING_LOCATION+3, 1, &data);
	data = 4;				//104, Baud Rate - 19200 default
	EE_Write(EEPROM_SETTING_LOCATION+4, 1, &data);
	data = 0;				//105, Parity - None default
	EE_Write(EEPROM_SETTING_LOCATION+5, 1, &data);

	data = 0;				//107, NIST
	EE_Write(EEPROM_SETTING_LOCATION+7, 1, &data);
	ddata[0] = 0;			//108, Cal Date
	ddata[1] = 0;
	EE_Write(EEPROM_SETTING_LOCATION+8, 2, ddata);

	val = 1;
	ddata[0] = *(((int *)&val)+1);
	ddata[1] = *((int *)&val);	
	EE_Write(EEPROM_SETTING_LOCATION+10, 2, ddata);
	EE_Write(EEPROM_FACTORYCAL_LOCATION, 2, ddata);	//Factory Cal

	val = 0;
	ddata[0] = *(((int *)&val)+1);
	ddata[1] = *((int *)&val);	
	EE_Write(EEPROM_SETTING_LOCATION+12, 2, ddata);
	EE_Write(EEPROM_SETTING_LOCATION+14, 2, ddata);
	EE_Write(EEPROM_FACTORYCAL_LOCATION+2, 2, ddata);	//Factory Cal
}

void ModBUS_EEDefaultV2(void)
{
	int data;
	double val;
	int ddata[2];

	data = 0;				//107, NIST
	EE_Write(EEPROM_SETTING_LOCATION+7, 1, &data);
	ddata[0] = 0;			//108, Cal Date
	ddata[1] = 0;
	EE_Write(EEPROM_SETTING_LOCATION+8, 2, ddata);

	val = 1;
	ddata[0] = *(((int *)&val)+1);
	ddata[1] = *((int *)&val);	
	EE_Write(EEPROM_FACTORYCAL_LOCATION, 2, ddata);	//Factory Cal

	val = 0;
	ddata[0] = *(((int *)&val)+1);
	ddata[1] = *((int *)&val);	
	EE_Write(EEPROM_FACTORYCAL_LOCATION+2, 2, ddata);	//Factory Cal
}

void ModBUS_SetData(int speed, int direction, unsigned int error, unsigned long counts)
{
	//apply calibration
	speed = Wind_ApplyCal(speed);
	direction = Dir_ApplyCal(direction);

	mbr2[0] = error;
	mbr2[1] = speed;
	mbr2[2] = direction;

	//Wind Speed Counts
	mbr3[0] = (unsigned int)(counts >> 16);
	mbr3[1] = (unsigned int)counts;
}

void ModBUS_SetAvg2(int speed, int dir)
{
	//apply calibration
	speed = Wind_ApplyCal(speed);
	dir = Dir_ApplyCal(dir);

	mbr2[3] = speed;
	mbr2[4] = dir;
}

void ModBUS_SetAvg10(int speed, int dir)
{
	//apply calibration
	speed = Wind_ApplyCal(speed);
	dir = Dir_ApplyCal(dir);

	mbr2[5] = speed;
	mbr2[6] = dir;
}

void ModBUS_SetGust(int speed, int dir)
{
	if(speed == -1)
	{
		mbr2[7] = 0;
		mbr2[8] = 0;
		return;
	}

	//apply calibration
	speed = Wind_ApplyCal(speed);
	dir = Dir_ApplyCal(dir);

	mbr2[7] = speed;
	mbr2[8] = dir;
}
