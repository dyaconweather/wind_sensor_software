#include <p24Fxxxx.h>
#include "modbus.h"
#include "modbus-setup.h"
#include "config.h"
#include "timer.h"
#include "eeprom.h"
#include "settings.h"
#include "Direction.h"

static unsigned char our_address;
volatile int mbr1[MBR1_LEN];
volatile int mbr2[MBR2_LEN];
volatile int mbr3[MBR3_LEN];
volatile int mbr4[MBR4_LEN];
extern const mbr1_config_t mbr1_config[MBR1_LEN];

#define TX_EN	LATBbits.LATB0

#define RX_BUF_SIZE		64
#define RX_BUF_NUM		2
#define TX_BUF_SIZE		63	//needs to be 2^x - 1

static unsigned char rx_buf[RX_BUF_NUM][RX_BUF_SIZE];
static unsigned char rx_count[RX_BUF_NUM];
static unsigned char rx_ptr;
static unsigned char tx_buf[TX_BUF_SIZE+1];
static unsigned char tx_fptr, tx_bptr;
static char tx_inprocess;
static char cal_unlocked = 0;

static void UART_write(unsigned char *str, int len);
extern void StartBootLoader(void);

static void ModBUS_SetBaud(int br)
{
	//Baudrate settings
	//Delay setup of 3.5 characters
	switch(br)
	{
	case 0:	//1200
		PR3 = 14600;
		U1BRG = 207;
		break;
	case 1:	//2400
		PR3 = 7300;
		U1BRG = 103;
		break;
	case 2:	//4800
		PR3 = 3650;
		U1BRG = 51;
		break;
	case 3:	//9600
		PR3 = 1825;
		U1BRG = 25;
		break;
	default:
	case 4:	//19200
		PR3 = 912;
		U1BRG = 12;
		break;
	case 5:	//38400
		PR3 = 456;
		U1MODEbits.BRGH = 1;
		U1BRG = 25;
		break;
	case 6:	//57600
		PR3 = 304;
		U1MODEbits.BRGH = 1;
		U1BRG = 16;
		break;
	case 7:	//115200
		PR3 = 152;
		U1MODEbits.BRGH = 1;
		U1BRG = 8;
		break;
	}
}

static void ModBUS_SetParity(int p)
{
	switch(p)
	{
	default:
	case 0:	//None
		U1MODEbits.PDSEL = 0;
		break;
	case 1:	//Odd
		U1MODEbits.PDSEL = 2;
		break;
	case 2:	//Even
		U1MODEbits.PDSEL = 1;
		break;
	}
}

void ModBUS_Init(void)
{
	int i;
	int data;
	
	//clear pointers
	tx_fptr = 0;
	tx_bptr = 0;
	rx_ptr = 0;
	for(i=0;i<RX_BUF_NUM;i++)
		rx_count[i] = 0;

	//check for valid eeprom data
	EE_Read(EEPROM_VERSION_LOCATION, 1, &data);
	if(data == 1)
		ModBUS_EEDefaultV2();	//update registers to version 2
	if(data != EEPROM_VERSION)
		ModBUS_EEDefault();

	//Load settings from EEPROM
	EE_Read(EEPROM_SETTING_LOCATION, MBR1_LEN, (int *)mbr1);
	EE_Read(EEPROM_FACTORYCAL_LOCATION, MBR4_LEN, (int *)mbr4);

	//zero input registers
	for(i=0;i<MBR2_LEN;i++)
		mbr2[i] = 0;
	for(i=0;i<MBR3_LEN;i++)
		mbr3[i] = 0;

	U1MODE = 0;
	U1STA = 0x2000;				//TX interrupt when FIFO empty
	IFS0bits.U1RXIF = 0;		//Clear Interrupt Flags
	IEC0bits.U1RXIE = 1;		//Enable Rx Interrupt
	IFS4bits.U1ERIF = 0;		//Clear Error Flag
	IEC4bits.U1ERIE = 1;		//Enable Error Interrupt
	IFS0bits.U1TXIF = 0;		//Clear Interrupt Flags

	mbr1[2] = FIRMWAREVERSION;
	our_address = mbr1[3];
	ModBUS_SetBaud(mbr1[4]);
	ModBUS_SetParity(mbr1[5]);

	U1MODEbits.USIDL = 0;	//module on in idle mode
	U1MODEbits.UARTEN = 1;	//Turn the peripheral on
	U1STAbits.UTXEN = 1;	//Turn on TX

	//Configure Timer 3 for end of rx packet
	T3CON = 0;              // Timer reset
 	IFS0bits.T3IF = 0;      // Reset Timer interrupt flag
	IPC2bits.T3IP = 3;      // Timer Interrupt priority
 	IEC0bits.T3IE = 1;      // Enable Timer interrupt
	T3CONbits.TSIDL = 0;	// Run in Idle
	T3CONbits.TCKPS = 1;	// 1:8 Prescale
 	TMR3 =  0x0000;  		// Clear counter
	//Timer is enabled on character reception
}

static const unsigned char ACRCL[] = { 
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 
0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 
0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 
0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 
0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 
0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 
0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 
0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 
0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 
0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 
0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 
0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 
0x40 
} ; 

static const char ACRCH[] = { 
0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6, 0x06, 0x07, 0xC7, 0x05, 0xC5, 0xC4, 
0x04, 0xCC, 0x0C, 0x0D, 0xCD, 0x0F, 0xCF, 0xCE, 0x0E, 0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09, 
0x08, 0xC8, 0xD8, 0x18, 0x19, 0xD9, 0x1B, 0xDB, 0xDA, 0x1A, 0x1E, 0xDE, 0xDF, 0x1F, 0xDD, 
0x1D, 0x1C, 0xDC, 0x14, 0xD4, 0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3, 
0x11, 0xD1, 0xD0, 0x10, 0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3, 0xF2, 0x32, 0x36, 0xF6, 0xF7, 
0x37, 0xF5, 0x35, 0x34, 0xF4, 0x3C, 0xFC, 0xFD, 0x3D, 0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A, 
0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38, 0x28, 0xE8, 0xE9, 0x29, 0xEB, 0x2B, 0x2A, 0xEA, 0xEE, 
0x2E, 0x2F, 0xEF, 0x2D, 0xED, 0xEC, 0x2C, 0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26, 
0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0, 0xA0, 0x60, 0x61, 0xA1, 0x63, 0xA3, 0xA2, 
0x62, 0x66, 0xA6, 0xA7, 0x67, 0xA5, 0x65, 0x64, 0xA4, 0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F, 
0x6E, 0xAE, 0xAA, 0x6A, 0x6B, 0xAB, 0x69, 0xA9, 0xA8, 0x68, 0x78, 0xB8, 0xB9, 0x79, 0xBB, 
0x7B, 0x7A, 0xBA, 0xBE, 0x7E, 0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C, 0xB4, 0x74, 0x75, 0xB5, 
0x77, 0xB7, 0xB6, 0x76, 0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71, 0x70, 0xB0, 0x50, 0x90, 0x91, 
0x51, 0x93, 0x53, 0x52, 0x92, 0x96, 0x56, 0x57, 0x97, 0x55, 0x95, 0x94, 0x54, 0x9C, 0x5C, 
0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E, 0x5A, 0x9A, 0x9B, 0x5B, 0x99, 0x59, 0x58, 0x98, 0x88, 
0x48, 0x49, 0x89, 0x4B, 0x8B, 0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C, 
0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42, 0x43, 0x83, 0x41, 0x81, 0x80, 
0x40 
} ; 

static void ModBUS_AddCRC(unsigned char *buf, unsigned char *ptr)
{
	unsigned char CRCH = 0xFF;
	unsigned char CRCL = 0xFF;
	int uIndex ;          		//index into CRC lookup table

	while (buf < ptr)
	{ 
		uIndex = CRCL ^ *buf++ ;    // Calculate the CRC
		CRCL = CRCH ^ ACRCL[uIndex] ; 
		CRCH = ACRCH[uIndex] ; 
	} 

	*buf++ = CRCL; 	// Low byte calculation
	*buf = CRCH; 	// High byte calculation
}

static int ModBUS_CheckCRC(unsigned char *buf, int len)
{
	int i, uIndex;
	unsigned char CRCH = 0xFF;
	unsigned char CRCL = 0xFF;

	for(i=0;i<(len-2);i++)
	{ 
		uIndex = CRCL ^ *buf++ ;    // Calculate the CRC
		CRCL = CRCH ^ ACRCL[uIndex] ; 
		CRCH = ACRCH[uIndex] ; 
	} 

	if(CRCL == buf[0] && CRCH == buf[1])
		return 0;
	else
		return 1;
}

static void IntToBuf(unsigned char *buf, int value)
{
	buf[0] = (unsigned char)(value >> 8);
	buf[1] = (unsigned char)value;
}

static unsigned int BufToInt(unsigned char *buf)
{
	unsigned int value;

	value = buf[0];
	value <<= 8;
	value += buf[1];

	return value;
}

static void Delay(void)
{
	volatile int i;
	for(i=0; i<500; i++);
}

static void ModBUS_SendError(unsigned char Code, unsigned char Error)
{
	unsigned char buf[5];

	buf[0] = our_address;		//our address
	buf[1] = 0x80 | Code;	//Function Code
	buf[2] = Error;
	ModBUS_AddCRC(buf, buf+3);
	UART_write(buf, 5);
}

void ModBUS_Process(void)
{
	int i;

	//check for ready message.
	for(i=0;i<RX_BUF_NUM;i++)
		if(rx_count[i] != 0 && rx_ptr != i)
			break;

	if(i == RX_BUF_NUM)
		return;		//no message to process

//process message
	//Check for minimum length
	if(rx_count[i] < 4)
		goto Process_Exit;

	//Check for our address
	if(rx_buf[i][0] != our_address)
		goto Process_Exit;

	//Check CRC
	if(ModBUS_CheckCRC(rx_buf[i], rx_count[i]))
		goto Process_Exit;

    Delay_ms(50);

	LATBbits.LATB1 = 0;
	Delay();
	Delay();

	//Decode Function Code
	switch(rx_buf[i][1])
	{
	case 3:	//Read holding registers
	case 4:	//Read input registers
		{
			unsigned char buf[32];
			int count;
			int j;
			int startaddr;
			int *reg;
			
			//Check address
			startaddr = rx_buf[i][2];
			startaddr <<= 8;
			startaddr += rx_buf[i][3];
			if( !( (startaddr >= 100 && startaddr < (100+MBR1_LEN)) ||
				   (startaddr >= 200 && startaddr < (200+MBR2_LEN)) ||
				   (startaddr >= 300 && startaddr < (300+MBR2_LEN)) ||
				   (startaddr >= 400 && startaddr < (400+MBR3_LEN)) ) )
			{
				ModBUS_SendError(rx_buf[i][1], 2);
				goto Process_Exit;	
			}
	
			//check address range
			if(startaddr < 200)
			{
				//100 range
				startaddr -= 100;
				if(rx_buf[i][4] != 0 || (startaddr + rx_buf[i][5]) > MBR1_LEN)
				{
					ModBUS_SendError(rx_buf[i][1], 3);
					goto Process_Exit;	
				}
				reg = (int *)mbr1;
			}
			else if(startaddr < 300)
			{
				//200 range
				startaddr -= 200;
	 			if(rx_buf[i][4] != 0 || (startaddr + rx_buf[i][5]) > MBR2_LEN)
				{
					ModBUS_SendError(rx_buf[i][1], 3);
					goto Process_Exit;	
				}
				reg = (int *)mbr2;
			}
			else if(startaddr < 400)
			{
				//300 range
				startaddr -= 300;
	 			if(rx_buf[i][4] != 0 || (startaddr + rx_buf[i][5]) > MBR3_LEN)
				{
					ModBUS_SendError(rx_buf[i][1], 3);
					goto Process_Exit;	
				}
				reg = (int *)mbr3;
			}
			else //if(startaddr < 500)
			{
				startaddr -= 400;
	 			if(rx_buf[i][4] != 0 || (startaddr + rx_buf[i][5]) > MBR4_LEN)
				{
					ModBUS_SendError(rx_buf[i][1], 3);
					goto Process_Exit;	
				}
				reg = (int *)mbr4;
			}
	
			//Check length
			if(rx_count[i] != 8)
			{
				ModBUS_SendError(rx_buf[i][1], 3);
				goto Process_Exit;	
			}
	
			//Send Response
			buf[0] = our_address;	//our address
			buf[1] = rx_buf[i][1];	//function code
			buf[2] = rx_buf[i][5] * 2;	//byte length
			count = 3;

			//registers
			for(j=startaddr;j<(startaddr + rx_buf[i][5]);j++)
			{
				IntToBuf(buf+count, reg[j]);
				count += 2;
			}

			//add CRC
			ModBUS_AddCRC(buf, buf+count);
			count += 2;

			//Transmit
			UART_write(buf, count);
		}
		break;

	case 6:	//Write single register
		{
			int incomingdata;
			int addr;

			//check for bootloader entrance
			if(rx_buf[i][2] == 0 && rx_buf[i][3] == 0 && rx_buf[i][4] == 0xA5 && rx_buf[i][5] == 0x5A)
			{
				//Send response
				unsigned char buf = 0;
				UART_write(&buf, 1);
				Delay_ms(100);	//allow character to go out before clocks change
				asm("mov      #__SP_init,w15");	//reset stack WARNING! - Don't return from this function!
				StartBootLoader(); 
			}

			//check for locked register access
			if(rx_buf[i][2] == 0 && rx_buf[i][3] == 0 && rx_buf[i][4] == 0x75 && rx_buf[i][5] == 0xBF && rx_buf[i][6] == 0)
			{
				int data, err;
				unsigned char buf = 0;

				switch(rx_buf[i][7])
				{
				case 0:
					//Lock out. This keeps the unit from responding until it is reset.
					while(1)
						Sleep();
					break;

				case 1:
					//write serial number
					data = rx_buf[i][8];
					data <<= 8;
					data += rx_buf[i][9];
					EE_Write(EEPROM_SETTING_LOCATION+1, 1, &data);
					//Send response
					UART_write(&buf, 1);
					break;

				case 2:
					//Save North
					data = Dir_Get(&err);
					EE_Write(EEPROM_DIRECTION_ZERO, 1, &data);
					//Send response
					UART_write(&buf, 1);
					break;

				case 3:
					//Cal start
					cal_unlocked = 1;
					//clear registers
					{
						double val;
						int i;
						
						for(i=0;i<MBR4_LEN;i+=4)
						{
							val = 1;
							mbr1[10+i] = *(((int *)&val)+1);
							mbr1[11+i] = *((int *)&val);	
							mbr4[0+i] = *(((int *)&val)+1);
							mbr4[1+i] = *((int *)&val);	
							val = 0;
							mbr1[12+i] = *(((int *)&val)+1);
							mbr1[13+i] = *((int *)&val);	
							mbr4[2+i] = *(((int *)&val)+1);
							mbr4[3+i] = *((int *)&val);	
						}
					}
					//Send response
					UART_write(&buf, 1);
					break;

				case 4:
					//Cal save
					if(cal_unlocked != 1)
						break;
					cal_unlocked = 0;
					//write values out to eeprom
					EE_Write(EEPROM_SETTING_LOCATION+10, MBR4_LEN, (int *)mbr1+10);
					EE_Write(EEPROM_FACTORYCAL_LOCATION, MBR4_LEN, (int *)mbr4);
					//set NIST and date
					mbr1[7] = rx_buf[i][8];
					EE_Write(EEPROM_SETTING_LOCATION+7, 1, (int *)&rx_buf[i][8]);
					EE_Write(EEPROM_SETTING_LOCATION+8, 2, (int *)&rx_buf[i][10]);
					//Send response
					UART_write(&buf, 1);
					break;			

				case 5:
					//Cal clear
					cal_unlocked = 0;
					//write values out to eeprom
					{
						double val;
						int i;

						for(i=0;i<MBR4_LEN;i+=4)
						{
							val = 1;
							mbr4[0+i] = *(((int *)&val)+1);
							mbr4[1+i] = *((int *)&val);	
							val = 0;
							mbr4[2+i] = *(((int *)&val)+1);
							mbr4[3+i] = *((int *)&val);	
						}
					}
					EE_Write(EEPROM_FACTORYCAL_LOCATION, MBR4_LEN, (int *)mbr4);
					//clear NIST and date
					{
						int val = 0;
						mbr1[7] = 0;
						EE_Write(EEPROM_SETTING_LOCATION+7, 1, &val);
						EE_Write(EEPROM_SETTING_LOCATION+8, 1, &val);
						EE_Write(EEPROM_SETTING_LOCATION+9, 1, &val);
					}
					//Send response
					UART_write(&buf, 1);
					break;
				}
			}

			//Check address
			if(rx_buf[i][2] != 0 || !(rx_buf[i][3] >= 100 && rx_buf[i][3] < (100+MBR1_LEN)))
			{
				ModBUS_SendError(rx_buf[i][1], 2);
				goto Process_Exit;	
			}

			//address is okay, convert to array address
 			addr = rx_buf[i][3] - 100;

			//Check to see if it is locked out
			if(mbr1[7] == 1 || cal_unlocked == 1)
			{
				//NIST flag set, lockout cal registers
				if(addr >= 10 && addr < (10+MBR4_LEN))
				{
					ModBUS_SendError(rx_buf[i][1], 2);
					goto Process_Exit;	
				}
			}

			//Check to see if it is writeable
			if(mbr1_config[addr].write_enable == 0)
			{
				ModBUS_SendError(rx_buf[i][1], 2);
				goto Process_Exit;	
			}

			//Check length
			if(rx_count[i] != 8)
			{
				ModBUS_SendError(rx_buf[i][1], 3);
				goto Process_Exit;	
			}

			//save data
			incomingdata = BufToInt(&rx_buf[i][4]);
	
			//check data range - only if both values are not zero
			if(mbr1_config[addr].min_value != 0 || mbr1_config[addr].max_value != 0)
			{
				if(incomingdata < mbr1_config[addr].min_value || incomingdata > mbr1_config[addr].max_value)
				{
					ModBUS_SendError(rx_buf[i][1], 3);
					goto Process_Exit;	
				}
			}

			//Update register
			mbr1[addr] = incomingdata;

			//Save to EEPROM
			EE_Write(EEPROM_SETTING_LOCATION+addr, 1, (int *)&mbr1[addr]);
	
			//Send Response
			UART_write(rx_buf[i], rx_count[i]);
		}
		break;

	case 16:	//Write multiple register
		{
			int startaddr;
			int incomingdata[MBR1_LEN];
			int count;
			int j;
			unsigned char buf[8];

			//Check address
			startaddr = rx_buf[i][2];
			startaddr <<= 8;
			startaddr += rx_buf[i][3];
			if( !( (startaddr >= 100 && startaddr < (100+MBR1_LEN)) ||
				   (startaddr >= 400 && startaddr < (400+MBR4_LEN)) ) )
			{
				ModBUS_SendError(rx_buf[i][1], 2);
				goto Process_Exit;	
			}
	
			//Check length and byte count
			count = BufToInt(&rx_buf[i][4]);
			if(rx_count[i] != (9+2*count) || 2*count != rx_buf[i][6])
			{
				ModBUS_SendError(rx_buf[i][1], 3);
				goto Process_Exit;	
			}

			//check address range
			if(startaddr < 200)
			{
				//100 range
				startaddr -= 100;
				if(rx_buf[i][4] != 0 || (startaddr + rx_buf[i][5]) > MBR1_LEN)
				{
					ModBUS_SendError(rx_buf[i][1], 3);
					goto Process_Exit;	
				}
	
				//Check to see if it is locked out
				if(mbr1[7] == 1 || cal_unlocked == 1)
				{
					//NIST flag set, lockout cal registers
					if((startaddr >= 10 && startaddr < (10+MBR4_LEN) ) ||
                       ((startaddr + count) >= 10 && (startaddr + count) < (10+MBR4_LEN)))
					{
						ModBUS_SendError(rx_buf[i][1], 2);
						goto Process_Exit;	
					}
				}

				//Check to see if it is writeable
				for(j=startaddr;j<(startaddr + count);j++)
				{
					if(mbr1_config[j].write_enable == 0)
					{
						ModBUS_SendError(rx_buf[i][1], 2);
						goto Process_Exit;	
					}
				}
	
				//get values and check data range - only if both values are not zero
				for(j=0;j<count;j++)
				{
					incomingdata[j] = BufToInt(&rx_buf[i][7+j*2]);
	
					if(mbr1_config[startaddr+j].min_value != 0 || mbr1_config[startaddr+j].max_value != 0)
					{
						if(incomingdata[j] < mbr1_config[startaddr+j].min_value || incomingdata[j] > mbr1_config[startaddr+j].max_value)
						{
							ModBUS_SendError(rx_buf[i][1], 3);
							goto Process_Exit;	
						}
					}
				}
	
				//Update registers
				for(j=0;j<count;j++)
					mbr1[startaddr+j] = incomingdata[j];
	
				//Save to EEPROM
				EE_Write(EEPROM_SETTING_LOCATION+startaddr, count, (int *)&mbr1[startaddr]);
			}
			else //if(startaddr < 400)
			{
				//400 range
				if(cal_unlocked == 0)
				{
					ModBUS_SendError(rx_buf[i][1], 2);
					goto Process_Exit;	
				}

				startaddr -= 400;
				if(rx_buf[i][4] != 0 || (startaddr + rx_buf[i][5]) > MBR4_LEN)
				{
					ModBUS_SendError(rx_buf[i][1], 3);
					goto Process_Exit;	
				}
	
				//Update registers
				for(j=0;j<count;j++)
					mbr4[startaddr+j] = BufToInt(&rx_buf[i][7+j*2]);
			}
	
			//Send Response
			buf[0] = our_address;	//our address
			buf[1] = rx_buf[i][1];	//function code
			buf[2] = rx_buf[i][2];	buf[3] = rx_buf[i][3];	//starting address
			buf[4] = rx_buf[i][4];	buf[5] = rx_buf[i][5];	//quantity of registers

			//add CRC
			ModBUS_AddCRC(buf, buf+6);

			UART_write(buf, 8);
		}
		break;

	default:
		ModBUS_SendError(rx_buf[i][1], 1);
		goto Process_Exit;
	}

Process_Exit:
	//We are done, clear count to indicate buffer consumed	
	rx_count[i] = 0;
	LATBbits.LATB1 = 1;
}

static void UART_write(unsigned char *str, int len)
{
	int i;

	//enable driver
	TX_EN = 1;
	Delay();
	
	for(i=0;i<len;i++)
	{
		if(((tx_fptr + 1) & TX_BUF_SIZE) == tx_bptr)
			break;		//no buffer space

		tx_buf[tx_fptr] = str[i];
		tx_fptr = (tx_fptr + 1) & TX_BUF_SIZE;
	}

	IEC0bits.U1TXIE = 1;	//Enable Tx Interrupt
	tx_inprocess = 1;
}

void __attribute__ ((interrupt, no_auto_psv)) _U1ErrInterrupt(void)
{
	//clear interrupt
	IFS4bits.U1ERIF = 0;		

	//if framing error, read out character
	while(U1STAbits.FERR)
	{
		if(U1STAbits.URXDA)
			U1RXREG;
		else
			break;
	}

	//clear overflow
	if(U1STA & 0x2)
		U1STAbits.OERR = 0;
}

void __attribute__ ((interrupt, no_auto_psv)) _U1RXInterrupt(void)
{
	char c;
	
	//reset timeout interrupt
 	TMR3 =  0x0000;  		// Clear counter
 	T3CONbits.TON = 1;      // Enable Timer and start the counter

	while(U1STAbits.URXDA)
	{
		//If framing error don't save character
		if(U1STAbits.FERR)
		{
			c = U1RXREG; 
			continue;
		}

		//read character
		c = U1RXREG; 
		
		//check for space
		if(rx_count[rx_ptr] >= RX_BUF_SIZE)
			continue;

		rx_buf[rx_ptr][rx_count[rx_ptr]] = c;
		++rx_count[rx_ptr];
	}

	//clear interrupt
	IFS0bits.U1RXIF = 0;
}

void __attribute__ ((interrupt, no_auto_psv)) _U1TXInterrupt(void)
{
	int count = 3;

	if(tx_fptr == tx_bptr)	//no more data
	{
		IEC0bits.U1TXIE = 0;	//Disable Tx Interrupt	

		//disable driver
		Delay();
		TX_EN = 0;

		//since we receive when we transmit, we need to clear the rx buffer
		rx_count[rx_ptr] = 0;
		tx_inprocess = 0;

		return;
	}

// Per rev 4 silicon errata, don't fill the buffer.
//	while(U1STAbits.UTXBF == 0)
	while(count--)
	{
		if(tx_fptr == tx_bptr)	//no more data
			break;

		U1TXREG = tx_buf[tx_bptr];
		tx_bptr = (tx_bptr + 1) & TX_BUF_SIZE;
	} 

	//clear interrupt
	IFS0bits.U1TXIF = 0;
}

void __attribute__((interrupt,no_auto_psv)) _T3Interrupt( void )
{
	IFS0bits.T3IF = 0;

	if(rx_count[rx_ptr] != 0)	//we timed out receiveing
	{
		if(rx_count[(rx_ptr + 1) & (RX_BUF_NUM - 1)] == 0)
			rx_ptr = (rx_ptr + 1) & (RX_BUF_NUM - 1);	//move to next buffer
		else
			rx_count[rx_ptr] = 0;		//throw away as there is no space
	}

 	T3CONbits.TON = 0;      // Disable timer
}

