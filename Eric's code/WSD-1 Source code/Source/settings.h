#ifndef _SETTINGS_H_
#define _SETTINGS_H_

void Setting_Init(void);
int DirectionZero(void);
double FCWindSlope(void);
double FCWindOffset(void);
double WindSlope(void);
double WindOffset(void);
double DirectionOffset(void);
 
#endif
