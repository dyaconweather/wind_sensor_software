#include <p24Fxxxx.h>
#include "config.h"
#include "timer.h"
#include "spi.h"
#include "direction.h"
#include "wind.h"
#include <stdio.h>
#include "modbus.h"
#include "eeprom.h"
#include "error.h"
#include "settings.h"
#include "average.h"
#include "gust.h"

int main(void)
{
#ifndef __DEBUG
	//enable watchdog
	RCONbits.SWDTEN = 1;
#else
	//disable watchdog
	RCONbits.SWDTEN = 0;
#endif

	//setup I/O
	ANSA = 0x0000;
	LATA = 0x0000;
	TRISA = 0x005C;

	ANSB = 0x0000;
	LATB = 0x8002;
	TRISB = 0x5094;

	//Configure modules enabled
	PMD1 = 0xC0D1;	//TMR1, TMR2, TMR3, SPI1, U1
	PMD2 = 0x0407;	//IC1, IC2
	PMD3 = 0x0682;	//All Off
	PMD4 = 0x008E;	//EEProm

	//setup stuff
	Timer_Init();
	SPI_Init();
	EE_Init();
	Setting_Init();
	ModBUS_Init();
	Dir_Init();
	Wind_Init();
	Average_Init();
	Gust_Init();

	while(1)
	{
#ifndef __DEBUG
		Idle();
#endif		

		//Process stuff
		Dir_Process();
		Wind_Process();
		Average_Process();
		Gust_Process();			//must be after average
		ModBUS_Process();
	}

	return 0;
}

