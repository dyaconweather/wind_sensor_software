#include <p24Fxxxx.h>
#include "spi.h"

void SPI_Init(void) 
{
	SPI1STAT = 0;	//module off

	//setup SPI1 registers
	SPI1CON1 = 0x043A;
	SPI1CON2 = 0x0001;
	SPI1STAT = 0x2014;
	SPI1STATbits.SPIEN = 1;	//enable module
}

unsigned int SPI_Transfer(unsigned long send)
{
	unsigned int data;
		
	//send dummy data
	SPI1BUF = send;

	//wait for transmit
	while(SPI1STATbits.SRMPT == 0 || SPI1STATbits.SPIBEC != 0)
		;
	
	//retreve data	
	data = SPI1BUF;

	return data;
}
