#include <p24Fxxxx.h>
#include "timer.h"
#include "direction.h"
#include "average.h"

static volatile int tmr_delay;
static volatile int tmr;

extern volatile int Dir_ProcessStart;
extern volatile int Wind_ProcessStart;
extern volatile int Average_ProcessStart;
extern volatile int Gust_ProcessStart;

//#define SIM_WIND

void Timer_Init(void)
{
	tmr_delay = 0;
	tmr = 0;

	T2CON = 0;              // Timer reset
 	IFS0bits.T2IF = 0;      // Reset Timer interrupt flag
	IPC1bits.T2IP = 3;      // Timer Interrupt priority
 	IEC0bits.T2IE = 1;      // Enable Timer interrupt
	T2CONbits.TSIDL = 0;	// Run in Idle
	T2CONbits.TCKPS = 1;	// 1:8 Prescale
 	TMR2=  0x0000;  		// Clear counter
	PR2 = 5000; 			// Timer period register
 	T2CONbits.TON = 1;      // Enable Timer and start the counter

#ifdef SIM_WIND
	TRISAbits.TRISA6 = 0;
#endif
}

//Timer interrupts every 10 ms
void __attribute__((interrupt,no_auto_psv)) _T2Interrupt( void )
{
	static int dir_tmr = 0;
	static int wind_tmr = 0;
	static int avg_tmr = 0;
	static int blink = 0;

	IFS0bits.T2IF = 0;

	//handle timers
	if(tmr_delay > 0)
		--tmr_delay;

	if(tmr > 0)
		--tmr;

	//handle ready tasks
	if(++dir_tmr >= 5)	//every 50 ms
	{
		dir_tmr = 0;
		Dir_ProcessStart = 1;
	}
	if(++wind_tmr >= 19)	//every 190 ms
	{
		wind_tmr = 0;
		Wind_ProcessStart = 1;
	}
	if(++avg_tmr >= 100)	//every 1 sec
	{
		avg_tmr = 0;
		Average_ProcessStart = 1;
		Gust_ProcessStart = 1;
	}
	
	//handle blinking LED for activity
	if(++blink >= 500)	//every 5 sec
	{
		blink = 0;
		LATBbits.LATB1 = 0;
	}
	else
	{
		LATBbits.LATB1 = 1;
	}

#ifdef SIM_WIND
	{
		static int windcnt = 0;
		if((windcnt = (windcnt + 1) & 0x7) == 0)
			LATAbits.LATA6 ^= 1;
	}
#endif
}

void Timer_Start(unsigned int ms)
{
 	IEC0bits.T2IE = 0;      // Disable Timer interrupt
	tmr = ms/10;
 	IEC0bits.T2IE = 1;      // Enable Timer interrupt
}

char Timer_Check(void)
{
	return (tmr == 0);
}

void Delay_ms(int delay)
{
 	IEC0bits.T2IE = 0;      // Disable Timer interrupt
	tmr_delay = delay/10;
 	IEC0bits.T2IE = 1;      // Enable Timer interrupt

	while(tmr_delay)
		Idle();
}
