#include <p24Fxxxx.h>
#include "direction.h"
#include "spi.h"
#include <math.h>
#include "error.h"
#include "settings.h"

#define CSn		LATBbits.LATB15
#define AVG_NUM 60	//3 second average

static double buf_x[AVG_NUM], buf_y[AVG_NUM];
static int error[AVG_NUM];
static int ptr;
volatile int Dir_ProcessStart = 0;

void Dir_Init(void)
{
	int i;

	//deselect
	CSn = 1;

	//clear data
	for(i=0;i<AVG_NUM;i++)
	{
		buf_x[i] = 0;
		buf_y[i] = 0;
	}

	ptr = 0;	
}

void Dir_Process(void)
{
	unsigned int datain;
	unsigned int parity;
	int paritybit;
	volatile int i;
	double rad;

	if(Dir_ProcessStart == 0)
		return;
	Dir_ProcessStart = 0;

	//read angle
	CSn = 0;
	datain = SPI_Transfer(0);
	CSn = 1;

	//check for errors
	error[ptr] = 0;		//no error
	parity = datain >> 1;
	paritybit = 0;
	for(i=0;i<15;i++)
	{
		paritybit ^= (parity & 0x0001);
		parity >>= 1;
	}

	if(paritybit != (datain & 0x0001))
		error[ptr] |= ERROR_DIR_INVALID;		//invalid read
	if((datain & 0x0002) == 0x0002)
		error[ptr] |= ERROR_DIR_ERROR;			//error flag set
	if((datain & 0x4000) == 0x4000)
		error[ptr] |= ERROR_DIR_FIELDHIGH;		//field too high
	if((datain & 0x8000) == 0x8000)
		error[ptr] |= ERROR_DIR_FIELDLOW;		//field too low

    //Calcualte components
	//deg = (datain >>= 2) / 2.844444
	//rad = (double)deg * PI / 180.0;
	rad = (double)((datain >>= 2) & 0x03FF);
   	rad *= 0.006135923;
    buf_x[ptr] = cos(rad);
    buf_y[ptr] = sin(rad) * -1;	//change rotation direction to match compass

	ptr++;
	if(ptr >= AVG_NUM)
	{
		ptr = 0;
	}
}

int Dir_GetRaw(double *avgx, double *avgy)
{
	int err = 0;
	int i;

	//sum data
	*avgx = 0;
	*avgy = 0;
	for(i=0;i<AVG_NUM;i++)
	{
		err |= error[i];
		*avgx += buf_x[i];
		*avgy += buf_y[i];
	}
	*avgx /= (double)AVG_NUM;
	*avgy /= (double)AVG_NUM;

	return err;	
}

int Dir_GetAngle(double x, double y)
{
	double rad;
	int degrees;

	if(x == 0 && y == 0)
		return 0;

	rad = atan2(y, x);
//	degrees = rad * 180.0 / PI * 10(so it is in 1/10 of degree)
	rad *= 572.95779;
	degrees = (int)rad;

	//atan2 is from -pi to pi. Correct so its 0 to 2*pi
	if(degrees < 0)
	    degrees += 3600;

	return degrees;
}

void Dir_GetComponent(int degrees, double *x, double *y)
{
	double rad;

	rad = (double)degrees / 572.95779;
    *x = cos(rad);
    *y = sin(rad);	//change rotation direction to match compass
}

//Takes about 5.9 ms
int Dir_Get(int *error)
{
	double avgx, avgy;
	int err;	
	int angle;

	err = Dir_GetRaw(&avgx, &avgy);
	angle = Dir_GetAngle(avgx, avgy);

	if(error)
		*error = err;

	return angle;
}

int Dir_ApplyCal(int angle)
{
	//apply zero
	if(DirectionZero() != 0)
		angle -= DirectionZero();
	
	//apply offset
	if(DirectionOffset() != 0)
		angle += DirectionOffset()*10;

	//check angle range
	while(angle < 0)
		angle += 3600;
	while(angle >= 3600)
		angle -= 3600;

	return angle;
}

