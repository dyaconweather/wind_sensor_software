#include <p24Fxxxx.h>
#include "wind.h"
#include "settings.h"
#include "direction.h"
#include "modbus.h"

#define COUNTS		500000.0
#define SLOPE		0.569250507
#define OFFSET		0.283435638

static unsigned int timer_count;
static unsigned long current_sum;
static int current_num;
static unsigned long previous;

//stores 3 second moving average
#define BUF_SIZE		16
#define BUF_MASK		15
static unsigned long wind_value[BUF_SIZE];
static unsigned int wind_ptr;
volatile int Wind_ProcessStart = 0;

typedef union _ULONG_VAL
{
    unsigned long Val;
	unsigned int  w[2];
    struct
    {
        unsigned int LW;
        unsigned int HW;
    } word;
} ULONG_VAL;

static void Wind_UpdateModBUS(void);

void Wind_Init(void) 
{
	int i;

	//clear buffer
	IC1CON1 = 0;
	IC1CON2 = 0;
	IC2CON1 = 0;
	IC2CON2 = 0;
	while(IC1CON1bits.ICBNE)
	{
		IC1BUF;
	}

	//Configure module
	IC1CON1bits.IC1TSEL = 4;	//timer1 clock
	IC2CON1bits.IC2TSEL = 4;
	IC1CON2bits.IC32 = 1;		//cascade mode
	IC2CON2bits.IC32 = 1;

 	IFS0bits.IC1IF = 0;      // Reset interrupt flag
	IPC0bits.IC1IP = 7;      // Interrupt priority
 	IEC0bits.IC1IE = 1;      // Enable interrupt

	//setup timer
	T1CON = 0;              // Timer reset
 	IFS0bits.T1IF = 0;      // Reset interrupt flag
	IPC0bits.T1IP = 6;      // Interrupt priority
 	IEC0bits.T1IE = 1;      // Enable interrupt
	T1CONbits.TSIDL = 0;	// Run in Idle
	T1CONbits.TCKPS = 1;	// 1:8 Prescale
 	TMR1=  0x0000;  		// Clear counter	
	PR1 = 0xFFFF; 			// Timer period register

	//clear holding registers
	wind_ptr = 0;
	for(i=0;i<BUF_SIZE;i++)
		wind_value[i] = 40L << 16;
	
	//clear current average window
	current_sum = 0;
	current_num = 0;
	previous = 40L << 16;

	//clear counts
	timer_count = 40;

	//enable
 	T1CONbits.TON = 1;
	IC2CON1bits.ICM = 2;	//capture on falling edge
	IC1CON1bits.ICM = 2;	//always enable lower half last
}

inline void AddValue(unsigned long val)
{
	previous = val;
	current_sum += val;
	++current_num;
}

void __attribute__((interrupt,no_auto_psv)) _T1Interrupt( void )
{
	IFS0bits.T1IF = 0;

	if(timer_count < 40)
		++timer_count;

	//check to see if we timed out	
	if(timer_count >= 40)
	{
		AddValue(40L << 16);
	}

	//check last stored value to see if we are slowing down
	else if(previous < (long)timer_count << 16)
	{
		AddValue((long)timer_count << 16);
	}
}

void __attribute__((interrupt,no_auto_psv)) _IC1Interrupt( void )
{
	static unsigned long previous_value;
	ULONG_VAL value;
    unsigned long delta_value;

	IFS0bits.IC1IF = 0;

	value.word.HW = IC2BUF;
	value.word.LW = IC1BUF;

    //Calculate duration between pulses
    delta_value = value.Val - previous_value;

    //store previous value
    previous_value = value.Val;
            
    //Watch for pulses that are too fast. 5000 = 57.2 m/s or 127.95 mph
    if(delta_value < 5000)
        return; //Discard value and do not reset timer
    
    //If timer hasn't expired, store value (if it is expired the wind is just starting)
	if(timer_count < 40)
        AddValue(delta_value);

	//reset counter
	TMR1 =  0x0000;
	timer_count = 0;
}

//runs every 190 msec
//takes about 103 us
void Wind_Process(void)
{
	if(Wind_ProcessStart == 0)
		return;
	Wind_ProcessStart = 0;

	//block interrupts
 	IEC0bits.T1IE = 0;
 	IEC0bits.IC1IE = 0;
	
	//save averaged value
	if(current_num == 0)
	{
		//there is no new data, replicate the last value.
		int ptr = wind_ptr - 1;
		if(ptr < 0)
			ptr = BUF_MASK;
		wind_value[wind_ptr] = wind_value[ptr];	
	}
	else
	{
		wind_value[wind_ptr] = current_sum / current_num;
	}

	current_sum	= 0;
	current_num = 0;

	//reenable interrupts
 	IEC0bits.T1IE = 1;
 	IEC0bits.IC1IE = 1;

	//move pointer to next buffer
	wind_ptr = (wind_ptr + 1) & BUF_MASK;

	//update ModBUS registers
	Wind_UpdateModBUS();
}

//Takes about 250 us
int Wind_Get(unsigned long *raw)
{
	unsigned long wavg = 0;
	double fw, hz;
	int i;

	//average
	for(i=0;i<BUF_SIZE;i++)
		wavg += wind_value[i];
	wavg /= BUF_SIZE;

	if(raw)
		*raw = wavg;

	if(wavg > 2600000L)
	{
		//no wind
		return 0;
	}

	hz = COUNTS/(double)wavg;
	fw = hz * SLOPE + OFFSET;
	fw *= 10;
	fw += 0.5;	//round up

	return (int)fw;
}

int Wind_ApplyCal(int wind)
{
	if(wind == 0)
		return 0;

	//apply calibration
	if(FCWindSlope() != 1)
		wind = FCWindSlope() * (double)wind;

	if(WindSlope() != 1)
		wind = WindSlope() * (double)wind;

	if(FCWindOffset() != 0)
		wind = (double)wind + FCWindOffset()*10.0;

	if(WindOffset() != 0)
		wind = (double)wind + WindOffset()*10.0;
	
	return wind;
}

static void Wind_UpdateModBUS(void)
{
	int speed, direction, error;
	unsigned long counts;

	speed = Wind_Get(&counts);
	direction = Dir_Get(&error);
	
	ModBUS_SetData(speed, direction, error, counts);
}

