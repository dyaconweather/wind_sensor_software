#ifndef _TIMER_H_
#define _TIMER_H_

void Timer_Init(void);
void Timer_isr(void);
void Timer_Start(unsigned int ms);
char Timer_Check(void);
void Delay_ms(int delay);

#endif
