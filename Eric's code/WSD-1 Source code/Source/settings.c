#include <p24Fxxxx.h>
#include "settings.h"
#include "modbus-setup.h"
#include "eeprom.h"

extern volatile int mbr1[MBR1_LEN];
extern volatile int mbr4[MBR4_LEN];

static int DirZero;
void Setting_Init(void)
{
	//Load Direction Zero From EEPROM
	EE_Read(EEPROM_DIRECTION_ZERO, 1, &DirZero);
	if(DirZero < 0 || DirZero >= 3600)
		DirZero = 0;
}

int DirectionZero(void)
{
	return DirZero;
}

double FCWindSlope(void)
{
	int d[2];
	d[1] = mbr4[0];
	d[0] = mbr4[1];
	return *((double *)d);
}

double FCWindOffset(void)
{
	int d[2];
	d[1] = mbr4[2];
	d[0] = mbr4[3];
	return *((double *)d);
}

double WindSlope(void)
{
	int d[2];
	d[1] = mbr1[10];
	d[0] = mbr1[11];
	return *((double *)d);
}

double WindOffset(void)
{
	int d[2];
	d[1] = mbr1[12];
	d[0] = mbr1[13];
	return *((double *)d);
}

double DirectionOffset(void)
{
	int d[2];
	d[1] = mbr1[14];
	d[0] = mbr1[15];
	return *((double *)d);
}
