#include <p24Fxxxx.h>
#include "error.h"
#include "modbus-setup.h"

extern volatile int mbr2[MBR2_LEN];

void Error_Init(void)
{
	mbr2[0] = 0;
}

void ErrorSet(unsigned int err)
{
	mbr2[0] |= err;
}

void ErrorClear(unsigned int err)
{
	mbr2[0] &= ~err;
}
