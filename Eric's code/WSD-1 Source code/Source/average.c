#include <p24Fxxxx.h>
#include "average.h"
#include <math.h>
#include "wind.h"
#include "direction.h"
#include "modbus.h"
#include "gust.h"

#define WIND_TOTAL_SECONDS		600	//10 minutes
#define WIND_SUB_SECONDS		10
#define WIND_TOTAL_NUM			(WIND_TOTAL_SECONDS/WIND_SUB_SECONDS)

//Wind Speed Average
static int WindSpeedAvg[WIND_TOTAL_NUM];	//holds WIND_TOTAL_SECONDS minutes of averages
static int WindSpeedAvgSub;

//Wind Direction Average
static int WindDirAvg[WIND_TOTAL_NUM];
static double WindDirAvgSubX;
static double WindDirAvgSubY;

//Location Pointers
static unsigned int WindPoint;
static unsigned int WindPointSub;
volatile int Average_ProcessStart = 0;

static void Average_UpdateModBUS(void);

void Average_Init(void)
{
	int i;

	//clear buffers
	for(i=0;i<WIND_TOTAL_NUM;i++)
	{
		WindSpeedAvg[i] = 0;
		WindDirAvg[i] = 0;
	}

	WindSpeedAvgSub = 0;
	WindDirAvgSubX = 0;
	WindDirAvgSubY = 0;
	
	WindPoint = 0;
	WindPointSub = 0;
}

//Processes every 1 sec
void Average_Process(void)
{
	double dirx, diry;

	if(Average_ProcessStart == 0)
		return;
	Average_ProcessStart = 0;

	//Get Data
	WindSpeedAvgSub += Wind_Get(0);
	Dir_GetRaw(&dirx, &diry);
    WindDirAvgSubX += dirx;
    WindDirAvgSubY += diry;

	//move sub average/max point
    WindPointSub++;	

	//if we have reached the number of sub points, advance main array
	if(WindPointSub >= WIND_SUB_SECONDS)
	{
		//save average speed
		WindSpeedAvg[WindPoint] = WindSpeedAvgSub / WIND_SUB_SECONDS;

        //save average direction
		WindDirAvgSubX /= (double)WIND_SUB_SECONDS;
		WindDirAvgSubY /= (double)WIND_SUB_SECONDS;
        WindDirAvg[WindPoint] = Dir_GetAngle(WindDirAvgSubX, WindDirAvgSubY);

        //reset pointer
        WindPointSub = 0;

        //clear temp avg holding variables
		WindSpeedAvgSub = 0;
        WindDirAvgSubX = 0;
        WindDirAvgSubY = 0;

        //see if we need to wrap
		if(++WindPoint >= WIND_TOTAL_NUM)
			WindPoint = 0;
	}

	//update ModBUS registers
	Average_UpdateModBUS();
}

static void Average_UpdateModBUS(void)
{
	int i;
	long wind_avg;
	double dirx_avg;
	double diry_avg;
	int ptr;

	//Get 10 minute data
	wind_avg = 0;
	dirx_avg = 0;
	diry_avg = 0;
	for(i=0;i<WIND_TOTAL_NUM;i++)
	{
		double x,y;

		//average
		wind_avg += WindSpeedAvg[i];
		Dir_GetComponent(WindDirAvg[i], &x, &y);		
		dirx_avg += x;
		diry_avg += y;
	}
	
	wind_avg /= WIND_TOTAL_NUM;
	dirx_avg /= (double)WIND_TOTAL_NUM;
	diry_avg /= (double)WIND_TOTAL_NUM;

	//save it
	ModBUS_SetAvg10(wind_avg, Dir_GetAngle(dirx_avg, diry_avg));

	//Get 2 minute data
	ptr = WindPoint;
	wind_avg = 0;
	dirx_avg = 0;
	diry_avg = 0;
    for(i=0;i<(120/WIND_SUB_SECONDS);i++)
    {
		double x,y;

		ptr--;
		if(ptr < 0)
			ptr = WIND_TOTAL_NUM-1;

		//average
		wind_avg += WindSpeedAvg[ptr];
		Dir_GetComponent(WindDirAvg[ptr], &x, &y);		
		dirx_avg += x;
		diry_avg += y;
    }

	wind_avg /= (120/WIND_SUB_SECONDS);
	dirx_avg /= (double)(120/WIND_SUB_SECONDS);
	diry_avg /= (double)(120/WIND_SUB_SECONDS);

	//save it
	Gust_SetAverage(wind_avg);
	ModBUS_SetAvg2(wind_avg, Dir_GetAngle(dirx_avg, diry_avg));
}
