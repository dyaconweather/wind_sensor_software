/***************************************************

This should only be included in modbus.c and modbus-setup.c

***************************************************/

#ifndef _MODBUSSETUP_H_
#define _MODBUSSETUP_H_

typedef struct {
	int write_enable;
	int min_value;
	int max_value;
} mbr1_config_t;

#define MBR1_LEN	16
#define MBR2_LEN	9
#define MBR3_LEN	2
#define MBR4_LEN	4

void ModBUS_EEDefault(void);
void ModBUS_EEDefaultV2(void);

#endif
