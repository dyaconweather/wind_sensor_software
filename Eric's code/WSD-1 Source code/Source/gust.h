#ifndef _GUST_H_
#define _GUST_H_

void Gust_Init(void);
void Gust_Process(void);
void Gust_SetAverage(int avg);

#endif
