#ifndef _MODBUS_H_
#define _MODBUS_H_

void ModBUS_Init(void);
void ModBUS_Process(void);
void ModBUS_SetData(int speed, int direction, unsigned int error, unsigned long counts);
void ModBUS_SetAvg2(int speed, int dir);
void ModBUS_SetAvg10(int speed, int dir);
void ModBUS_SetGust(int speed, int dir);

#endif
