#ifndef _DIRECTION_H_
#define _DIRECTION_H_

void Dir_Init(void);
void Dir_Process(void);
int Dir_Get(int *error);
int Dir_ApplyCal(int angle);

//used for average
int Dir_GetRaw(double *avgx, double *avgy);
int Dir_GetAngle(double avgx, double avgy);
void Dir_GetComponent(int degrees, double *x, double *y);

#endif
