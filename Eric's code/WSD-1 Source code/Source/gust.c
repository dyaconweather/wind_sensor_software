#include <p24Fxxxx.h>
#include "gust.h"
#include <math.h>
#include "wind.h"
#include "direction.h"
#include "modbus.h"

#define GUST_THRESHOLD			46		//9 knots = 4.63 m/s

#define GUST_TOTAL_SECONDS		600		//10 minutes
#define GUST_SUB_SECONDS		30
#define GUST_TOTAL_NUM			(GUST_TOTAL_SECONDS/GUST_SUB_SECONDS)

//Gust Speed
static int GustSpeed[GUST_TOTAL_NUM];
static int GustDir[GUST_TOTAL_NUM];
static int GustSubSpeed;
static int GustSubDir;
static int WindAvg;

//Location Pointers
static int GustPoint;
static int GustSubPoint;
volatile int Gust_ProcessStart = 0;

void Gust_Init(void)
{
	int i;

	//clear buffers
	for(i=0;i<GUST_TOTAL_NUM;i++)
	{
		GustSpeed[i] = -1;
		GustDir[i] = 0;
	}

	GustPoint = 0;
	GustSubPoint = 0;
	GustSubSpeed = -1;
	GustSubDir = 0;
	WindAvg = 0;
}

//Processes every 1 sec
void Gust_Process(void)
{
	long wind_point;
	int i;
	int MaxSpeed, MaxDir;

	if(Gust_ProcessStart == 0)
		return;
	Gust_ProcessStart = 0;

	//Get Data
	wind_point = Wind_Get(0);

	if(wind_point >= (WindAvg + GUST_THRESHOLD))
	{
		if(wind_point > GustSubSpeed)
		{
			GustSubSpeed = wind_point;
			GustSubDir = Dir_Get(0);
		}
	}

	//see if minute has passed
	if(++GustSubPoint >= GUST_SUB_SECONDS)
	{
		//save point
		GustSpeed[GustPoint] = GustSubSpeed;
		GustDir[GustPoint] = GustSubDir;

		//clear sub registers
		GustSubSpeed = -1;
		GustSubDir = 0;
		GustSubPoint = 0;

		//see if we need to wrap
		if(++GustPoint >= GUST_TOTAL_NUM)
			GustPoint = 0;
	}

	//update ModBUS registers
	MaxSpeed = -1;
	MaxDir = 0;
	for(i=0; i<GUST_TOTAL_NUM; i++)
	{
		if(GustSpeed[i] > MaxSpeed)
		{
			MaxSpeed = GustSpeed[i];
			MaxDir = GustDir[i];
		}
	}
	ModBUS_SetGust(MaxSpeed, MaxDir);
}

void Gust_SetAverage(int avg)
{
	WindAvg = avg;
}
