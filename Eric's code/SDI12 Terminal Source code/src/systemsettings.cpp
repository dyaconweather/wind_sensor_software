#include <QtGui>
#include <string>
#include <iostream>
#include <sstream>
#include <QSettings>
#include <QLocale>
#include "systemsettings.h"
#include "version.h"

sysSettings::sysSettings(void)
{
   	qDebug()<<"configuring system";
    location =  QDesktopServices::storageLocation(QDesktopServices::DataLocation);
    location += "\\MiMetLink";

    //check to see if the location exists
    if(QDir(location).exists() == false)
        QDir().mkpath(location);

    location += "\\settings.ini";
    qDebug()<<"Location:"<<location;

    LoadSettings();
}

void sysSettings::LoadSettings(void)
{
    Settings = new QSettings(location, QSettings::IniFormat);

	//load holding variables
    port = Settings->value("Port", QString("COM1")).toString();
	address = Settings->value("Address", QChar('0')).toChar();
}

QString sysSettings::GetSettingLocation(void)
{
    return location;
}

//*************************************************************************

QString sysSettings::GetSerialPort(void)
{
	return port;
}

void sysSettings::SetSerialPort(QString p)
{
	port = p;
	Settings->setValue("Port", p);
}

QChar sysSettings::GetAddress(void)
{
	return address;
}

void sysSettings::SetAddress(QChar a)
{
	address = a;
	Settings->setValue("Address", a);
}
