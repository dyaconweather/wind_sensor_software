#ifndef SETTINGS_H
#define SETTINGS_H

#define PORT
#define ADDRESS

class sysSettings
{
public:
    sysSettings(void);

private:
	QSettings *Settings;
    QString location;

	QString port;
	QChar address;

public:
    QString GetSettingLocation(void);
    void LoadSettings(void);

	QString GetSerialPort(void);
	void SetSerialPort(QString p);

	QChar GetAddress(void);
	void SetAddress(QChar a);
};

extern sysSettings SystemSettings;

#endif

