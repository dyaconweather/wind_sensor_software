#ifndef SETUPSCREEN_H
#define SETUPSCREEN_H

#include <QtGui>
#include "MyQWidget.h"
#include <QStackedWidget.h>

class setupScreen : public MyQWidget
{
    // Definition necessary to define this class as a QWidget
    Q_OBJECT

public:
    setupScreen(QStackedWidget* window, QWidget *parent);
	void Display(void);

// Objects from the screen that will interact with one another
private:
    QStackedWidget *Window;

    QComboBox *ComboBoxPort;
    QLineEdit *LineEditAddress;

signals:
    void CommChange(void);

//! [slots]
private slots:
    // Correspond to slots on the objects in the screen
    // Follow strict naming conventions
    // Connected in the constructor
    void on_ButtonCancel_clicked(void);
    void on_ButtonSave_clicked(void);

//! [slots]
};

#endif

