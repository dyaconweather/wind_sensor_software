#include <QtGui>
#include <QStackedWidget.h>
#include "mainwindow.h"
#include "systemsettings.h"

sysSettings SystemSettings;

int main(int argc, char * argv[])
{
	Q_INIT_RESOURCE(resource);
//	QResource::registerResource("screens/resource.rcc");
   	QApplication app(argc, argv);

    QStackedWidget* window = new QStackedWidget;

	MainWindow *mainScreen;
    mainScreen = new MainWindow(window, 0);

	//set size and show it
	window->setGeometry(100, 100, 450, 200);
    window->setWindowTitle("SDI12 Terminal");
    window->setFixedSize(window->width(), window->height());
    window->show();

    return app.exec();
}

