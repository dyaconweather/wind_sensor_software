#include <QtUiTools>
#include <QtGui>
#include <QSettings>
#include "mainwindow.h"
#include "systemsettings.h"

#define INTERNAL_AVG 0

MainWindow::MainWindow(QStackedWidget* window, QWidget *parent)
    : MyQWidget(window, parent)
{
    Window = (QStackedWidget *)window;
    qDebug("building main screen\r\n");

    // Load screen from ui file
    QUiLoader loader;
    loader.setLanguageChangeEnabled(true);
    QFile file(":/forms/mainwindow.ui");
    file.open(QFile::ReadOnly);
    loader.load(&file, this);
    file.close();

	//put in stacked widget
	mainWindow->addWidget(this);

    // Connect functions
    QMetaObject::connectSlotsByName(this);
	
	//connect to buttons

	//connect to labels
	LineEditCommand = qFindChild<QLineEdit*>(this, "LineEditCommand");
	LineEditResponse = qFindChild<QLineEdit*>(this, "LineEditResponse");

	//setup serial port
	Port = new QSerialPort();
    connect(Port, SIGNAL(hasLine()), this, SLOT(processRxLine()));
    connect(Port, SIGNAL(openPortFailed()), this, SLOT(openFailed()));
    connect(Port, SIGNAL(openPortSuccess()), this, SLOT(openSuccess()));
    connect(Port, SIGNAL(bufferOverflow()), this, SLOT(rxBufferOverflowed()));

    Port->usePort(SystemSettings.GetSerialPort(), Baud1200, CS8, SB1, ParityNone);
	Port->start();

	//update and show screen
	Display();

	//setup timers
	Timer = new QTimer(this);
	Timer->setSingleShot(true);
    QObject::connect(Timer, SIGNAL(timeout()), this, SLOT(TimerTimeout()));

	//setup other screens
	SetupScreen = new setupScreen(window, this);
    QObject::connect(SetupScreen, SIGNAL(CommChange()), this, SLOT(CommChange()));
    AboutScreen = new aboutScreen(window, this);
}

void MainWindow::CommChange(void)
{
    Port->closePort();
    Port->usePort(SystemSettings.GetSerialPort(), Baud1200, CS8, SB1, ParityNone);
    Port->start();
}

void MainWindow::Display(void)
{
	setWindowTitle("SDI12 Terminal");

	mainWindow->setCurrentWidget(this);
}

void MainWindow::on_ButtonSetup_clicked(void)
{
	Port->closePort();
	SetupScreen->Display();
	Port->start();
}

void MainWindow::on_ButtonAbout_clicked(void)
{
    AboutScreen->Display();
}

void MainWindow::on_ButtonSend_clicked(void)
{
	QByteArray buf;

	buf += LineEditCommand->text();

	//add parity
	unsigned char c, parity;
	for(int k=0;k<buf.length();k++)
	{
		c = buf[k];	
		parity = 0;
		for(int i=0;i<7;i++)
		{
			c <<= 1;
			parity ^= c & 0x80;
		}
		buf[k] = buf[k] + parity;
	}

	Port->writeBuffer(&buf);
	LineEditResponse->setText(QString());
	Timer->start(500);
}

void MainWindow::TimerTimeout(void)
{
	LineEditResponse->setText("Error: No Response");
}

//Serial Port Functions	*******************************************************
void MainWindow::processRxLine(void)
{
	QString str;

	Timer->stop();
	str = LineEditResponse->text();
	str += Port->getLine();
	str.replace("\r\n", "<CR><LF>");
	LineEditResponse->setText(str);
}

void MainWindow::openFailed() 
{
    qDebug()<<"Serial port object signaled it failed to open the desired comm device\n";
}

void MainWindow::openSuccess()
{
    qDebug()<<"Serial port object signaled it opened the desired comm device successfully\n";
}

void MainWindow::rxBufferOverflowed()
{
    qDebug()<<"Serial port buffer has overflowed\n";
}

