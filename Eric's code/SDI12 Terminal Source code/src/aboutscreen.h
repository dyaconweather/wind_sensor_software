#ifndef ABOUTSCREEN_H
#define ABOUTSCREEN_H

#include <QtGui>
#include "MyQWidget.h"

class aboutScreen : public MyQWidget
{
    // Definition necessary to define this class as a QWidget
    Q_OBJECT

public:
    aboutScreen(QStackedWidget* window, QWidget *parent);

// Objects from the screen that will interact with one another
private:
    QLabel *LabelSoftwareVer;

//! [slots]
private slots:
    void on_ButtonOK_clicked(void);

//! [slots]

};

#endif

