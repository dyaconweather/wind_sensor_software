#include "QSerialPort.h"
#include <QtDebug>
#include "version.h"

#ifndef Q_OS_WIN32
// POSIX C stuff for accessing the serial port
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#endif

QSerialPort::QSerialPort(QObject *parent) :
    QThread(parent)
{
    // make everything in this thread, run in this thread. (Including signals/slots)
    QObject::moveToThread(this);
    // make our data buffer
    dataBuffer = new QByteArray();
    running = true;
    deviceName=NULL;
#ifdef Q_OS_WIN32
    hSerial = INVALID_HANDLE_VALUE;
#else
	sfd = 0;
#endif
    bufferMutex = new QMutex(); // control access to buffer
    bufferMutex->unlock();  // not in a locked state

}

QSerialPort::~QSerialPort() 
{
    running = false;
#ifdef Q_OS_WIN32
    CloseHandle(hSerial);
#else
	close(sfd);
#endif
}

void QSerialPort::SendBreak(void)
{
	SetCommBreak(hSerial);
	Sleep(13);
	ClearCommBreak(hSerial);
}

//write data to serial port
int QSerialPort::writeBuffer(QByteArray *buffer) 
{
#ifdef Q_OS_WIN32
    if (hSerial != INVALID_HANDLE_VALUE) 
	{
		//wake device
		SendBreak();
		Sleep(5);
		SendBreak();
		Sleep(5);

		// have a valid file discriptor
        int dwBytesWritten = 0;
        WriteFile(hSerial, buffer->constData(), buffer->size(), (DWORD *)&dwBytesWritten, NULL);
        return dwBytesWritten;
#else
    if (sfd != 0) 
	{
	    // have a valid file discriptor
        return write(sfd, buffer->constData(), buffer->size());
#endif
    }
	else
	{
        return -1;
    }
}

// setup what device we should use
void QSerialPort::usePort(QString device_Name, int _buad, int _byteSize, int _stopBits, int _parity)
{
    deviceName = new QString(device_Name.toLatin1());
    // serial port settings
    Buad = _buad;
    ByteSize = _byteSize;
    StopBits = _stopBits;
    Parity = _parity;
}

// data fetcher, get next byte from buffer
uint8_t QSerialPort::getNextByte(void) 
{
    // mutex needed to make thread safe
	bufferMutex->lock(); // lock access to resource, or wait untill lock is avaliable
    uint8_t byte = (uint8_t)dataBuffer->at(0); // get the top most byte
    dataBuffer->remove(0, 1); // remove top most byte
	bufferMutex->unlock();
    return byte; // return top most byte
}

// return string of bytes in receive buffer
QString QSerialPort::getLine(void)
{
	QString str;
	uint8_t byte;

    // mutex needed to make thread safe
	bufferMutex->lock(); // lock access to resource, or wait untill lock is avaliable
	while(dataBuffer->size())
	{
		byte = (uint8_t)dataBuffer->at(0); // get the top most byte
	    dataBuffer->remove(0, 1); // remove top most byte
		str+= byte;

		//have we found the end of line?
		if(byte == '\n')
			break;
	}

	bufferMutex->unlock();
    return str;
}

// return number of bytes in receive buffer
uint32_t QSerialPort::bytesAvailable(void) 
{
    // this is thread safe, read only operation
    return (uint32_t)dataBuffer->size();
}

void QSerialPort::run(void)
{
	runComm();
}

// our main code thread
void QSerialPort::runComm(void)
{
    // thread procedure
#if _SERIALTHREAD_DEBUG
        qDebug() << "QSerialPort: QSerialPort Started..";
        qDebug() << "QSerialPort: Openning serial port " << deviceName->toLatin1();
#endif

    // open selected device
#ifdef Q_OS_WIN32
    QString device = "\\\\.\\";
    device += deviceName;
    qDebug()<<"Device: "<<device;
    hSerial = CreateFile((WCHAR *)device.constData(), GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, NULL);
    if ( hSerial == INVALID_HANDLE_VALUE ) 
	{
#else
    sfd = open( deviceName->toLatin1(), O_RDWR | O_NOCTTY );
    if ( sfd < 0 ) 
	{
#endif
        qDebug() << "QSerialPort: Failed to open serial port " << deviceName->toLatin1();
        emit openPortFailed();
        return;  // exit thread
    }

    // Yay we are able to open device as read/write
    qDebug() << "QSerialPort: Opened serial port " << deviceName->toLatin1() << " Sucessfully!";

    // now save current device/terminal settings
#ifdef Q_OS_WIN32
    dcbSerialParams.DCBlength=sizeof(dcbSerialParams);
    if (!GetCommState(hSerial, &dcbSerialParams)) 
	{
        qDebug() << "QSerialPort: Failed to get com port paramters";
        emit openPortFailed();
        return;
    }

    COMMTIMEOUTS timeouts;
    timeouts.ReadIntervalTimeout        = MAXDWORD;
    timeouts.ReadTotalTimeoutMultiplier    = 0;
    timeouts.ReadTotalTimeoutConstant    = 0;
    timeouts.WriteTotalTimeoutMultiplier    = 0;
    timeouts.WriteTotalTimeoutConstant    = 0;
    SetCommTimeouts(hSerial, &timeouts);
#else
    tcgetattr(sfd,&oldtio);
    // setup new terminal settings
    bzero(&newtio, sizeof(newtio));
    newtio.c_cflag = Buad | ByteSize | StopBits | Parity | CREAD | CLOCAL; // enable rx, ignore flowcontrol

    newtio.c_iflag = IGNPAR;
    newtio.c_oflag = 0;
    newtio.c_lflag = 0;
    newtio.c_cc[VTIME]    = 0;   /* inter-character timer unused */
    newtio.c_cc[VMIN]     = 1;   /* blocking read until atleast 1 charactors received */
    // flush device buffer
    tcflush(sfd, TCIFLUSH);
    // set new terminal settings to the device
    tcsetattr(sfd,TCSANOW,&newtio);
    // ok serial port setup and ready for use
#endif

#if _SERIALTHREAD_DEBUG
        qDebug() << "QSerialPort: Serial port setup and ready for use";
        qDebug() << "QSerialPort: Starting QSerialPort main loop";
#endif

#ifdef Q_OS_WIN32
    dcbSerialParams.BaudRate=Buad;
    dcbSerialParams.ByteSize=ByteSize;
    dcbSerialParams.Parity=Parity;
    dcbSerialParams.StopBits=StopBits;

    if(!SetCommState(hSerial, &dcbSerialParams)) 
	{
        qDebug() << "QSerialPort: Failed to set new com port paramters";
        emit openPortFailed();
        return;
    }
#endif
    // signal we are opened and running
    emit openPortSuccess();
    running = true;

    // start polling loop
    while(running) 
	{
	    uint8_t byte; // temp storage byte

#ifdef Q_OS_WIN32
	    int dwBytesRead;
        ReadFile(hSerial, (void *)&byte, 1, (DWORD *)&dwBytesRead, NULL); // reading 1 byte at a time
		if(dwBytesRead == 0)
        {
            Sleep(5);
            continue;
        }
#else
        if(read(sfd, (void *)&byte, 1) == -1) // reading 1 byte at a time
			continue;
#endif

        // print what we received
#if _SERIALTHREAD_DEBUG
            qDebug() << "QSerialPort: Received byte with value: " << byte;
#endif

        if (dataBuffer->size() > 1023) 
		{
            qDebug() << "Local buffer overflow, dropping input serial port data";
            emit bufferOverflow();
        }
		else
		{
            // stick byte read from device into buffer
            // Mutex needed to make thread safe from buffer read operation
            bufferMutex->lock();
            dataBuffer->append(byte & 0x7F);	//remove added parity
 			bufferMutex->unlock();
            emit hasData(); // signal our user that there is data to receive 
//NOTE: this is not really a good way to do it since it calls multiple times, but the receiver can read all data on the first call,
//leaving an empty buffer on repeat calls.

			if(byte == '\n')
				emit hasLine();	//signal user that there is a new line character
        }
    }
	
#if _SERIALTHREAD_DEBUG
    qDebug() << "QSerialPort: Ending Serial Thread";
#endif

#ifdef Q_OS_WIN32
	if(hSerial)
		CloseHandle(hSerial);
	hSerial = NULL;
#else
	close(sfd);
#endif
}

void QSerialPort::closePort(void)
{
    while(isRunning() == true)
	{
		running = false;
		Sleep(1);
	}
}

