#include <QtUiTools>
#include <QtGui>
#include <QSettings>
#include "aboutscreen.h"
#include "version.h"

aboutScreen::aboutScreen(QStackedWidget* window, QWidget *parent)
    : MyQWidget(window, parent)
{
    qDebug("building about screen\r\n");

    // Load screen from ui file
    QUiLoader loader;
    loader.setLanguageChangeEnabled(true);
    QFile file(":/forms/about.ui");
    file.open(QFile::ReadOnly);
    loader.load(&file, this);
    file.close();

    //put in stacked widget
    mainWindow->addWidget(this);

    // Connect functions
    QMetaObject::connectSlotsByName(this);

    LabelSoftwareVer = qFindChild<QLabel*>(this, "LabelSoftwareVer");

    LabelSoftwareVer->setText(QString("Version: ") + QString(SOFTWARE_VERSION));
}

void aboutScreen::on_ButtonOK_clicked(void)
{
    if(Parent)
        Parent->Display();
}
