#include <QtUiTools>
#include <QtGui>
#include <QSettings>
#include "setupscreen.h"
#include "systemsettings.h"
#include "enumser.h"
#include "version.h"

setupScreen::setupScreen(QStackedWidget* window, QWidget *parent)
    : MyQWidget(window, parent)
{
    qDebug("building setup screen\r\n");

    Window = (QStackedWidget *)window;

    // Load screen from ui file
    QUiLoader loader;
    loader.setLanguageChangeEnabled(true);
    QFile file(":/forms/configure.ui");
    file.open(QFile::ReadOnly);
    loader.load(&file, this);
    file.close();

    //put in stacked widget
    mainWindow->addWidget(this);

    // Connect functions
    QMetaObject::connectSlotsByName(this);

    ComboBoxPort = qFindChild<QComboBox*>(this, "ComboBoxPort");
    LineEditAddress = qFindChild<QLineEdit*>(this, "LineEditAddress");
}

void setupScreen::Display(void)
{
	ComboBoxPort->clear();

	//add serial ports
    CSimpleArray<UINT> ports_found;
    if (CEnumerateSerial::UsingGetDefaultCommConfig(ports_found))
    {
        for (int i=0; i<ports_found.GetSize(); i++)
        {
            ComboBoxPort->addItem(QString("COM")+QString::number(ports_found[i]));
        }
    }
	
	ComboBoxPort->setCurrentIndex(ComboBoxPort->findText(SystemSettings.GetSerialPort()));

    //Address
    LineEditAddress->setText(SystemSettings.GetAddress());

    mainWindow->setCurrentWidget(this);
}

void setupScreen::on_ButtonSave_clicked(void)
{
//save stuff
    //setup port
    if(SystemSettings.GetSerialPort() != ComboBoxPort->currentText())
    {
        SystemSettings.SetSerialPort(ComboBoxPort->currentText());
        emit this->CommChange();
    }
    else
        SystemSettings.SetSerialPort(ComboBoxPort->currentText());

    //Location
    SystemSettings.SetAddress(LineEditAddress->text().at(0));

    //leave screen
    on_ButtonCancel_clicked();
}

void setupScreen::on_ButtonCancel_clicked(void)
{
    if(Parent)
        Parent->Display();
}
