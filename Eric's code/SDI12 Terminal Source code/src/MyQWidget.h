#ifndef MYQWIDGET_H
#define MYQWIDGET_H

#include <QtGui>
#include <QWidget>
#include <QStackedWidget>

class MyQWidget : public QWidget
{
	Q_OBJECT

public:
	MyQWidget(QStackedWidget* window, QWidget *parent) : QWidget(parent)
	{
		mainWindow = window;
		Parent = (MyQWidget*)parent;
	}

	QStackedWidget* mainWindow;
	MyQWidget* Parent;

	virtual void Display(void)
	{
		mainWindow->setCurrentWidget(this);
	}
};

#endif

