#include <QtGui>
#include <QStackedWidget.h>
#include "MyQWidget.h"
#include "QSerialPort.h"
#include "setupscreen.h"
#include "aboutscreen.h"

class MainWindow : public MyQWidget
{
    Q_OBJECT

public:
    MainWindow(QStackedWidget* window, QWidget *parent);
	void Display(void);

private:
    QStackedWidget *Window;

	//screens
	setupScreen *SetupScreen;
    aboutScreen *AboutScreen;

	//items
    QLineEdit *LineEditCommand;
	QLineEdit *LineEditResponse;

	QSerialPort *Port;

	QTimer *Timer;

public slots:
    void CommChange(void);

private slots:
	void on_ButtonSetup_clicked(void);
    void on_ButtonAbout_clicked(void);
    void on_ButtonSend_clicked(void);

	void TimerTimeout(void);

    void processRxLine();
    void openFailed();
    void openSuccess();
    void rxBufferOverflowed();
};
