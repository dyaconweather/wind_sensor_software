TEMPLATE = app
TARGET = 
DEPENDPATH += . src 
INCLUDEPATH += . src
CONFIG += uitools

#QT      +=  webkit network
RESOURCES = ./screens/resource.qrc
RC_FILE = ./screens/SDI12Term.rc

# Input
HEADERS += $$files(./src/*.h)

SOURCES += $$files(./src/*.cpp)

#-------------------------------------------------
# Make sure output directory for object file and
# executable is in the correct subdirectory
#-------------------------------------------------
macx {
    DESTDIR = mac
    OBJECTS_DIR = mac/obj/
    MOC_DIR = mac/moc/
#    UI_DIR = mac
#    RCC_DIR = mac
}
unix: !macx {
    DESTDIR = linux
    OBJECTS_DIR = linux/obj/
    MOC_DIR = linux/moc/
#    UI_DIR = linux
#    RCC_DIR = linux
}
win32 {
    DESTDIR = windows
    OBJECTS_DIR = windows/obj/
    MOC_DIR = windows/moc/
#    UI_DIR = windows
#    RCC_DIR = windows
}
