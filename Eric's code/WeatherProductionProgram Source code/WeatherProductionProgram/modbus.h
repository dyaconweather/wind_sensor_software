#ifndef _MODBUS_H_
#define _MODBUS_H_

#define OS_WIN32

unsigned char ToBin(char *buf);
int ToBinAddr(char *buf);
int PutChar(unsigned char c);
int WriteBuffer(unsigned char *buffer, int len);
void ClosePort(void);
void OpenPort(char *deviceName, int Baud, int ByteSize, int StopBits, int Parity);
int GetChar(unsigned char *c);
void ModBUS_AddCRC(unsigned char *buf, unsigned char *ptr);

#ifdef OS_WIN32
///////////////////////////////////////////////////////
//  IF BUILDING ON WINDOWS, IMPLEMENT WINDOWS VERSION
//  THE SERIAL PORT CLASS.
///////////////////////////////////////////////////////
#include <windows.h>

#else
////////////////////////////////////////////////////////////////
//  IF USING A POSIX OS, ONE THAT UNDSTANDS THE NOTION        /
//  OF A TERMINAL DEVICE (Linux,BSD,Mac OSX, Solaris, etc)   /
/////////////////////////////////////////////////////////////
#include <fcntl.h>
#include <termios.h>
#include <stdlib.h>
#include <inttypes.h>
#include <strings.h>
#include <unistd.h>
#define Sleep(x) usleep(x*1000)

#endif

// default defined baud rates
#ifdef OS_WIN32
// Use windows definitions
#define Baud300        CBR_300
#define Baud600        CBR_600
#define Baud1200       CBR_1200
#define Baud2400       CBR_2400
#define Baud4800       CBR_4800
#define Baud9600       CBR_9600
#define Baud19200      CBR_19200
#define Baud38400      CBR_38400
#define Baud57600      CBR_57600
#define Baud115200     CBR_115200
#else
// Use Posix definitions
#define Baud300        B300
#define Baud600        B600
#define Baud1200       B1200
#define Baud2400       B2400
#define Baud4800       B4800
#define Baud9600       B9600
#define Baud19200      B19200
#define Baud38400      B38400
#define Baud57600      B57600
#define Baud115200     B115200
#endif

// bytes sizes
#ifdef OS_WIN32
// windows byte defines
#define CS8            8
#define CS7            7
#define CS6            6
#define CS5            5
#else
// posix is already CS8 CS7 CS6 CS5 defined
#endif

// parity
#ifdef OS_WIN32
#define ParityEven      EVENPARITY
#define ParityOdd       ODDPARITY
#define ParityNone      NOPARITY
#else
#define ParityEven      PARENB
#define ParityOdd       PARENB | PARODD
#define ParityNone      0
#endif

// stop bit
#ifdef OS_WIN32
#define SB1             0
#define SB2             CSTOPB
#else
#define SB1             0
#define SB2             2
#endif

#endif
