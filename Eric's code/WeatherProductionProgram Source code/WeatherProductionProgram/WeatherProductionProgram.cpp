// WeatherProductionProgram.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;
#include "modbus.h"

static int modbusaddr;

void SetSerialNumber(void)
{
	unsigned char rsp;
	int rsplen;
	int timeout;
	unsigned char msg[12];
	unsigned int serialnumber;

	cout<<endl<<"Enter Serial Number: ";
	cin>>serialnumber;

	if(serialnumber < 0 || serialnumber > 0xFFFF)
	{
		cout<<"Invalid Serial Number"<<endl<<endl;
		return;
	}

	//message
	msg[0] = modbusaddr;	//address of unit
	msg[1] = 6;	//write single register
	msg[2] = 0;	//4 digit key
	msg[3] = 0;
	msg[4] = 0x75;
	msg[5] = 0xBF;
	msg[6] = 0;
	msg[7] = 1;	//set serial number code
	msg[8] = (unsigned char)(serialnumber >> 8);
	msg[9] = (unsigned char)serialnumber;

	//add CRC
	ModBUS_AddCRC(msg, msg+10);

	//Transmit
	WriteBuffer(msg, 12);

	//wait for response
	timeout = 200;
	rsplen = 0;
	while(1)
	{
		rsplen += GetChar(&rsp);
		if(rsplen == 1)
			break;
		if(--timeout == 0)
			break;
		Sleep(10);
	}

	if(rsp != 0 || timeout == 0)
	{
		//no response
		cout<<"Setting Failed"<<endl;
	}
	else
	{
		cout<<"Success"<<endl<<endl;
	}
}

void SetNorth(void)
{
	unsigned char rsp;
	int rsplen;
	int timeout;
	unsigned char msg[10];

	//message
	msg[0] = modbusaddr;	//address of unit
	msg[1] = 6;	//write single register
	msg[2] = 0;	//4 digit key
	msg[3] = 0;
	msg[4] = 0x75;
	msg[5] = 0xBF;
	msg[6] = 0;
	msg[7] = 2;	//set north

	//add CRC
	ModBUS_AddCRC(msg, msg+8);

	//Transmit
	WriteBuffer(msg, 10);

	//wait for response
	timeout = 200;
	rsplen = 0;
	while(1)
	{
		rsplen += GetChar(&rsp);
		if(rsplen == 1)
			break;
		if(--timeout == 0)
			break;
		Sleep(10);
	}

	if(rsp != 0 || timeout == 0)
	{
		//no response
		cout<<"Setting Failed"<<endl;
	}
	else
	{
		cout<<"Success"<<endl<<endl;
	}
}

void CalibrateStart(void)
{
	unsigned char rsp;
	int rsplen;
	int timeout;
	unsigned char msg[10];

	//message
	msg[0] = modbusaddr;	//address of unit
	msg[1] = 6;	//write single register
	msg[2] = 0;	//4 digit key
	msg[3] = 0;
	msg[4] = 0x75;
	msg[5] = 0xBF;
	msg[6] = 0;
	msg[7] = 3;	//start

	//add CRC
	ModBUS_AddCRC(msg, msg+8);

	//Transmit
	WriteBuffer(msg, 10);

	//wait for response
	timeout = 200;
	rsplen = 0;
	while(1)
	{
		rsplen += GetChar(&rsp);
		if(rsplen == 1)
			break;
		if(--timeout == 0)
			break;
		Sleep(10);
	}

	if(rsp != 0 || timeout == 0)
	{
		//no response
		cout<<"Setting Failed"<<endl;
	}
	else
	{
		cout<<"Success"<<endl<<endl;
	}
}

void CalibrateSave(int nist)
{
	unsigned char rsp;
	int rsplen;
	int timeout;
	unsigned char msg[16];

	//message
	msg[0] = modbusaddr;	//address of unit
	msg[1] = 6;	//write single register
	msg[2] = 0;	//4 digit key
	msg[3] = 0;
	msg[4] = 0x75;
	msg[5] = 0xBF;
	msg[6] = 0;
	msg[7] = 4;	//save

	msg[8] = nist;
	msg[9] = 0;

	//save cal date
	time_t rawtime;
	struct tm * timeinfo;
	time (&rawtime);
	timeinfo = localtime(&rawtime);
	
	long tm;
	tm = timeinfo->tm_year + 1900;
	tm *= 100;
	tm += timeinfo->tm_mon + 1;
	tm *= 100;
	tm += timeinfo->tm_mday;

	msg[10] = (unsigned char)(tm >> 16);
	msg[11] = (unsigned char)(tm >> 24);
	msg[12] = (unsigned char)tm;
	msg[13] = (unsigned char)(tm >> 8);

	//add CRC
	ModBUS_AddCRC(msg, msg+14);

	//Transmit
	WriteBuffer(msg, 16);

	//wait for response
	timeout = 200;
	rsplen = 0;
	while(1)
	{
		rsplen += GetChar(&rsp);
		if(rsplen == 1)
			break;
		if(--timeout == 0)
			break;
		Sleep(10);
	}

	if(rsp != 0 || timeout == 0)
	{
		//no response
		cout<<"Setting Failed"<<endl;
	}
	else
	{
		cout<<"Success"<<endl<<endl;
	}
}

void CalibrateClear(void)
{
	unsigned char rsp;
	int rsplen;
	int timeout;
	unsigned char msg[10];

	//message
	msg[0] = modbusaddr;	//address of unit
	msg[1] = 6;	//write single register
	msg[2] = 0;	//4 digit key
	msg[3] = 0;
	msg[4] = 0x75;
	msg[5] = 0xBF;
	msg[6] = 0;
	msg[7] = 5;	//clear

	//add CRC
	ModBUS_AddCRC(msg, msg+8);

	//Transmit
	WriteBuffer(msg, 10);

	//wait for response
	timeout = 200;
	rsplen = 0;
	while(1)
	{
		rsplen += GetChar(&rsp);
		if(rsplen == 1)
			break;
		if(--timeout == 0)
			break;
		Sleep(10);
	}

	if(rsp != 0 || timeout == 0)
	{
		//no response
		cout<<"Setting Failed"<<endl;
	}
	else
	{
		cout<<"Success"<<endl<<endl;
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	char* comport;
	int parity;
	int baud;

	if(argc != 5)
	{
		cout<<"usage: "<<argv[0]<<" {MODBUS Address} COMx {baud} {parity}"<<endl;
		cout<<"example: "<<argv[0]<<" 1 COM1 19200 N"<<endl;
		return -1;
	}
	modbusaddr = atoi(argv[1]);
	comport = argv[2];
	baud = atoi(argv[3]);
	switch(argv[4][0])
	{
	default: 
		parity = ParityNone;
		break;
	case 'o':
	case 'O':
		parity = ParityOdd;
		break;
	case 'e':
	case 'E':
		parity = ParityEven;
		break;
	}

	if(modbusaddr < 1 || modbusaddr > 255)
	{
		cout<<"Invalid MODBUS address."<<endl;
		return -2;
	}

	//Open port
	#ifdef OS_WIN32
		OpenPort(comport, baud, CS8, SB1, parity);
	#else
		OpenPort(comport, baud, CS8, SB1, parity);
	#endif

	//Menu Loop
	while(1)
	{
		char input;

		cout<<"1 - Program Serial Number"<<endl;
		cout<<"2 - Set North"<<endl;
		cout<<"3 - Start Calibration"<<endl;
		cout<<"4 - Save Calibration"<<endl;
		cout<<"5 - Clear Calibration"<<endl;
		cout<<"E - Exit"<<endl;
		cout<<"Enter Selection: ";
		cin>>input;

		switch(input)
		{
		case '1':
			SetSerialNumber();
			break;
		case '2':
			SetNorth();
			break;
		case '3':
			CalibrateStart();
			break;
		case '4':
			cout<<"Is this NIST Traceable (y/n)? ";
			cin>>input;
			if(input == 'y' || input == 'Y')
				CalibrateSave(1);
			else
				CalibrateSave(0);
			break;
		case '5':
			CalibrateClear();
			break;
		case 'E':
		case 'e':
			ClosePort();
			return 0;
		}
	}
	
	return 0;
}
