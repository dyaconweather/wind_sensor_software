// WeatherProductionProgramSDI12.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;
#include "serial.h"

static char sdi12addr;

void SetSerialNumber(void)
{
	unsigned char rsp;
	int rsplen;
	int timeout;
	unsigned char msg[12];
	unsigned int serialnumber;

	cout<<endl<<"Enter Serial Number: ";
	cin>>serialnumber;

	if(serialnumber < 0 || serialnumber > 0xFFFF)
	{
		cout<<"Invalid Serial Number"<<endl<<endl;
		return;
	}

	//clear input
	while(GetChar(&rsp) == 1)
		;	//keep reading

	//wake device
	SendBreak();
	Sleep(5);
	SendBreak();
	Sleep(5);
	SendBreak();
	Sleep(5);

	//message
	msg[0] = sdi12addr;	//address of unit
	msg[1] = 'B';
	msg[2] = 0x0D;		//4 digit key
	msg[3] = 0x01;
	msg[4] = 0x25;
	msg[5] = 0x5A;
	msg[6] = (unsigned char)(serialnumber >> 12) & 0x3F;
	msg[7] = (unsigned char)(serialnumber >> 6) & 0x3F;
	msg[8] = (unsigned char)serialnumber & 0x3F;
	msg[9] = '!';		//termination character

	//add parity to address		
	unsigned char c, parity;
	for(int k=0;k<10;k++)
	{
		c = msg[k];	
		parity = 0;
		for(int i=0;i<7;i++)
		{
			c <<= 1;
			parity ^= c & 0x80;
		}
		msg[k] += parity;
	}

	//Transmit
	WriteBuffer(msg, 10);

	//wait for response
	timeout = 200;
	rsplen = 0;
	while(1)
	{
		rsplen += GetChar(&rsp);
		if(rsplen == 1)
			break;
		if(--timeout == 0)
			break;
		Sleep(10);
	}

	if((rsp & 0x7F) != sdi12addr || timeout == 0)
	{
		//no response
		cout<<"Setting Failed"<<endl;
	}
	else
	{
		cout<<"Success"<<endl<<endl;
	}
}

void SetNorth(void)
{
	unsigned char rsp;
	int rsplen;
	int timeout;
	unsigned char msg[10];

	//clear input
	while(GetChar(&rsp) == 1)
		;	//keep reading

	//wake device
	SendBreak();
	Sleep(5);
	SendBreak();
	Sleep(5);
	SendBreak();
	Sleep(5);

	//message
	msg[0] = sdi12addr;	//address of unit
	msg[1] = 'B';
	msg[2] = 0x0E;		//4 digit key
	msg[3] = 0x01;
	msg[4] = 0x25;
	msg[5] = 0x5A;
	msg[6] = '!';		//termination character

	//add parity to address		
	unsigned char c, parity;
	for(int k=0;k<7;k++)
	{
		c = msg[k];	
		parity = 0;
		for(int i=0;i<7;i++)
		{
			c <<= 1;
			parity ^= c & 0x80;
		}
		msg[k] += parity;
	}

	//Transmit
	WriteBuffer(msg, 7);

	//wait for response
	timeout = 200;
	rsplen = 0;
	while(1)
	{
		rsplen += GetChar(&rsp);
		if(rsplen == 1)
			break;
		if(--timeout == 0)
			break;
		Sleep(10);
	}

	if((rsp & 0x7F) != sdi12addr || timeout == 0)
	{
		//no response
		cout<<"Setting Failed"<<endl;
	}
	else
	{
		cout<<"Success"<<endl<<endl;
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	char* comport;

	if(argc != 3)
	{
		cout<<"usage: "<<argv[0]<<" {SDI-12 Address} COMx"<<endl;
		cout<<"example: "<<argv[0]<<" 0 COM1"<<endl;
		return -1;
	}
	sdi12addr = argv[1][0];
	comport = argv[2];

	//Open port
	#ifdef OS_WIN32
		OpenPort(comport, Baud1200, CS8, SB1, ParityNone);
	#else
		OpenPort(comport, baud, CS8, SB1, parity);
	#endif

	cout<<"Address = "<<sdi12addr<<endl<<endl;

	//Menu Loop
	while(1)
	{
		char input;

		cout<<"1 - Program Serial Number"<<endl;
		cout<<"2 - Set North"<<endl;
		cout<<"E - Exit"<<endl;
		cout<<"Enter Selection: ";
		cin>>input;

		switch(input)
		{
		case '1':
			SetSerialNumber();
			break;
		case '2':
			SetNorth();
			break;
		case 'E':
		case 'e':
			ClosePort();
			return 0;
		}
	}
	
	return 0;
}
