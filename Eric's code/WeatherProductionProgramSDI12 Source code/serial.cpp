//Define OS_WIN32 to compile for windows (Visual Studio C++)
#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;
#include "serial.h"

#ifdef OS_WIN32
    static HANDLE hSerial;
    // windows uses a struct called DCB to hold serial port configuration information
    static DCB dcbSerialParams;
#else
    static int sfd;
    // termio structs
    static struct termios oldtio, newtio;
#endif

unsigned char ToBin(char *buf)
{
	unsigned char val;

	//upper nibble
	if(*buf >= '0' && *buf <= '9')
		val = *buf - '0';
	else if(*buf >= 'A' && *buf <= 'F')
		val = *buf - 'A' + 10;
	else if(*buf >= 'a' && *buf <= 'f')
		val = *buf - 'a' + 10;
	val <<= 4;

	//lower nibble
	buf++;
	if(*buf >= '0' && *buf <= '9')
		val += *buf - '0';
	else if(*buf >= 'A' && *buf <= 'F')
		val += *buf - 'A' + 10;
	else if(*buf >= 'a' && *buf <= 'f')
		val += *buf - 'a' + 10;

	return val;
}

int ToBinAddr(char *buf)
{
	int val;

	val = ToBin(buf);
	val <<= 8;
	val += ToBin(buf+2);
	
	return val;
}

int PutChar(unsigned char c)
{
	#ifdef OS_WIN32
		int dwBytesRead = 0;
		if (hSerial != INVALID_HANDLE_VALUE) 
		{
			// have a valid file discriptor
			WriteFile(hSerial, &c, 1, (DWORD *)&dwBytesRead, NULL);
			return dwBytesRead;
	#else
		if (sfd != 0) 
		{
			// have a valid file discriptor
			return write(sfd, &c, 1);
	#endif
    }
	else
	{
        return -1;
    }
}

int WriteBuffer(unsigned char *buffer, int len) 
{
	#ifdef OS_WIN32
		int dwBytesRead = 0;
		if (hSerial != INVALID_HANDLE_VALUE) 
		{
			// have a valid file discriptor
			WriteFile(hSerial, buffer, len, (DWORD *)&dwBytesRead, NULL);
			return dwBytesRead;
	#else
		if (sfd != 0) 
		{
			// have a valid file discriptor
			return write(sfd, buffer, len);
	#endif
    }
	else
	{
        return -1;
    }
}

void ClosePort(void)
{
#ifdef OS_WIN32
	if(hSerial)
		CloseHandle(hSerial);
	hSerial = NULL;
#else
	close(sfd);
#endif
}

// setup what device we should use
void OpenPort(char *deviceName, int Baud, int ByteSize, int StopBits, int Parity)
{
	#ifdef OS_WIN32
		hSerial = CreateFile(deviceName, GENERIC_WRITE | GENERIC_READ, 0, 0, OPEN_EXISTING, 0 , NULL);
		if ( hSerial == INVALID_HANDLE_VALUE ) 
		{
			 DWORD err = GetLastError();
			cout<<"Port Open Failed: "<<deviceName<<endl;
			exit(-1);
		}
	#else
		sfd = open( deviceName, O_RDWR | O_NOCTTY | O_NONBLOCK);
		if ( sfd < 0 ) 
		{
			cout<<"Port Open Failed: "<<deviceName<<endl;
			exit(-1);
		}
	#endif

    // now save current device/terminal settings
	#ifdef OS_WIN32
		dcbSerialParams.DCBlength=sizeof(dcbSerialParams);
		if (!GetCommState(hSerial, &dcbSerialParams)) 
		{
			cout<<"Failed to get com port paramters"<<endl;
			exit(-2);
		}
	#else
		tcgetattr(sfd,&oldtio);
		// setup new terminal settings
		bzero(&newtio, sizeof(newtio));
		newtio.c_cflag = Baud | ByteSize | StopBits | Parity | CREAD | CLOCAL; // enable rx, ignore flowcontrol

		newtio.c_iflag = IGNPAR;
		newtio.c_oflag = 0;
		newtio.c_lflag = 0;
		newtio.c_cc[VTIME]    = 0;   /* inter-character timer unused */
		newtio.c_cc[VMIN]     = 1;   /* blocking read until atleast 1 charactors received */
		// flush device buffer
		tcflush(sfd, TCIFLUSH);
		// set new terminal settings to the device
		tcsetattr(sfd,TCSANOW,&newtio);
		// ok serial port setup and ready for use
	#endif

	#ifdef OS_WIN32
		//set up for timeout
		COMMTIMEOUTS CommTimeOuts;
		CommTimeOuts.ReadIntervalTimeout         = MAXDWORD;	//Blocking on the read will also block the write, so let it timeout quickly
		CommTimeOuts.ReadTotalTimeoutMultiplier  = 0;
		CommTimeOuts.ReadTotalTimeoutConstant    = 0;
		CommTimeOuts.WriteTotalTimeoutMultiplier = 2*CBR_9600/Baud;
		CommTimeOuts.WriteTotalTimeoutConstant   = 0 ;

		if(SetCommTimeouts(hSerial, &CommTimeOuts) == FALSE)
		{
			cout<<"Failed to set new com port timeouts"<<endl;
			exit(-3);
		}

		dcbSerialParams.BaudRate=Baud;
		dcbSerialParams.ByteSize=ByteSize;
		dcbSerialParams.Parity=Parity;
		dcbSerialParams.StopBits=StopBits;

		if(!SetCommState(hSerial, &dcbSerialParams)) 
		{
			cout<<"Failed to set new com port paramters"<<endl;
			exit(-4);
		}
	#endif
}

// data fetcher, get next byte from buffer
int GetChar(unsigned char *c) 
{
	#ifdef OS_WIN32
		int dwBytesRead;
		ReadFile(hSerial, (void *)c, 1, (DWORD *)&dwBytesRead, NULL); // reading 1 byte at a time
		return dwBytesRead;
	#else
		if(read(sfd, (void *)c, 1) == -1) // reading 1 byte at a time
			return 0;
		return 1;
	#endif
}

void SendBreak(void)
{
	SetCommBreak(hSerial);
	Sleep(13);
	ClearCommBreak(hSerial);
}
