#ifndef _SPI_H_
#define _SPI_H_

void SPI_Init(void);
unsigned int SPI_Transfer(unsigned long send);

#endif
