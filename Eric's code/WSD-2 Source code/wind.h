#ifndef _WIND_H_
#define _WIND_H_

void Wind_Init(void);
void Wind_isr(void);
void Wind_Timer_isr(void);
void Wind_Get(unsigned long *wind);
void Wind_GetRaw(unsigned long *w1);


#endif
