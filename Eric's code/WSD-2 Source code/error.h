#ifndef _ERROR_H_
#define _ERROR_H_

#define ERROR_DIR_INVALID	0x0001
#define ERROR_DIR_ERROR		0x0002
#define ERROR_DIR_FIELDHIGH	0x0004
#define ERROR_DIR_FIELDLOW	0x0008

void Error_Init(void);
void ErrorSet(unsigned int err);
void ErrorClear(unsigned int err);
unsigned int ErrorGet(void);

#endif
