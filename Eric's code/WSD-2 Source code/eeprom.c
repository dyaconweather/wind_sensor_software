#include <p24Fxxxx.h>
#include "eeprom.h"

#define START_ADDR	0x7FFE00

void EE_Init(void)
{

}

void EE_EraseAll(void)
{
	// Set up NVMCON to bulk erase the data EEPROM
	NVMCON = 0x4050;
	// Disable Interrupts For 5 Instructions
	asm volatile ("disi #5");
	// Issue Unlock Sequence and Start Erase Cycle
	__builtin_write_NVM();
}

int EE_Erase(int addr, int len)
{
	int i;
	unsigned int offset;
	unsigned long EE_addr;

	//check address range
	if((addr+len) > 256)
		return 0;

	//setup erase before write
	NVMCONbits.PGMONLY = 0;

	//setup EEPROM address
	EE_addr = (unsigned long)addr*2 + START_ADDR;

	for(i=0;i<len;i++)
	{
		// Set up NVMCON to erase one word of data EEPROM
		NVMCON = 0x4058;

		// Set up a pointer to the EEPROM location to be erased
		TBLPAG = EE_addr>>16; 			// Initialize EE Data page pointer
		offset = EE_addr & 0xFFFF; 		// Initialize lower word of address
	
		__builtin_tblwtl(offset, 0); 	// Write EEPROM data to write latch
	
		asm volatile ("disi #5"); 		// Disable Interrupts For 5 Instructions
		__builtin_write_NVM(); 			// Issue Unlock Sequence & Start Write Cycle
		while(NVMCONbits.WR == 1); 		// Poll WR bit to wait for sequence to complete

		EE_addr += 2;
	}

	return i;
}

int EE_Write(int addr, int len, int *data)
{
	int i;
	unsigned int offset;
	unsigned long EE_addr;

	//check address range
	if((addr+len) > 256)
		return 0;

	//setup EEPROM address
	EE_addr = (unsigned long)addr*2 + START_ADDR;

	for(i=0;i<len;i++)
	{
		// Set up NVMCON to write one word of data EEPROM
		NVMCON = 0x4004;

		// Set up a pointer to the EEPROM location to be written
		TBLPAG = EE_addr>>16; 				// Initialize EE Data page pointer
		offset = EE_addr & 0xFFFF; 			// Initialize lower word of address
		__builtin_tblwtl(offset, *data); 	// Write EEPROM data to write latch

		asm volatile ("disi #5"); 			// Disable Interrupts For 5 Instructions
		__builtin_write_NVM(); 				// Issue Unlock Sequence & Start Write Cycle
		while(NVMCONbits.WR == 1); 			// Poll WR bit to wait for sequence to complete

		// Move to next location
		EE_addr += 2;
		data++;
	}

	return i;
}

int EE_Read(int addr, int len, int *data)
{
	int i;
	unsigned int offset;
	unsigned long EE_addr;

	//check address range
	if((addr+len) > 256)
		return 0;

	//setup EEPROM address
	EE_addr = (unsigned long)addr*2 + START_ADDR;

	for(i=0;i<len;i++)
	{
		// Set up a pointer to the EEPROM location to be read
		TBLPAG = EE_addr>>16; 			// Initialize EE Data page pointer
		offset = EE_addr & 0xFFFF; 		// Initialize lower word of address

		// Read the EEPROM data
		*data = __builtin_tblrdl(offset);

		// Move to next location
		EE_addr += 2;
		data++;
	}

	return i;
}

