#include <p24Fxxxx.h>
#include "config.h"
#include "timer.h"
#include "spi.h"
#include "direction.h"
#include "wind.h"
#include <stdio.h>
#include "sdi12.h"
#include "eeprom.h"
#include "error.h"
#include "settings.h"
#include "sensor.h"

#if (defined(RS485) && defined(SDI12)) || (!defined(RS485) && !defined(SDI12) && !defined(MODBUS))
    #error "Must choose one protocol."
#endif

int main(void)
{
#ifndef __DEBUG
	//enable watchdog
	RCONbits.SWDTEN = 1;
#else
	//disable watchdog
	RCONbits.SWDTEN = 0;
#endif

	//setup I/O
	ANSA = 0x0000;
	LATA = 0x0000;
	TRISA = 0x005C;

	ANSB = 0x0000;
#if defined(RS485) || defined(MODBUS)
	LATB = 0x8002;
#endif
#ifdef SDI12
	LATB = 0x8003;
#endif
	TRISB = 0x5094;

	//Configure modules enabled
	PMD1 = 0xC0D1;	//TMR1, TMR2, TMR3, SPI1, U1
	PMD2 = 0x0407;	//IC1, IC2
	PMD3 = 0x0682;	//All Off
	PMD4 = 0x008E;	//EEProm

	//setup stuff
	Timer_Init();
	SPI_Init();
	EE_Init();
	Setting_Init();
	Dir_Init();
	Wind_Init();
	SDI12_Init();

	while(1)
	{
		ClrWdt();

		if(LowPowerMode() > 0 && T3CONbits.TON == 0 && IEC0bits.U1TXIE == 0 && Timer_Check() == 1)
		{
			if(U1STAbits.RIDLE)	//if we are receiving a character, don't go to sleep
			{
				SDI12_SetBlock();
			}
			else
			{
				Timer_Start(LowPowerMode());
			}
		}

		Idle();
		
		//see if we need to read the direction
		Dir_Process();

		//Process stack
		SDI12_Process();
	}

	return 0;
}

