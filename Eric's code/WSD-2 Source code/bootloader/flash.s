.global _blReadPMHigh
.global _blReadPMLow
.global _blResetStack

.section .bootloader, code
_blReadPMHigh:
	tblrdh [W0],W0
	return

_blReadPMLow:
	tblrdl [W0],W0
	return		

.end
