//NOTE: this must not call anything that is not located in protected memory, basically anything not in this file.
//Make sure constants also sit in the bootloader memory range.
//You CANNOT modify configuration from the bootloader. You must use a hardware programmer for that.
#include <p24Fxxxx.h>

#ifdef __DEBUG
_FBS(BWRP_OFF & BSS_OFF);
_FGS(GWRP_OFF & GSS0_OFF);
#else
_FBS(BWRP_OFF & BSS_STD1K);
_FGS(GWRP_OFF & GSS0_ON);
#endif
_FWDT(WDTPS_PS2048 & FWDTEN_SWON & WINDIS_OFF);
_FOSCSEL(FNOSC_PRI & SOSCSRC_DIG & LPRCSEL_HP & IESO_ON);
_FOSC(POSCMOD_HS & OSCIOFNC_ON & POSCFREQ_MS & FCKSM_CSECME);
_FPOR(BOREN_BOR3 & PWRTEN_ON & I2C1SEL_PRI & BORV_V27 & MCLRE_ON);
_FICD(ICS_PGx3);
//_FDS()										// Comparator - not used

extern int  blReadPMHigh(int);
extern int  blReadPMLow(int);

#define NUM_INSTRUCTION_PER_ROW 32
#define TX_EN	LATBbits.LATB0

static void __attribute__((__section__(".bootloader"))) bl_ErasePM(unsigned long progAddr)	// Address of row to erase
{
	unsigned int offset;

	//Set up pointer to the first memory location to be written
	TBLPAG = progAddr>>16; 		// Initialize PM Page Boundary SFR
	offset = progAddr & 0xFFFF; // Initialize lower word of address
	__builtin_tblwtl(offset, 0x0000); // Set base address of erase block with dummy latch write

	NVMCON = 0x405A; // Initialize NVMCON
	asm("DISI #5"); // Block all interrupts with priority <7 for next 5 instructions
	__builtin_write_NVM(); // C30 function to perform unlock sequence and set WR
		
	//wait for competion
	while(NVMCONbits.WR == 1);
}

static void __attribute__((__section__(".bootloader"))) bl_WritePM(unsigned long progAddr, unsigned int *progData)	// Address of row to write, data to write
{
	unsigned int offset;
	unsigned int i;

	//Set up NVMCON for row programming
	NVMCON = 0x4004; // Initialize NVMCON

	//Set up pointer to the first memory location to be written
	TBLPAG = progAddr>>16; // Initialize PM Page Boundary SFR
	offset = progAddr & 0xFFFF; // Initialize lower word of address

	//Perform TBLWT instructions to write necessary number of latches
	for(i=0; i < 2*NUM_INSTRUCTION_PER_ROW; i++)
	{
		__builtin_tblwtl(offset, progData[i++]); // Write to address low word
		__builtin_tblwth(offset, progData[i]); // Write to upper byte
		offset = offset + 2; // Increment address
	}

	asm("DISI #5"); // Block all interrupts with priority < 7 for next 5 instructions

	__builtin_write_NVM(); // Perform unlock sequence and set WR

	//wait for competion
	while(NVMCONbits.WR == 1);
}

void __attribute__((__section__(".bootloader"))) GetChar(unsigned char * ptrChar)
{
	while(1)
	{	
		/* must clear the overrun error to keep uart receiving */
		if(U1STAbits.OERR == 1)
		{
			U1STAbits.OERR = 0;
			continue;
		}

		/* check for receive errors */
		if(U1STAbits.FERR == 1)
		{
			U1RXREG;
			continue;
		}

		if(U1STAbits.PERR == 1)
		{
			U1RXREG;
			continue;
		}
			
		/* get the data */
		if(U1STAbits.URXDA == 1)
		{
			*ptrChar = U1RXREG;
			break;
		}
	}
}

void __attribute__((__section__(".bootloader"))) PutChar(char Char)
{
	volatile int i;
	unsigned char c;

	while(!U1STAbits.TRMT);

	TX_EN = 0;
	for(i=0;i<50;i++);	//delay	for tranceiver to switch on
	U1TXREG = Char;
	for(i=0;i<200;i++);	
	TX_EN = 1;

	//read the character sent since it is RS485
	GetChar(&c);
}

void __attribute__((__section__(".bootloader"))) BootLoader(void)
{
	ANSA = 0x0000;
	ANSB = 0x0000;

	// Setup UART
	U1MODE = 0x0000;

	//Set baud rate for 38400
	U1MODEbits.BRGH = 1;
	U1BRG = 25;

	// Setup driver pin
	TX_EN = 1;
	TRISBbits.TRISB0 = 0;

	//Disable Interrupts - we are polling only
	IEC0bits.U1RXIE = 0;
	IEC4bits.U1ERIE = 0;
	IEC0bits.U1TXIE = 0;
	//Turn On	
	U1MODEbits.UARTEN = 1;	//Turn the peripheral on
	U1STAbits.UTXEN = 1;	//Turn on TX

	while(1)
	{
		//read command
		unsigned char Command;

		GetChar(&Command);

		switch(Command)
		{
			default:
			case 0x00:	//reset
			{
				asm("reset");
				break;
			}

			case 0x01:	//bl check
			{
				PutChar(0x35);
				break;
			}

			case 0x02:	//program
			{
				unsigned long addr;
				unsigned int chksum_in;
				unsigned char buf[512];		//128 instructions
				int i;
				unsigned char c;

				//receive address
				GetChar(&c);
				addr = c;
				addr <<= 8;
				GetChar(&c);
				addr += c;
		
				//receive checksum
				GetChar(&c);
				chksum_in = c;
				chksum_in <<= 8;
				GetChar(&c);
				chksum_in += c;
				
				//receive data
				unsigned int chksum = 0;
				for(i=0;i<512;i++)
				{
					GetChar(buf+i);
					chksum += buf[i];
				}

				//check checksum;
				if(chksum != chksum_in)
				{
					PutChar(0xF1);
					break;
				}

				//check memory range isn't in bootloader range
				if(addr >= 0x200 && addr < 0xB00)
				{
					PutChar(0xF2);
					break;
				}

				//erase memory
				bl_ErasePM(addr);
	
				//program memory
				bl_WritePM(addr, ((unsigned int*)buf));

				//if we erased vector table, we also want to erase the last memory location to clear the magic num.
				if(addr == 0)
					bl_ErasePM(0x5780);
					
				bl_WritePM(addr+64, ((unsigned int*)buf)+64);
				bl_WritePM(addr+128, ((unsigned int*)buf)+128);
				bl_WritePM(addr+192, ((unsigned int*)buf)+192);

				PutChar(0xF0);
				break;
			}
		}
	}

	asm("reset");
}

extern void _resetProg(void);
int __attribute__((__section__(".bootloader"))) bootloader_main(void)
{
	//check for invalid program. We do this by looking for the magic number in the last memory location. It is the last thing written
	TBLPAG = 0;
	if(blReadPMLow(0x57FE) != 0x483C)
	{
		//disable watchdog
		RCONbits.SWDTEN = 0;
		//run bootloader
		BootLoader();
	}

	_resetProg();
	return 0;
}

//This is located at a certain address so we can jump here from our running program
void __attribute__((address(0xAD0))) StartBootLoader(void)
{
	//stop interrupts
	SET_CPU_IPL(7);
	//disable watchdog
	RCONbits.SWDTEN = 0;
	//Reset the stack
	//WARNING - Don't ever return from this function!
	asm("mov      #__SP_init,w15");
	//start bootloader
	BootLoader();
}

//Place a magic number to verify the entire program space is written
const unsigned int __attribute__ ((space(psv),address (0x57FE))) MagicNum = 0x483C;
