#ifndef _DIRECTION_H_
#define _DIRECTION_H_

void Dir_Init(void);
void Dir_Process(void);
int Dir_Get(int *angle);
int Dir_GetError(void);
void Dir_ProcessStart(void);
int Dir_Ready(void);

#endif
