#ifndef _EEPROM_H_
#define _EEPROM_H_

void EE_Init(void);
int EE_Read(int addr, int len, int *data);
int EE_Write(int addr, int len, int *data);
void EE_EraseAll(void);

#define EEPROM_VERSION	1

#define EEPROM_VERSION_LOCATION		0
#define EEPROM_DIRECTION_ZERO		1
#define EEPROM_SETTING_LOCATION		2

#endif
