#include <p24Fxxxx.h>
#include "direction.h"
#include "spi.h"
#include <math.h>
#include "error.h"

#define CSn		LATBbits.LATB15
#define AVG_NUM	20

static double x[AVG_NUM], y[AVG_NUM];
static int error[AVG_NUM];
static int ptr;
static int start = 0;

void Dir_Init(void)
{
	int i;

	//deselect
	CSn = 1;

	//clear data
	for(i=0;i<AVG_NUM;i++)
	{
		x[i] = 0;
		y[i] = 0;
	}

	ptr = 0;	
	start = 0;
}

void Dir_ProcessStart(void)
{
	start = 1;
}

void Dir_Process(void)
{
	unsigned int datain;
	unsigned int parity;
	int paritybit;
	volatile int i;
	double rad;

	if(start == 0)
		return;
	start = 0;

	//read angle
	CSn = 0;
	datain = SPI_Transfer(0);
	CSn = 1;

	//check for errors
	error[ptr] = 0;		//no error
	parity = datain >> 1;
	paritybit = 0;
	for(i=0;i<15;i++)
	{
		paritybit ^= (parity & 0x0001);
		parity >>= 1;
	}

	if(paritybit != (datain & 0x0001))
		error[ptr] |= ERROR_DIR_INVALID;		//invalid read
	if((datain & 0x0002) == 0x0002)
		error[ptr] |= ERROR_DIR_ERROR;			//error flag set
	if((datain & 0x4000) == 0x4000)
		error[ptr] |= ERROR_DIR_FIELDHIGH;		//field too high
	if((datain & 0x8000) == 0x8000)
		error[ptr] |= ERROR_DIR_FIELDLOW;		//field too low

    //Calcualte components
	//deg = (datain >>= 2) / 2.844444
	//rad = (double)deg * PI / 180.0;
	rad = (double)((datain >>= 2) & 0x03FF);
   	rad *= 0.006135923;
    x[ptr] = cos(rad);
    y[ptr] = sin(rad) * -1;	//change rotation direction to match compass

	ptr++;
	if(ptr >= AVG_NUM)
	{
		ptr = 0;
	}
}

int Dir_GetError(void)
{
	int err = 0;	
	int i;

	for(i=0;i<AVG_NUM;i++)
		err |= error[i];

	return err;
}

int Dir_Get(int *angle)
{
	double avgx, avgy;
	double rad;
	volatile int ang;
	int err = 0;	
	int i;

	//sum data
	avgx = 0;
	avgy = 0;
	for(i=0;i<AVG_NUM;i++)
	{
		err |= error[i];
		avgx += x[i];
		avgy += y[i];
	}
	avgx /= (double)AVG_NUM;
	avgy /= (double)AVG_NUM;

	rad = atan2(avgy, avgx);
//	angle = rad * 180.0 / PI * 10(so it is in 1/10 of degree)
	rad *= 572.9578;
	ang = (int)rad;

	//atan2 is from -pi to pi. Correct so its 0 to 2*pi
	if(ang < 0)
	    ang += 3600;

	*angle = ang;
	return err;
}
