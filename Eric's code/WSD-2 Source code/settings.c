#include <p24Fxxxx.h>
#include "settings.h"
#include "eeprom.h"
#include "direction.h"
#include <stdio.h>
#include <string.h> 

static double cal[3];
static char value_str[3][32];
static int value_len[3];

static int DirZero;
void Setting_Init(void)
{
	int data;
	double ddata;
	int i;

	//check for valid eeprom data
	EE_Read(EEPROM_VERSION_LOCATION, 1, &data);
	if(data != EEPROM_VERSION)
	{
		//Write eeprom version
		data = EEPROM_VERSION;
		EE_Write(EEPROM_VERSION_LOCATION, 1, &data);

		//Write Address	
		data = '0';		
		EE_Write(EEPROM_SETTING_LOCATION, 1, &data);

		//Skip Serial Number

		//Write in defaults
		ddata = 1;
		EE_Write(EEPROM_SETTING_LOCATION+2, 2, (int *)&ddata);

		ddata = 0;
		EE_Write(EEPROM_SETTING_LOCATION+4, 2, (int *)&ddata);
		EE_Write(EEPROM_SETTING_LOCATION+6, 2, (int *)&ddata);
	}

	//Load Direction Zero From EEPROM
	EE_Read(EEPROM_DIRECTION_ZERO, 1, &DirZero);
	if(DirZero < 0 || DirZero >= 3600)
		DirZero = 0;

	EE_Read(EEPROM_SETTING_LOCATION+2, 2, (int *)(&cal[0]));
	EE_Read(EEPROM_SETTING_LOCATION+4, 2, (int *)(&cal[1]));
	EE_Read(EEPROM_SETTING_LOCATION+6, 2, (int *)(&cal[2]));

	for(i=0;i<3;i++)
		value_len[i] = sprintf((char *)value_str[i], "%+G", cal[i]);
}

void SetNorth(void)
{
	int data;
	Dir_Get(&data);
	EE_Write(EEPROM_DIRECTION_ZERO, 1, &data);
}

int DirectionZero(void)
{
	return DirZero;
}

int LowPowerMode(void)
{
	return 105;
}

double WindSlope(void)
{
	return cal[0];
}

double WindOffset(void)
{
	return cal[1];
}

double DirectionOffset(void)
{
	return cal[2];
}

void WriteWindSlope(double value)
{
	cal[0] = value;
	EE_Write(EEPROM_SETTING_LOCATION+2, 2, (int *)(&cal[0]));
	value_len[0] = sprintf((char *)value_str[0], "%+G", cal[0]);
}

void WriteWindOffset(double value)
{
	cal[1] = value;
	EE_Write(EEPROM_SETTING_LOCATION+4, 2, (int *)(&cal[1]));
	value_len[1] = sprintf((char *)value_str[1], "%+G", cal[1]);
}

void WriteDirectionOffset(double value)
{
	cal[2] = value;
	EE_Write(EEPROM_SETTING_LOCATION+6, 2, (int *)(&cal[2]));
	value_len[2] = sprintf((char *)value_str[2], "%+G", cal[2]);
}

int WindSlopeStr(char **str)
{
	*str = value_str[0];
	return value_len[0];
}

int WindOffsetStr(char **str)
{
	*str = value_str[1];
	return value_len[1];
}

int DirectionStr(char **str)
{
	*str = value_str[2];
	return value_len[2];
}

