#ifndef _SETTINGS_H_
#define _SETTINGS_H_

void Setting_Init(void);
int LowPowerMode(void);

int DirectionZero(void);
double WindSlope(void);
double WindOffset(void);
double DirectionOffset(void);

void SetNorth(void);
void WriteWindSlope(double value);
void WriteWindOffset(double value);
void WriteDirectionOffset(double value);

int WindSlopeStr(char **str);
int WindOffsetStr(char **str);
int DirectionStr(char **str);

#endif
