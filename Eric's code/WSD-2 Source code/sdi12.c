#include <p24Fxxxx.h>
#include "sdi12.h"
#include "config.h"
#include "timer.h"
#include "eeprom.h"
#include "sensor.h"
#include "settings.h"
#include "error.h"
#include <stdio.h>
#include <string.h> 

#ifdef SDI12

#define MEASUREMENT_DELAY	90

static unsigned char our_address;

#define TX_EN	LATBbits.LATB0

#define RX_BUF_SIZE		32
#define RX_BUF_NUM		4
#define TX_BUF_SIZE		63		//needs to be 2^x - 1

static unsigned char rx_buf[RX_BUF_NUM][RX_BUF_SIZE];
static unsigned char rx_count[RX_BUF_NUM];
static unsigned char rx_ptr;
static unsigned char tx_buf[TX_BUF_SIZE+1];
static unsigned char tx_fptr, tx_bptr;
static unsigned char tx_inprocess;
static char discard = 0;
static char block = 1;		//used to block receiving until there is a break detected
static unsigned char ignore_tx = 0;

static int crcflag = 0;

static unsigned char ver_response[30];
static int ver_len = 0;
static unsigned char response[34];
static int response_length = 0;

extern void StartBootLoader(void);
void SDI12_Response(unsigned char *str, int len);

void SDI12_Init(void)
{
	int i;
	int data;
	
	//clear pointers
	tx_fptr = 0;
	tx_bptr = 0;
	rx_ptr = 0;
	for(i=0;i<RX_BUF_NUM;i++)
		rx_count[i] = 0;

	//Load settings from EEPROM
	EE_Read(EEPROM_SETTING_LOCATION, 1, &data);
	our_address = (unsigned char)data;

	//create version string
	//SDI-12 Compatability
	strcpy((char *)ver_response, "13");
	//Company
	strcpy((char *)ver_response+2, "DYACON  ");
	//Sensor Model Number
	ver_len = 10 + sprintf((char *)(ver_response+10), "%06d", PRODUCTID);
	//Sensor Version
	ver_len += sprintf((char *)(ver_response+ver_len), "%03d", FIRMWAREVERSION);
	//Serial Number
	EE_Read(EEPROM_SETTING_LOCATION+1, 1, &data);
	ver_len += sprintf((char *)(ver_response+ver_len), "%05u", (unsigned int)data);

	U1MODE = 0;
	U1STA = 0x2000;				//TX interrupt when FIFO empty
	IFS0bits.U1RXIF = 0;		//Clear Interrupt Flags
	IEC0bits.U1RXIE = 1;		//Enable Rx Interrupt
	IFS4bits.U1ERIF = 0;		//Clear Error Flag
	IEC4bits.U1ERIE = 1;		//Enable Error Interrupt
	IFS0bits.U1TXIF = 0;		//Clear Interrupt Flags
	U1MODEbits.PDSEL = 0;		//None
	U1BRG = 207;				//1200 baud

	U1MODEbits.USIDL = 0;	//module on in idle mode
	U1MODEbits.UARTEN = 1;	//Turn the peripheral on
	U1STAbits.UTXEN = 1;	//Turn on TX

	//Configure Timer 3 for end of rx packet
	T3CON = 0;              // Timer reset
 	IFS0bits.T3IF = 0;      // Reset Timer interrupt flag
	IPC2bits.T3IP = 3;      // Timer Interrupt priority
 	IEC0bits.T3IE = 1;      // Enable Timer interrupt
	T3CONbits.TSIDL = 0;	// Run in Idle
	T3CONbits.TCKPS = 1;	// 1:8 Prescale
 	TMR3 =  0x0000;  		// Clear counter
	PR3 = 5200;				// 10 ms between receipt of characters (8.33 min mark + 1.66)
}

static int SDI12_AddCRC(unsigned char *buf, int len)
{
	unsigned int CRC = 0;
	int i,k;

	//include address in CRC
	CRC ^= our_address;
	for(k=0;k<8;k++)
	{
		if(CRC & 0x0001)
		{
			CRC >>= 1;
			CRC ^= 0xA001;
		}
		else
		{
			CRC >>= 1;
		}
	}

	//process packet
	for(i=0;i<len;i++)
	{
		CRC ^= buf[i];
		
		for(k=0;k<8;k++)
		{
			if(CRC & 0x0001)
			{
				CRC >>= 1;
				CRC ^= 0xA001;
			}
			else
			{
				CRC >>= 1;
			}
		}
	}

	//add to data
	buf[len++] = 0x40 | (CRC >> 12);
	buf[len++] = 0x40 | ((CRC >> 6) & 0x3F);
	buf[len++] = 0x40 | (CRC & 0x3F);
	return len;
}

void SDI12_Process(void)
{
	int i;

	//check for ready message.
	for(i=0;i<RX_BUF_NUM;i++)
		if(rx_count[i] != 0 && rx_ptr != i)
			break;

	if(i == RX_BUF_NUM)
		return;		//no message to process

//process message
	//Check for minimum length
	if(rx_count[i] < 2)
		goto Process_Exit;

	//delay so bus can be idle
	Delay_ms(5);

	//Check for our address	or wild card
	if(rx_buf[i][0] != our_address && rx_buf[i][0] != '?')
	{
		block = 1;
		goto Process_Exit;
	}

	LATBbits.LATB1 = 0;

	//Decode Function Code
	switch(rx_buf[i][1])
	{
	case '!':
		//Acknowlege Active
		SDI12_Response(0,0);
		break;

	case 'I':
		//Send Identification
		SDI12_Response(ver_response, ver_len);
		break;

	case 'A':
		{
			if(rx_count[i] == 4 && ((rx_buf[i][2] >= '0' && rx_buf[i][2] <= '9') || (rx_buf[i][2] >= 'A' && rx_buf[i][2] <= 'Z') || (rx_buf[i][2] >= 'a' && rx_buf[i][2] <= 'z')))
			{
				int data;
				//Change address
				our_address = rx_buf[i][2];
				//save it
				data = our_address;
				EE_Write(EEPROM_SETTING_LOCATION, 1, &data);
				//response
				SDI12_Response(0,0);
			}
		}
		break;

	case 'M':
		//Check for CRC request
		if(rx_buf[i][2] == 'C')
		{
			crcflag = 1;

			//Check for additional measurements
			if(rx_buf[i][3] >= '1' && rx_buf[i][3] <= '9')
			{
				SDI12_Response((unsigned char *)"0000",4);
				break;
			}
		}
		else
		{
			crcflag = 0;

			//Check for additional measurements
			if(rx_buf[i][2] >= '1' && rx_buf[i][2] <= '9')
			{
				SDI12_Response((unsigned char *)"0000",4);
				break;
			}
		}

		//send response
		SDI12_Response((unsigned char *)"0002",4);
		
		//make measurement
		response_length = 0;
		SensorRead();
		break;

	case 'C':
		//Check for CRC request
		if(rx_buf[i][2] == 'C')
		{
			crcflag = 1;

			//Check for additional measurements
			if(rx_buf[i][3] >= '1' && rx_buf[i][3] <= '9')
			{
				SDI12_Response((unsigned char *)"00000",5);
				break;
			}
		}
		else
		{
			crcflag = 0;

			//Check for additional measurements
			if(rx_buf[i][2] >= '1' && rx_buf[i][2] <= '9')
			{
				SDI12_Response((unsigned char *)"00000",5);
				break;
			}
		}

		//send response
		SDI12_Response((unsigned char *)"00002",5);
		
		//make measurement
		response_length = 0;
		SensorRead();
		break;

	case 'D':
		{
			int length;

			//check for additional data
			if(rx_buf[i][2] != '0')
			{
				SDI12_Response(0,0);
				break;
			}
	
			//add CRC if needed
			length = response_length;
			if(crcflag == 1)
				length = SDI12_AddCRC(response, response_length);
	
			//Send Data
			SDI12_Response(response, length);	
		}	
		break;

	case 'V':
		{
			//Verification
			SDI12_Response((unsigned char *)"0001",4);	
			response_length = sprintf((char *)response, "+%d", ErrorGet());
			crcflag = 0;
		}
		break;

	case 'R':
		//Check for CRC request
		if(rx_buf[i][2] == 'C')
		{
			unsigned char resp[3];
			SDI12_AddCRC(resp, 0);
			SDI12_Response(resp,3);
		}
		else
		{
			SDI12_Response(0,0);
		}
		break;

	case 'B':
		//check for bootloader
		if(rx_buf[i][2] == 0x0C && rx_buf[i][3] == 0x01 && rx_buf[i][4] == 0x25 && rx_buf[i][5] == 0x5A)
		{
			//Send response
			SDI12_Response(0,0);
			Delay_ms(100);
			StartBootLoader(); 
		}
		if(rx_buf[i][2] == 0x0D && rx_buf[i][3] == 0x01 && rx_buf[i][4] == 0x25 && rx_buf[i][5] == 0x5A)
		{
			unsigned int value;
			value = rx_buf[i][6] & 0x3f;
			value <<= 6;
			value += rx_buf[i][7] & 0x3f;
			value <<= 6;
			value += rx_buf[i][8] & 0x3f;
			EE_Write(EEPROM_SETTING_LOCATION+1, 1, (int *)&value);
			SDI12_Response(0,0);
		}
		if(rx_buf[i][2] == 0x0E && rx_buf[i][3] == 0x01 && rx_buf[i][4] == 0x25 && rx_buf[i][5] == 0x5A)
		{
			SetNorth();
			SDI12_Response(0,0);
		}
		break;

	case 'X':
		//check for unlock characters
		if(rx_buf[i][2] == 'W')
		{
			float value;

			SDI12_Response(0, 0);

			//write
			if(rx_buf[i][4] == '+')
				sscanf((char *)&rx_buf[i][5], "%f", &value);
			else
				sscanf((char *)&rx_buf[i][4], "%f", &value);

			switch(rx_buf[i][3])
			{
			case 'A':
				WriteWindSlope(value);
				break;
			case 'B':
				WriteWindOffset(value);
				break;
			case 'C':
				WriteDirectionOffset(value);
				break;
			}
		}
		else if(rx_buf[i][2] == 'R')
		{
			unsigned char *buf;
			int len = 0;

			//read
			switch(rx_buf[i][3])
			{
			case 'A':
				len = WindSlopeStr((char **)&buf);
				break;
			case 'B':
				len = WindOffsetStr((char **)&buf);
				break;
			case 'C':
				len = DirectionStr((char **)&buf);
				break;
			}

			SDI12_Response(buf, len);
		}
		break;
	}

	//set power down timer
	if(LowPowerMode())
		Timer_Start(LowPowerMode());

Process_Exit:
	//We are done, clear count to indicate buffer consumed	
	rx_count[i] = 0;
	LATBbits.LATB1 = 1;
}

void SDI12_Response(unsigned char *str, int len)
{
	int i;

	//enable driver
	TX_EN = 0;

	//send address
	if(((tx_fptr + 1) & TX_BUF_SIZE) == tx_bptr)
		return;		//no buffer space
	tx_buf[tx_fptr] = our_address;
	tx_fptr = (tx_fptr + 1) & TX_BUF_SIZE;

	for(i=0;i<len;i++)
	{
		if(((tx_fptr + 1) & TX_BUF_SIZE) == tx_bptr)
			break;		//no buffer space

		tx_buf[tx_fptr] = str[i];
		tx_fptr = (tx_fptr + 1) & TX_BUF_SIZE;
	}

	//end address
	if(((tx_fptr + 1) & TX_BUF_SIZE) == tx_bptr)
		return;		//no buffer space
	tx_buf[tx_fptr] = 0x0D;
	tx_fptr = (tx_fptr + 1) & TX_BUF_SIZE;
	if(((tx_fptr + 1) & TX_BUF_SIZE) == tx_bptr)
		return;		//no buffer space
	tx_buf[tx_fptr] = 0x0A;
	tx_fptr = (tx_fptr + 1) & TX_BUF_SIZE;

	IEC0bits.U1TXIE = 1;	//Enable Tx Interrupt
	tx_inprocess = 1;
}

static void Break_Detected(void)
{
	rx_count[rx_ptr] = 0;
	discard = 0;
	block = 0;
	
	//start marking timer
	TMR3 =  0x0000;  		// Clear counter
	T3CONbits.TON = 1;      // Enable Timer and start the counter
	
	//set power down timer
	if(LowPowerMode())
		Timer_Start(LowPowerMode());
}

void __attribute__ ((interrupt, no_auto_psv)) _U1ErrInterrupt(void)
{
	//clear interrupt
	IFS4bits.U1ERIF = 0;		

	if(U1STAbits.FERR)
	{

		if(U1STAbits.URXDA)
		{
			unsigned char c = U1RXREG;

			if(c == 0)
				Break_Detected();
		}
	}

	//clear overflow
	if(U1STAbits.OERR)
		U1STAbits.OERR = 0;
}

//Returns a 1 if parity is odd
static int check_parity(unsigned char data)
{
	data ^= (data >> 1);
	data ^= (data >> 2);
	data ^= (data >> 4);
	data &= 1;
	return data;
}

void __attribute__ ((interrupt, no_auto_psv)) _U1RXInterrupt(void)
{
	unsigned char c;

	//reset timeout interrupt
 	TMR3 =  0x0000;  		// Clear counter
 	T3CONbits.TON = 1;      // Enable Timer and start the counter

	while(U1STAbits.URXDA)
	{
		//read unsigned character
		c = U1RXREG; 
	
		if(ignore_tx)
		{
			ignore_tx--;
			continue;
		}

		//reject null character created by break
		if(c == 0 && rx_count[rx_ptr] == 0)
		{
			Break_Detected();
			continue;
		}

		if(block)
			continue;

		//check parity
		if(check_parity(c))
			discard = 1;

		//check for space
		if(rx_count[rx_ptr] >= RX_BUF_SIZE)
			continue;

		c &= 0x7F;
		rx_buf[rx_ptr][rx_count[rx_ptr]] = c;
		++rx_count[rx_ptr];

		if(c == '!')
		{
			//Termination found
			if(discard == 1)
			{
				discard = 0;
				rx_count[rx_ptr] = 0;		//throw away as there is no space
			}
			else if(rx_count[(rx_ptr + 1) & (RX_BUF_NUM - 1)] == 0)
			{
				rx_ptr = (rx_ptr + 1) & (RX_BUF_NUM - 1);	//move to next buffer
			}
			else
			{
				rx_count[rx_ptr] = 0;		//throw away as there is no space
			}
		}
	}

	//clear interrupt
	IFS0bits.U1RXIF = 0;
}

void __attribute__ ((interrupt, no_auto_psv)) _U1TXInterrupt(void)
{
	int count = 3;

	if(tx_fptr == tx_bptr)	//no more data
	{
		IEC0bits.U1TXIE = 0;	//Disable Tx Interrupt	

		//disable driver
		TX_EN = 1;

		//since we receive when we transmit, we need to clear the rx buffer
		rx_count[rx_ptr] = 0;
		tx_inprocess = 0;

		return;
	}

// Per rev 4 silicon errata, don't fill the buffer.
//	while(U1STAbits.UTXBF == 0)
	while(count--)
	{
		unsigned char c, parity = 0;
		int i;

		if(tx_fptr == tx_bptr)	//no more data
			break;

		c = tx_buf[tx_bptr];	
		for(i=0;i<7;i++)
		{
			c <<= 1;
			parity ^= c & 0x80;
		}

		U1TXREG = tx_buf[tx_bptr] + parity;
		tx_bptr = (tx_bptr + 1) & TX_BUF_SIZE;
		ignore_tx++;
	} 

	//clear interrupt
	IFS0bits.U1TXIF = 0;
}

void __attribute__((interrupt,no_auto_psv)) _T3Interrupt( void )
{
	IFS0bits.T3IF = 0;

	rx_count[rx_ptr] = 0;		//throw away
	discard = 0;

	//set power down timer
	if(LowPowerMode())
		Timer_Start(LowPowerMode());

 	T3CONbits.TON = 0;      // Disable timer
}

void SDI12_SetData(int speed, int direction)
{
	response_length = sprintf((char *)response, "%+0.1f%+0.1f", (double)speed/10.0, (double)direction/10.0);
}

void SDI12_SetBlock(void)
{
	block = 1;
}

#endif

