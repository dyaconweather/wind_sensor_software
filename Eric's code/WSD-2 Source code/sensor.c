#include <p24Fxxxx.h>
#include "sensor.h"
#include "wind.h"
#include "direction.h"
#include "sdi12.h"
#include "settings.h"
#include "error.h"
#include "timer.h"
#include "config.h"

#define COUNTS		500000.0
#define SLOPE		0.569250507
#define OFFSET		0.283435638

void SensorRead(void)
{
	int angle;
	int error;
	unsigned long wind;
	int iw;
	
	error = Dir_Get((int *)&angle);
	Wind_Get((unsigned long *)&wind);
	
	if(wind > 2600000L)
	{
		iw = 0;
	}
	else
	{
		double w, fw, hz;

		w = wind;
		hz = COUNTS/w;
		fw = hz * SLOPE + OFFSET;
		fw *= 10;
		fw += 0.5;	//round up

		//apply calibration
		if(WindSlope() != 1 || WindOffset() != 0)
			fw = WindSlope() * fw + WindOffset()*10;

		iw = (int)fw;
	}

	//apply zero
	if(DirectionZero() != 0)
		angle -= DirectionZero();
	
	//apply offset
	if(DirectionOffset() != 0)
		angle += DirectionOffset()*10;

	//check angle range
	while(angle < 0)
		angle += 3600;
	while(angle >= 3600)
		angle -= 3600;

	//set errors
	Error_Init();
	ErrorSet(error);

	//update holding registers
	SDI12_SetData(iw, angle);
}
