#include <p24Fxxxx.h>
#include "error.h"
#include "sdi12.h"

static unsigned int e;

void Error_Init(void)
{
	e = 0;
}

void ErrorSet(unsigned int err)
{
	e |= err;
}

void ErrorClear(unsigned int err)
{
	e &= ~err;
}

unsigned int ErrorGet(void)
{
	return e;
}
