#ifndef _SDI12_H_
#define _SDI12_H_

void SDI12_Init(void);
void SDI12_Process(void);
void SDI12_SetData(int speed, int direction);
void SDI12_SetBlock(void);

#endif
