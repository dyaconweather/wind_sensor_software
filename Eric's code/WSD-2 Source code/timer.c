#include <p24Fxxxx.h>
#include "timer.h"
#include "direction.h"

static volatile int tmr_delay;
static volatile int tmr;

void Timer_Init(void)
{
	tmr_delay = 0;
	tmr = 0;

	T2CON = 0;              // Timer reset
 	IFS0bits.T2IF = 0;      // Reset Timer interrupt flag
	IPC1bits.T2IP = 3;      // Timer Interrupt priority
 	IEC0bits.T2IE = 1;      // Enable Timer interrupt
	T2CONbits.TSIDL = 0;	// Run in Idle
	T2CONbits.TCKPS = 1;	// 1:8 Prescale
 	TMR2=  0x0000;  		// Clear counter
	PR2 = 500; 				// Timer period register
 	T2CONbits.TON = 1;      // Enable Timer and start the counter

//TRISAbits.TRISA6 = 0;
}

//Timer interrupts every 1 ms
void __attribute__((interrupt,no_auto_psv)) _T2Interrupt( void )
{
	static int dir_tmr = 0;
	static int blink = 0;

	IFS0bits.T2IF = 0;

//LATAbits.LATA6 ^= 1;

	if(tmr_delay > 0)
		--tmr_delay;

	if(tmr > 0)
		--tmr;

	if(++dir_tmr >= 50)
	{
		dir_tmr = 0;
		Dir_ProcessStart();
	}
	
	if(++blink >= 5000)
	{
		blink = 0;
		LATBbits.LATB1 = 0;
	}
	else
	{
		LATBbits.LATB1 = 1;
	}
}

void Timer_Start(unsigned int ms)
{
 	IEC0bits.T2IE = 0;      // Disable Timer interrupt
	tmr = ms;
 	IEC0bits.T2IE = 1;      // Enable Timer interrupt
}

char Timer_Check(void)
{
	return (tmr == 0);
}

void Delay_ms(int delay)
{
 	IEC0bits.T2IE = 0;      // Disable Timer interrupt
	tmr_delay = delay;
 	IEC0bits.T2IE = 1;      // Enable Timer interrupt

	while(tmr_delay)
		Idle();
}
