#include <p24Fxxxx.h>
#include "wind.h"
#include "direction.h"

static volatile unsigned int timer_count;
#define BUF_SIZE	3	//must be (2^x - 1) in size
static volatile unsigned long wind_value[BUF_SIZE+1];
static volatile unsigned int ptr;

typedef union _ULONG_VAL
{
    unsigned long Val;
	unsigned int  w[2];
    struct
    {
        unsigned int LW;
        unsigned int HW;
    } word;
} ULONG_VAL;

void Wind_Init(void) 
{
	int i;

	//clear buffer
	IC1CON1 = 0;
	IC1CON2 = 0;
	IC2CON1 = 0;
	IC2CON2 = 0;
	while(IC1CON1bits.ICBNE)
	{
		IC1BUF;
	}

	//Configure module
	IC1CON1bits.IC1TSEL = 4;	//timer1 clock
	IC2CON1bits.IC2TSEL = 4;
	IC1CON2bits.IC32 = 1;		//cascade mode
	IC2CON2bits.IC32 = 1;

 	IFS0bits.IC1IF = 0;      // Reset interrupt flag
	IPC0bits.IC1IP = 7;      // Interrupt priority
 	IEC0bits.IC1IE = 1;      // Enable interrupt

	//setup timer
	T1CON = 0;              // Timer reset
 	IFS0bits.T1IF = 0;      // Reset interrupt flag
	IPC0bits.T1IP = 6;      // Interrupt priority
 	IEC0bits.T1IE = 1;      // Enable interrupt
	T1CONbits.TSIDL = 0;	// Run in Idle
	T1CONbits.TCKPS = 1;	// 1:8 Prescale
 	TMR1=  0x0000;  		// Clear counter	
	PR1 = 0xFFFF; 			// Timer period register

	//clear holding registers
	ptr = 0;
	for(i=0;i<=BUF_SIZE;i++)
		wind_value[i] = 40L << 16;

	//clear counts
	timer_count = 40;

	//enable
 	T1CONbits.TON = 1;
	IC2CON1bits.ICM = 2;	//capture on falling edge
	IC1CON1bits.ICM = 2;	//always enable lower half last
}

void __attribute__((interrupt,no_auto_psv)) _T1Interrupt( void )
{
	IFS0bits.T1IF = 0;

	if(timer_count < 40)
		++timer_count;

	//check last stored value by moving backwards 1 from current location
	unsigned int test_ptr = ptr;
	if(test_ptr == 0)
		test_ptr = BUF_SIZE;
	else
		--test_ptr;
	if((wind_value[test_ptr] + 0x8000L) < (long)timer_count << 16)
	{
		wind_value[ptr] = (long)timer_count << 16;
		ptr = (ptr + 1) & BUF_SIZE;
	}
	if(timer_count >= 40)
	{
		wind_value[ptr] = 40L << 16;
		ptr = (ptr + 1) & BUF_SIZE;
	}
}

void __attribute__((interrupt,no_auto_psv)) _IC1Interrupt( void )
{
	static unsigned long previous_value;
	ULONG_VAL value;
	unsigned long delta_value;

	IFS0bits.IC1IF = 0;

	value.word.HW = IC2BUF;
	value.word.LW = IC1BUF;

    //Calculate duration between pulses
    delta_value = value.Val - previous_value;

    //store previous value
    previous_value = value.Val;
            
    //Watch for pulses that are too fast. 5000 = 57.2 m/s or 127.95 mph
    if(delta_value < 5000)
        return; //Discard value and do not reset timer
    
    //If timer hasn't expired, store value (if it is expired the wind is just starting)
	if(timer_count < 40)
	{
		//save data
		wind_value[ptr] = delta_value;
		ptr = (ptr + 1) & BUF_SIZE;
	}

	//reset counter
	TMR1 =  0x0000;
	timer_count = 0;
}

void Wind_Get(unsigned long *wind)
{
	unsigned long avg = 0;
	int i;

	//average
	for(i=0;i<=BUF_SIZE;i++)
	{
		avg += wind_value[i];

	}

	avg /= (BUF_SIZE+1);

	*wind = avg;
}

void Wind_GetRaw(unsigned long *w1)
{
	int tptr;

	//save last reading
	tptr = ptr;
	tptr--;
	if(tptr < 0)
		tptr = BUF_SIZE;
	*w1 = wind_value[tptr];
}
