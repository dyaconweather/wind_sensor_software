import minimalmodbus as mm
import serial
import pandas as pd
from struct import *
import time
from serial.serialutil import PARITY_EVEN, SEVENBITS, SerialTimeoutException

'''msg = bytearray(12)
sn = 1666
sn = sn.to_bytes(2, 'big')
#new_sn = pack('>H',65340 )
print(sn[0], sn[1])'''

def setNorth():
    ins = mm.Instrument('COM6',1, debug=True)
    address = chr(1)
    functioncode = chr(6)
    code = pack('>HBBBB',0, 117, 191, 0, 2)
    code = str(code, encoding="latin1")
    while True:
        try:
            data = ins._perform_command(6, code)
        except mm.NoResponseError:
            continue
        except:
            break


def setSN(sn: int):
    # SN must be less than or equal to 65535. Must be an integer. Must be greater than 0.
    ins = mm.Instrument('COM6',1, debug=True)
    code = pack('>HBBB',0, 117, 191, 0)
    case = pack('B', 1)
    this_sn = sn.to_bytes(2, 'big')
    payload = (str(code, encoding="latin1")+str(case, encoding="latin1")
                +str(this_sn[0].to_bytes(1,'big'), encoding="latin1")
                +str(this_sn[1].to_bytes(1, 'big'), encoding="latin1"))
    request = bytes(payload, encoding="latin1")
    print(request)
    #print(unpack('8c',request))
    while True:
        try:
            data = ins._perform_command(6, payload)
        except mm.NoResponseError:
            continue
        except Exception as e:
            print(e)
            break


def readDirection():
    ins = mm.Instrument('COM6',1)
    #ins.write_register(30143, value = 2, functioncode=6)
    while True:
        try:
            direction = ins.read_register(202, number_of_decimals=1)
            print(direction)
        except:
            pass

def readSN():
    ins = mm.Instrument('COM6',1)
    while True:
        try:
            sn = ins.read_register(101, number_of_decimals=0)
            print(sn)
            break
        except:
            continue

def tryAll():
    ins = mm.Instrument('COM6',1)
    while True:
        try:
            all = ins.read_registers(201,8,4)
            print(all)
            time.sleep(0.2)
        except:
            continue

def trySDI12():
    ins = serial.Serial('COM4', bytesize=SEVENBITS, parity=PARITY_EVEN)
    ins.baudrate = 1200
    ins.timeout = 1
    ins.write_timeout = 0.5
    ins.break_condition = ' '
    duration = 0.012
    address = 0
    command1 = f"{address}M!"
    command2 = f"{address}D0!"

    def measure():
        while True:
            time.sleep(0.25)
            try:
                ins.reset_input_buffer()
                ins.send_break(duration=duration)
                ins.write(command1.encode('ascii'))
                raw = ins.readline().decode('utf-8')
                # raw = ins.readline().decode('utf-8')
                print(raw)
                ins.write(command2.encode('ascii'))
                raw = ins.readline().decode('utf-8')
                print(raw)
                raw = ins.readline().decode('utf-8')
                print(raw)
                raw = raw.strip('\r\n')
                print(raw)
                values = raw.split('+')
                del values[0]
                print(values)
            except:
                raise

    def getsn():
        command = f"{address}I!"
        while True:
            try:
                ins.reset_input_buffer()
                ins.send_break(duration=duration)
                ins.write(command.encode('ascii'))
                raw = ins.readline().decode('utf-8')
                raw = raw.strip('\r\n')
                sn = raw[20:]
                print(sn)
                time.sleep(0.25)
                break
            except:
                raise
    
    def setsn(sn:int):
        digitKey = pack('>BBBB',13, 1, 37, 90)
        andValue = 63
        this_sn = []
        firstRaw = sn>>12
        this_sn.append(firstRaw&63)
        secondRaw = (sn>>6)
        this_sn.append((secondRaw)&63)
        this_sn.append(sn&63)

        payload = (str(digitKey, encoding="latin1")
                    +str(this_sn[0].to_bytes(1, 'big'), "latin1")
                    +str(this_sn[1].to_bytes(1, 'big'), "latin1")
                    +str(this_sn[2].to_bytes(1, 'big'), "latin1")+'!')
        request = bytes(payload, encoding="latin1")
        try:
            ins.reset_input_buffer()
            ins.send_break(duration=duration)
            ins.write(f"{address}B{payload}".encode('ascii'))
            while True:
                raw = ins.read().decode('utf-8')
                print(raw)
                if raw == '0':
                    break
                time.sleep(.1)
        except:
            raise

    def setnorth():
        digitKey = pack('>BBBBB', 66, 14, 1, 37, 90)
        payload = str(digitKey, "latin1")
        try:
            ins.reset_input_buffer()
            ins.send_break(duration=duration)
            ins.write(f"{address}{payload}!".encode('ascii'))
            raw = ins.readline().decode('utf-8')
            print(raw)
            raw = ins.readline().decode('utf-8')
            print(raw)
        except:
            raise
    #setsn(11008)
    #getsn()
    #setnorth()
    measure()


#setNorth()
#readDirection()
#setSN(1258)
#readSN()
#tryAll()
trySDI12()