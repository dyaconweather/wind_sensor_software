import serial
import minimalmodbus as mm

class WSD1():
    def __init__(self, port, address):
        self.sn = None
        self.port = port
        self.address = address
        self.baudrate = None
        self.comm = None
        self.connected = False
    
    def connection(self):
        self.comm = mm.Instrument(self.port, self.address, debug=True)
        self.comm.serial.timeout=0.9

    def getMeasurements(self):
        for x in range(10):
            try:
                # answer = self.comm.write_register(104, 3, functioncode=6, signed=True)
                # print(answer)
                # self.comm.serial.baudrate = 9600
                rawValuesList = self.comm.read_register(103, functioncode = 3)
                print(rawValuesList)
                #break
            except mm.NoResponseError:
                '''Will raise to def measurementLoop, which will take care of it.'''
                raise
            except:
                raise

    def changeAddress(self, newAddress:int, error_count:int):
        '''Attempt to change sensor address. This function can't be called until the sensor is configured, so any error should
        be a checksum/timing/fluke, UNLESS the user has unplugged the sensor after configuring it. Due to that possibility and
        to avoid an infinite loop, this function will call itself only 5 times, then give an error.'''
        success = False
        try:
            self.comm.write_register(103, newAddress, functioncode=6, signed=True)
            # self.address = self.comm.read_register(103)
            # print(self.address)
            # self.comm = mm.Instrument(self.port, newAddress)
            # success = True
        except Exception as e:
            print(e)
            error_count += 1
            if error_count == 10:
                raise
            else:
                raise
                #self.changeAddress(newAddress, error_count)
        return success
            

    def test(self):
        ser = serial.Serial('COM4', 19200, timeout=2)
        print(ser)

        ser.write(':010310010001EA\r\n'.encode('ISO-8859-1'))
        print(ser.read_until())
        
wind = WSD1('COM4', 1)
wind.connection()
wind.getMeasurements()
#wind.changeAddress(3, 0)
#wind.test()